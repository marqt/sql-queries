select 
    partner.naam,
    count(distinct (inkooporder.id)) / (select 
            count(inkooporder.id)
        from
            inkooporder
        where
            inkooporder.statusdatumtijd >= DATE_SUB(DATE_ADD(MAKEDATE(YEAR(CURDATE()), 1),
                    INTERVAL WEEK(CURDATE()) - 1 WEEK),
                INTERVAL WEEKDAY(DATE_ADD(MAKEDATE(YEAR(CURDATE()), 1),
                            INTERVAL WEEK(CURDATE()) WEEK)) DAY)
                AND inkooporder.statusdatumtijd < DATE_SUB(DATE_ADD(MAKEDATE(YEAR(CURDATE()), 1),
                    INTERVAL WEEK(CURDATE()) WEEK),
                INTERVAL WEEKDAY(DATE_ADD(MAKEDATE(YEAR(CURDATE()), 1),
                            INTERVAL WEEK(CURDATE()) WEEK)) DAY)) percentile
from
    inkooporder,
    inkooporderregel,
    partner
where
    inkooporder.statusdatumtijd >= DATE_SUB(DATE_ADD(MAKEDATE(YEAR(CURDATE()), 1),
            INTERVAL WEEK(CURDATE()) - 1 WEEK),
        INTERVAL WEEKDAY(DATE_ADD(MAKEDATE(YEAR(CURDATE()), 1),
                    INTERVAL WEEK(CURDATE()) WEEK)) DAY)
        AND inkooporder.statusdatumtijd < DATE_SUB(DATE_ADD(MAKEDATE(YEAR(CURDATE()), 1),
            INTERVAL WEEK(CURDATE()) WEEK),
        INTERVAL WEEKDAY(DATE_ADD(MAKEDATE(YEAR(CURDATE()), 1),
                    INTERVAL WEEK(CURDATE()) WEEK)) DAY)
        AND inkooporder.partnerid = partner.id
        AND inkooporder.id = inkooporderregel.orderid
        AND partner.OrderEDIFACT = 0
group by partner.naam
order by percentile DESC