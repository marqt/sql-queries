select 
    afdeling.omschrijving, partner.naam, levergroep.naam, orderschemaitem.besteldag, orderschemaitem.besteltijd
from
    winkel,
    artikel,
    winkelartikel,
    winkelpartnerafdeling,
    levergroep,
    afdeling,
    orderschema,
    orderschemaitem,
    partner
where
    winkel.id = winkelartikel.winkelid
        and artikel.id = winkelartikel.artikelid
        and winkelartikel.winkelpartnerafdelingid = winkelpartnerafdeling.id
        and winkelpartnerafdeling.defaultlevergroepid = levergroep.id
        and winkelpartnerafdeling.afdelingid = afdeling.id
        and winkelpartnerafdeling.partnerid = partner.id
        and levergroep.id = orderschema.levergroepid
        and orderschema.id = orderschemaitem.orderschemaid
        and orderschema.actief = 1
        and winkel.nummer = 151
group by afdeling.id , partner.id