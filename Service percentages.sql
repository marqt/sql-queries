select 
    jaarweek,
    partnernummer,
    partnernaam,
    afdeling,
    orderkenmerk,
    servicepercentage,
    goederenontvangstdatumtijd,
    winkel,
    grondslag,
    artikel.omschrijving1,
    sum(v_orderstatus.BEbesteld) as BEbesteld,
    sum(v_orderstatus.CEbesteld) CEbesteld,
    sum(v_orderstatus.CEgeleverd) CEgeleverd
from
    v_orderstatus
        inner join
    artikel ON artikel.id = v_orderstatus.artikelid
where
    jaarweek = concat(YEAR(CURDATE()),
            (DATE_FORMAT(CURDATE(), '%v%') - 2))
        and orderstatus = 'GELEVERD'
group by artikelid , orderkenmerk , winkel
order by goederenontvangstdatumtijd
