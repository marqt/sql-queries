#
# Cyclecount KW voor telling op dinsdag en woensdag
# Parameters defineren voor testen. Voor live in BIRT @Winkelid vervangen door ?
#
Set @WinkelID=9;

#
# Artikelen met een negatieve voorraadstand
#
(select 
    og3.omschrijving,
    winkelartikel.schapnummer,
    winkelartikel.planknummer,
    winkelartikel.positie,
    artikel.Omschrijving1,
    artikel.merk,
    winkelartikelvoorraadstand.voorraadstand,
    product.inhoud,
    meeteenheid.code,
    (select 
            lpad(barcode, 13, 0)
        from
            barcode
        where
        	artikel.id = barcode.artikelid
            and datumvanaf <= curdate()
                and besteleenheidid is null
        order by datumvanaf desc
        limit 1) as cebarcode
from
    artikel,
    winkelartikel,
    winkelartikelvoorraadstand,
    product,
    meeteenheid,
    operationelegroep og1,
    operationelegroep og2,
    operationelegroep og3
where
    artikel.id = winkelartikel.ArtikelId
        and winkelartikel.id = winkelartikelvoorraadstand.winkelartikelId
        and artikel.ProductId = product.id
        and product.inhoudmeeteenheidid = meeteenheid.Id
        and product.OperationeleGroepId = og3.id
        and og3.OperationeleGroepParentId = og2.id
        and og2.OperationeleGroepParentId = og1.id
        #artikelen met een negatieve voorraadstand
        and winkelartikelvoorraadstand.voorraadstand < 0
        #artikelstatus is actueel of gewijzigd
        and artikel.StatusId in (3 , 4)
        and winkelartikel.bestelbaar = 1
        and winkelartikel.winkelid = @WinkelID
        #operationelegroep is KW-Food, Wijn of Non-food
        and og2.id in (417 , 418, 419)
        #het artikel staat niet in een inkooporder voor deze winkel voor deze dag
		and not exists (select 
    		artikelid
		from
   			inkooporder,
   			inkooporderregel
		where
   			inkooporder.id = inkooporderregel.orderid
        and date(inkooporder.verwachteLeverDatumWinkelTijd) = curdate()
		and winkelid = @WinkelID
		and artikelid = artikel.id)
        #het veld inassortiment ligt niet in de afgelopen 14 dagen of is niet ingevuld
		and (inAssortiment < date_sub(curdate(), interval 14 day)
        or inAssortiment is null)
        #het artikel is niet geinventariseerd de afgelopen 7 dagen tenzij de telling < -10 of >10 was
        and not exists( select 
            ArtikelId
        from
            inventaris,
            inventarisregel
        where
            inventaris.id = inventarisregel.inventarisid
                and inventaris.statusdatumtijd > date_sub(curdate(), interval 7 day)
                and inventaris.winkelid = winkelartikel.winkelid
                and inventaris.type != 'CORRECTIETELLING'
                and inventarisregel.artikelid = artikel.id
                and inventarisregel.totaalgeteld > -10
                and inventarisregel.totaalgeteld < 10))
                
union

#
# Alle artikelen met een invenarisatieboeking van < -10 of > 10 in de afgelopen 7 dagen
#
(select 
    og3.omschrijving,
    winkelartikel.schapnummer,
    winkelartikel.planknummer,
    winkelartikel.positie,
    artikel.Omschrijving1,
    artikel.merk,
    winkelartikelvoorraadstand.voorraadstand,
    product.inhoud,
    meeteenheid.code,
    (select 
            lpad(barcode, 13, 0)
        from
            barcode
        where
        	artikel.id = barcode.artikelid
            and datumvanaf <= curdate()
                and besteleenheidid is null
        order by datumvanaf desc
        limit 1) as cebarcode
from
    artikel,
    winkelartikel,
    winkelartikelvoorraadstand,
    product,
    meeteenheid,
    operationelegroep og1,
    operationelegroep og2,
    operationelegroep og3
where
    artikel.id = winkelartikel.ArtikelId
        and winkelartikel.id = winkelartikelvoorraadstand.winkelartikelId
        and artikel.ProductId = product.id
        and product.inhoudmeeteenheidid = meeteenheid.Id
        and product.OperationeleGroepId = og3.id
        and og3.OperationeleGroepParentId = og2.id
        and og2.OperationeleGroepParentId = og1.id
        #artikelstatus is actueel of gewijzigd
        and artikel.StatusId in (3 , 4)
        and winkelartikel.bestelbaar = 1
        and winkelartikel.winkelid = @WinkelID
        #operationelegroep is KW-Food, Wijn of Non-food
        and og2.id in (417 , 418, 419)
        #het artikel staat niet in een inkooporder voor deze winkel voor deze dag
		and not exists (select 
    		artikelid
		from
   			inkooporder,
   			inkooporderregel
		where
   			inkooporder.id = inkooporderregel.orderid
        and date(inkooporder.verwachteLeverDatumWinkelTijd) = curdate()
		and winkelid = @WinkelID
		and artikelid = artikel.id)
        #het veld inassortiment ligt niet in de afgelopen 14 dagen of is niet ingevuld
		and (inAssortiment < date_sub(curdate(), interval 14 day)
        or inAssortiment is null)
        #het artikel is wel geinventariseerd de afgelopen 7 dagen en de telling was < -10 of >10 
        and exists( select 
            ArtikelId
        from
            inventaris,
            inventarisregel
        where
            inventaris.id = inventarisregel.inventarisid
                and inventaris.statusdatumtijd > date_sub(curdate(), interval 7 day)
                and inventaris.winkelid = winkelartikel.winkelid
                and inventaris.type != 'CORRECTIETELLING'
                and inventarisregel.artikelid = artikel.id
                and inventarisregel.totaalgeteld > -10
                and inventarisregel.totaalgeteld < 10))
                
 union

#
# 10 Artikelen met voorraadstand > 80
#               
(select 
    og3.omschrijving,
    winkelartikel.schapnummer,
    winkelartikel.planknummer,
    winkelartikel.positie,
    artikel.Omschrijving1,
    artikel.merk,
    winkelartikelvoorraadstand.voorraadstand,
    product.inhoud,
    meeteenheid.code,
    (select 
            lpad(barcode, 13, 0)
        from
            barcode
        where
        	artikel.id = barcode.artikelid
            and datumvanaf <= curdate()
                and besteleenheidid is null
        order by datumvanaf desc
        limit 1) as cebarcode
from
    artikel,
    winkelartikel,
    winkelartikelvoorraadstand,
    product,
    meeteenheid,
    operationelegroep og1,
    operationelegroep og2,
    operationelegroep og3
where
    artikel.id = winkelartikel.ArtikelId
        and winkelartikel.id = winkelartikelvoorraadstand.winkelartikelId
        and artikel.ProductId = product.id
        and product.inhoudmeeteenheidid = meeteenheid.Id
        and product.OperationeleGroepId = og3.id
        and og3.OperationeleGroepParentId = og2.id
        and og2.OperationeleGroepParentId = og1.id
        #artikelen met voorraadstand > 80
        and winkelartikelvoorraadstand.voorraadstand >80
        #artikelstatus is actueel of gewijzigd
        and artikel.StatusId in (3 , 4)
        and winkelartikel.bestelbaar = 1
        and winkelartikel.winkelid = @WinkelID
        #operationelegroep is KW-Food, Wijn of Non-food
        and og2.id in (417 , 418, 419)
        #het artikel staat niet in een inkooporder voor deze winkel voor deze dag
		and not exists (select 
    		artikelid
		from
   			inkooporder,
   			inkooporderregel
		where
   			inkooporder.id = inkooporderregel.orderid
        and date(inkooporder.verwachteLeverDatumWinkelTijd) = curdate()
		and winkelid = @WinkelID
		and artikelid = artikel.id)
        #het veld inassortiment ligt niet in de afgelopen 14 dagen of is niet ingevuld
		and (inAssortiment < date_sub(curdate(), interval 14 day)
        or inAssortiment is null)
        #het artikel is niet geinventariseerd de afgelopen 7 dagen tenzij de telling < -10 of >10 was
        and not exists( select 
            ArtikelId
        from
            inventaris,
            inventarisregel
        where
            inventaris.id = inventarisregel.inventarisid
                and inventaris.statusdatumtijd > date_sub(curdate(), interval 7 day)
                and inventaris.winkelid = winkelartikel.winkelid
                and inventaris.type != 'CORRECTIETELLING'
                and inventarisregel.artikelid = artikel.id
                and inventarisregel.totaalgeteld > -10
                and inventarisregel.totaalgeteld < 10))
union
                
#
# Alle artikelen met voorraadstand van 1,2 of 3 stuks
#     
(select 
    og3.omschrijving,
    winkelartikel.schapnummer,
    winkelartikel.planknummer,
    winkelartikel.positie,
    artikel.Omschrijving1,
    artikel.merk,
    winkelartikelvoorraadstand.voorraadstand,
    product.inhoud,
    meeteenheid.code,
    (select 
            lpad(barcode, 13, 0)
        from
            barcode
        where
        	artikel.id = barcode.artikelid
            and datumvanaf <= curdate()
                and besteleenheidid is null
        order by datumvanaf desc
        limit 1) as cebarcode
from
    artikel,
    winkelartikel,
    winkelartikelvoorraadstand,
    product,
    meeteenheid,
    operationelegroep og1,
    operationelegroep og2,
    operationelegroep og3
where
    artikel.id = winkelartikel.ArtikelId
        and winkelartikel.id = winkelartikelvoorraadstand.winkelartikelId
        and artikel.ProductId = product.id
        and product.inhoudmeeteenheidid = meeteenheid.Id
        and product.OperationeleGroepId = og3.id
        and og3.OperationeleGroepParentId = og2.id
        and og2.OperationeleGroepParentId = og1.id
        #artikelen met voorraadstand groter dan 0 en kleiner of gelijk aan 3
        and winkelartikelvoorraadstand.voorraadstand > 0
        and winkelartikelvoorraadstand.voorraadstand <= 3
        #artikelstatus is actueel of gewijzigd
        and artikel.StatusId in (3 , 4)
        and winkelartikel.bestelbaar = 1
        and winkelartikel.winkelid = @WinkelID
        #operationelegroep is KW-Food, Wijn of Non-food
        and og2.id in (417 , 418, 419)
        #het artikel staat niet in een inkooporder voor deze winkel voor deze dag
		and not exists (select 
    		artikelid
		from
   			inkooporder,
   			inkooporderregel
		where
   			inkooporder.id = inkooporderregel.orderid
        and date(inkooporder.verwachteLeverDatumWinkelTijd) = curdate()
		and winkelid = @WinkelID
		and artikelid = artikel.id)
        #het veld inassortiment ligt niet in de afgelopen 14 dagen of is niet ingevuld
		and (inAssortiment < date_sub(curdate(), interval 14 day)
        or inAssortiment is null)
        #het artikel is niet geinventariseerd de afgelopen 7 dagen tenzij de telling < -10 of >10 was
        and not exists( select 
            ArtikelId
        from
            inventaris,
            inventarisregel
        where
            inventaris.id = inventarisregel.inventarisid
                and inventaris.statusdatumtijd > date_sub(curdate(), interval 7 day)
                and inventaris.winkelid = winkelartikel.winkelid
                and inventaris.type != 'CORRECTIETELLING'
                and inventarisregel.artikelid = artikel.id
                and inventarisregel.totaalgeteld > -10
                and inventarisregel.totaalgeteld < 10))
                

                
                order by rand() limit 50