select 
    date_format(verkoop.datum,'%a'), verkoop.uur, count(verkoopregel.id)
from
    verkoop,
    verkoopregel
where
    verkoop.id = verkoopregel.verkoopid
        and verkoop.jaar = 2014
        and verkoop.week = 25
and verkoopregel.afdelingid = 6
and TransactieSoort = 'KAS'
group by verkoop.weekdag, verkoop.Uur