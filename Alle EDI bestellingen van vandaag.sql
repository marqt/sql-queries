select 
    partner.naam, orderkenmerk, inkooporder.laatstewijziging
from
    inkooporder, partner
where
    inkooporder.partnerid = partner.id
        and inkooporder.laatstewijziging > date_sub(CURDATE(), interval 2 DAY)
        and inkooporder.laatstewijziging < date_add(CURDATE(), interval 1 DAY)
        and partner.OrderEDIFACT = 1
	and inkooporder.orderstatus = 'BESTELLING'