select 
    null as 'InvoiceNr',
    null as 'Factuurnummer',
    inkooporder.orderkenmerk as 'Ordernummer',
    partner.naam as 'Leverancier',
    winkel.nummer as 'Kostenplaats',
    inkooporder.goederenontvangstdatumtijd as 'Datum ontvangen',
    null as 'Bedrag incl. BTW',
    null as 'BTW bedrag',
    inkooporderregel.MWbesteld as 'MW besteld',
    inkooporderregel.CEbesteld as 'CE besteld',
    inkooporderregel.MWgeleverd as 'MW geleverd',
    inkooporderregel.CEgeleverd as 'CE geleverd',
    format(if(inkooporderregel.MWgeleverd > 1.1 * inkooporderregel.MWbesteld,
            1.1 * inkooporderregel.MWbesteld,
            inkooporderregel.MWgeleverd),
        2,
        'nl_NL') as 'max te factureren bedrag excl. BTW',
    format(if(inkooporderregel.CEgeleverd > 1.1 * inkooporderregel.CEbesteld,
            1.1 * inkooporderregel.CEbesteld,
            inkooporderregel.CEgeleverd),
        2,
        'nl_NL') as 'max te factureren aantallen',
    winkelartikel.bestelcodepartner as 'Artikelcode leverancier',
    artikel.artikelnummer as 'Artikelcode'
from
    inkooporder,
    inkooporderregel,
    winkelartikel,
    artikel,
    partner,
    winkel
where
    inkooporder.id = inkooporderregel.orderid
        and inkooporderregel.artikelid = artikel.id
        and inkooporder.winkelid = winkelartikel.winkelid
        and artikel.id = winkelartikel.artikelid
        and inkooporder.partnerid = partner.id
        and inkooporder.winkelid = winkel.id
        and inkooporder.goederenontvangstdatumtijd >= date_sub(curdate(), interval (7) week)
        and inkooporder.goederenontvangstdatumtijd <= date_sub(curdate(), interval (1) week)
        and orderkenmerk = 32172