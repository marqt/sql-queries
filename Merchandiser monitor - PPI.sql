SELECT 
    jaar2013min1week.omschrijving,
    jaar2013min1week.ppi,
    jaar2013min1week.ppi / jaar2012min1week.ppi - 1
FROM
    (SELECT 
        com2.omschrijving,
            sum(totaalbedrag) / sum(totaalmarqtvolume) AS 'PPI'
    FROM
        resultaat, artikel, product, winkel, commercielegroep com3, commercielegroep com2
    WHERE
        resultaat.artikelid = artikel.id
            AND product.id = artikel.productid
            AND winkel.id = resultaat.winkelid
            AND com3.id = product.commercieleGroepId
            AND com2.id = com3.commercieleGroepParentId
            AND resultaat.jaar = YEAR(CURDATE())
            AND resultaat.week = WEEK(CURDATE(), 3) - 1
            AND resultaat.label = 'OPSW'
    GROUP BY com2.id) jaar2013min1week,
    (SELECT 
        com2.omschrijving,
            sum(totaalbedrag) / sum(totaalmarqtvolume) AS 'PPI'
    FROM
        resultaat, artikel, product, winkel, commercielegroep com3, commercielegroep com2
    WHERE
        resultaat.artikelid = artikel.id
            AND product.id = artikel.productid
            AND winkel.id = resultaat.winkelid
            AND com3.id = product.commercieleGroepId
            AND com2.id = com3.commercieleGroepParentId
            AND resultaat.jaar = YEAR(CURDATE()) - 1
            AND resultaat.week = WEEK(CURDATE(), 3) - 1
            AND resultaat.label = 'OPSW'
    GROUP BY com2.id) jaar2012min1week
WHERE
    jaar2013min1week.omschrijving = jaar2012min1week.omschrijving
