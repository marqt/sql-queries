#
# Cyclecount Zuivel voor telling.
# 
# Parameters defineren voor testen. Voor live in BIRT @Winkelid vervangen door ?
#
Set @WinkelID=2;

#
# Artikelen Zuivel 

(select 
    og3.omschrijving,
    winkelartikel.schapnummer,
    winkelartikel.planknummer,
    winkelartikel.positie,
    artikel.Omschrijving1,
    artikel.merk,
    winkelartikelvoorraadstand.voorraadstand,
    product.inhoud,
    meeteenheid.code,
    (select 
            lpad(barcode, 13, 0)
        from
            barcode
        where
        	artikel.id = barcode.artikelid
            and datumvanaf <= curdate()
                and besteleenheidid is null
        order by datumvanaf desc
        limit 1) as cebarcode,
    product.grondslagid,
    artikel.artikelTypeId
from
    artikel,
    winkelartikel,
    winkelartikelvoorraadstand,
    product,
    meeteenheid,
    operationelegroep og1,
    operationelegroep og2,
    operationelegroep og3
where
    artikel.id = winkelartikel.ArtikelId
        and winkelartikel.id = winkelartikelvoorraadstand.winkelartikelId
        and artikel.ProductId = product.id
        and product.inhoudmeeteenheidid = meeteenheid.Id
        and product.OperationeleGroepId = og3.id
        and og3.OperationeleGroepParentId = og2.id
        and og2.OperationeleGroepParentId = og1.id
        #Voorraadstand groter dan 2
        and winkelartikelvoorraadstand.voorraadstand > 2
        #Status gewijzigd of actueel
        and artikel.StatusId in (3 , 4)
        and winkelartikel.bestelbaar = 1
        and winkelartikel.winkelid = @WinkelID
        # alleen OG2 (Zuivel en Toetjes)
        and og2.id in (407)
        # Komt niet voor in een inventarisatie van de afgelopen 3 dagen
        and not exists( select 
            ArtikelId
        from
            inventaris,
            inventarisregel
        where
            inventaris.id = inventarisregel.inventarisid
                and inventaris.statusdatumtijd > date_sub(curdate(), interval 3 day)
                and inventaris.winkelid = winkelartikel.winkelid
                and inventaris.type != 'CORRECTIETELLING'
                and inventarisregel.artikelid = artikel.id)
        # En komt niet voor in een inkooporder die vandaag geleverd wordt        
        and not exists 
        (select 
    		artikelid
		from
   			inkooporder,
   			inkooporderregel
		where
   			inkooporder.id = inkooporderregel.orderid
        and date(inkooporder.verwachteLeverDatumWinkelTijd) = curdate()
		and winkelid = @WinkelID
		and artikelid = artikel.id)
order by rand()
limit 15)
union 
(select 
    og3.omschrijving,
    winkelartikel.schapnummer,
    winkelartikel.planknummer,
    winkelartikel.positie,
    artikel.Omschrijving1,
    artikel.merk,
    winkelartikelvoorraadstand.voorraadstand,
    product.inhoud,
    meeteenheid.code,
    (select 
            lpad(barcode, 13, 0)
        from
            barcode
        where
        	artikel.id = barcode.artikelid
            and datumvanaf <= curdate()
                and besteleenheidid is null
        order by datumvanaf desc
        limit 1) as cebarcode,
    product.grondslagid,
    artikel.artikelTypeId
from
    artikel,
    winkelartikel,
    winkelartikelvoorraadstand,
    product,
    meeteenheid,
    operationelegroep og1,
    operationelegroep og2,
    operationelegroep og3
where
    artikel.id = winkelartikel.ArtikelId
        and winkelartikel.id = winkelartikelvoorraadstand.winkelartikelId
        and artikel.ProductId = product.id
        and product.inhoudmeeteenheidid = meeteenheid.Id
        and product.OperationeleGroepId = og3.id
        and og3.OperationeleGroepParentId = og2.id
        and og2.OperationeleGroepParentId = og1.id
        # Negatieve voorraadstand
        and winkelartikelvoorraadstand.voorraadstand < 0
        and artikel.StatusId in (3 , 4)
        and winkelartikel.bestelbaar = 1
        and winkelartikel.winkelid = @WinkelID
        and og2.id in (407)
		and not exists (select 
    		artikelid
		from
   			inkooporder,
   			inkooporderregel
		where
   			inkooporder.id = inkooporderregel.orderid
        and date(inkooporder.verwachteLeverDatumWinkelTijd) = curdate()
		and winkelid = @WinkelID
		and artikelid = artikel.id))