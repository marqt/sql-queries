#leadtimes in orderschemas actief in de seek van 14-21 juni (7dagen)

select 
    partner.naam,
    partner.nummer as partnernummer, 
    levergroep.naam as levergroep_naam,
    LeverGroepId,
    orderschema.naam as orderschema_naam,
    orderschemaitem.LeverDagDC,
    orderschemaitem.LeverDagWinkel
from
    partner,
    levergroep,
    orderschema,
    orderschemaitem
where
    partner.id = levergroep.partnerid
        and levergroep.id = orderschema.levergroepid
        and orderschema.id = orderschemaitem.orderschemaid
        and orderschema.actief = 1
        and OrderSchemaId in (SELECT 

 
    `orderschema`.`Id` AS `orderschemaid`

FROM
    (((((`levergroep`
    JOIN `orderschema`)
    JOIN `orderschemaitem`)
    JOIN `datuminfo` `besteldag`)
    JOIN `datuminfo` `leverdagwinkel`)
    LEFT JOIN `orderschema` `orderschema_leeg` ON (((`orderschema_leeg`.`LeverGroepId` = `orderschema`.`LeverGroepId`)
        AND (`orderschema_leeg`.`IngangsDatum` <= `besteldag`.`Datum`)
        AND (`orderschema_leeg`.`EindDatum` >= `besteldag`.`Datum`)
        AND (`orderschema_leeg`.`Actief` <> 0)
        AND (`orderschema_leeg`.`Id` <> `orderschema`.`Id`))))
WHERE
    ((`levergroep`.`Id` = `orderschema`.`LeverGroepId`)
        AND (`orderschema`.`Id` = `orderschemaitem`.`OrderSchemaId`)
        AND (`orderschema`.`Actief` <> 0)
        AND (`besteldag`.`WeekDag` = `orderschemaitem`.`BestelDag`)
        AND (`besteldag`.`Datum` >= (NOW() + INTERVAL 21 DAY))
        AND (`besteldag`.`Datum` <= (NOW() + INTERVAL 28 DAY))
        AND (`leverdagwinkel`.`Datum` = (`besteldag`.`Datum` + INTERVAL `orderschemaitem`.`LeverDagWinkel` DAY))
        AND (`orderschema`.`IngangsDatum` <= `besteldag`.`Datum`)
        AND ((`orderschema`.`EindDatum` >= `besteldag`.`Datum`)
        OR ISNULL(`orderschema`.`EindDatum`))
        AND ISNULL(`orderschema_leeg`.`Id`))
ORDER BY `besteldag`.`Datum` , `orderschemaitem`.`BestelTijd` , `orderschema`.`IngangsDatum` DESC , `orderschema`.`EindDatum`)

        
        order by LeverDagDC
	