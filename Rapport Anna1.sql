SELECT 
    *
FROM
    (SELECT 
        com2.omschrijving,
            week,
            sum(totaalbedrag) AS 'totaalbedrag2012',
            sum(totaalbtw) AS 'btw2012',
            sum(totaalmarqtwaarde) AS 'mw2012',
            sum(totaalmarqtvolume) AS 'scans2012'
    FROM
        resultaat, artikel, product, winkel, commercielegroep com3, commercielegroep com2
    WHERE
        resultaat.artikelid = artikel.id
            AND product.id = artikel.productid
            AND winkel.id = resultaat.winkelid
            AND com3.id = product.commercieleGroepId
            AND com2.id = com3.commercieleGroepParentId
            AND resultaat.jaar = 2012
            AND resultaat.week BETWEEN WEEK(CURDATE(), 6) - 2 AND WEEK(CURDATE(), 6) - 1
            AND resultaat.label = 'OPSW'
            AND winkel.nummer in (151 , 152, 153)
    GROUP BY com2.id , resultaat.week) jaar2012,
    (SELECT 
        com2.omschrijving,
            week,
            sum(totaalbedrag) AS 'totaalbedrag2013',
            sum(totaalbtw) AS 'btw2013',
            sum(totaalmarqtwaarde) AS 'mw2013',
            sum(totaalmarqtvolume) AS 'scans2013'
    FROM
        resultaat, artikel, product, winkel, commercielegroep com3, commercielegroep com2
    WHERE
        resultaat.artikelid = artikel.id
            AND product.id = artikel.productid
            AND winkel.id = resultaat.winkelid
            AND com3.id = product.commercieleGroepId
            AND com2.id = com3.commercieleGroepParentId
            AND resultaat.jaar = 2013
            AND resultaat.week BETWEEN WEEK(CURDATE(), 6) - 2 AND WEEK(CURDATE(), 6) - 1
            AND resultaat.label = 'OPSW'
            AND winkel.nummer in (151 , 152, 153)
    GROUP BY com2.id , resultaat.week) jaar2013,
    (SELECT 
    week, count(verkoop.id) AS 'klanten2012'
FROM
    verkoop,
    winkel
WHERE
    winkel.id = verkoop.winkelid
        AND verkoop.jaar = 2012
        AND verkoop.week BETWEEN WEEK(CURDATE(), 6) - 2 AND WEEK(CURDATE(), 6) - 1
        AND winkel.nummer in (151 , 152, 153)
GROUP BY verkoop.week) klanten2012,
    (SELECT 
    week, count(verkoop.id) AS 'klanten2013'
FROM
    verkoop,
    winkel
WHERE
    winkel.id = verkoop.winkelid
        AND verkoop.jaar = 2013
        AND verkoop.week BETWEEN WEEK(CURDATE(), 6) - 2 AND WEEK(CURDATE(), 6) - 1
        AND winkel.nummer in (151 , 152, 153)
GROUP BY verkoop.week) klanten2013
WHERE
    jaar2012.omschrijving = jaar2013.omschrijving
        AND jaar2012.week = jaar2013.week
        AND jaar2012.week = klanten2012.week
        AND jaar2012.week = klanten2013.week
