SELECT 
    'Op resultaat tabel',
    sum(totaalbedrag) as 'Omzet',
    sum(totaalbtw) as 'BTW',
    sum(totaalmarqtwaarde) as 'Inkoop',
    sum(Totaalvolume) as 'Volume',
    sum(TotaalItems) as 'Items',
    sum(Totaalmarqtvolume) as 'Scans',
    0 as 'Klanten'
from
    resultaat
where
    resultaat.label = 'OPSW' and jaar = 2014
        and week = 30
        and rubriekid = 1
union select 
    'Op verkoop tabel',
    sum(verkoop.bedragtotaal),
    sum(verkoop.bedragbtw),
    0,
    0,
    sum(verkoop.aantalitems) as 'Items',
    sum(verkoop.marqtvolume) as 'Scans',
    count(verkoop.id) as 'klanten'
from
    verkoop
where
    TransactieSoort = 'KAS' and jaar = 2014
        and week = 30
		#and bedragtotaal <> 0
union select 
    'Op verkoopregel tabel',
    sum(verkoopregel.bedragtotaal),
    sum(verkoopregel.bedragbtw),
    sum(verkoopregel.bedraginkoop * verkoopregel.hoeveelheid),
    round(sum(verkoopregel.hoeveelheid),2) as 'Volume',
    count(verkoopregel.id) as 'Items',
    sum(case
        when
            (product.GrondslagId = 2 or artikelnummer = 119823 or ArtikelNummer = 162009)
                and hoeveelheid > 0
        then
            1
        when
            (product.GrondslagId = 2 or artikelnummer = 119823 or ArtikelNummer = 162009)
                and hoeveelheid < 0
        then
            -1
        when
            (product.GrondslagId = 2 or artikelnummer = 119823 or ArtikelNummer = 162009)
                and hoeveelheid = 0
        then
            0
        else verkoopregel.Hoeveelheid
    end) as scans,
    count(distinct (verkoop.id)) as 'Klanten'
from
    verkoop,
    verkoopregel,
    artikel,
    product
where
    verkoop.id = verkoopregel.verkoopid
        and verkoopregel.ArtikelId = artikel.Id
        and artikel.ProductId = product.Id
        and verkoop.jaar = 2014
        and verkoop.week = 30
        and verkoopregel.rubriekid = 1