SELECT `bar1`.barcode AS 'gtin', `winkelart`.`bestelcodepartner` AS 'artikelnummer_leverancier', `art`.`Omschrijving1` AS 'artoms', `BEbar`.`barcode` as 'begtin'
FROM `central`.`barcode` bar1
LEFT JOIN 
    (SELECT `barcode`.`artikelId`, `barcode`.`barcode` 
    FROM `central`.`barcode`, `central`.`besteleenheid`
    WHERE `barcode`.`besteleenheidid` = `besteleenheid`.`id`) BEbar 
    ON (`BEbar`.`artikelId` = `bar1`.`artikelid`)
INNER JOIN `central`.`artikel` art on `art`.`id` = `bar1`.`artikelid`
INNER JOIN 
    (SELECT bestelcodepartner, artikelid 
    FROM `central`.`winkelartikel` 
    GROUP BY  bestelcodepartner, artikelid) winkelart 
    ON (`winkelart`.`artikelId` = `bar1`.`artikelid`)
WHERE NOT EXISTS 
    (SELECT  * FROM `central`.`besteleenheid`
    WHERE `bar1`.`besteleenheidid` = `besteleenheid`.`id`)