select 
    artikelen.productcluster,
    artikelen.artikelnummer,
    artikelen.artikelomschrijving,
    artikelen.artikeltype,
    inventarisvw.volume as 'inventaris vorige week',
    ontvangen.volume as ontvangen,
    opgeboekt.volume as opgeboekt,
    verwaarden.volume as verwaard,
    omzet.volume as verkocht,
    sampling.volume as gesampled,
    derving.volume as gedorven,
    inventarisgisteren.volume as inventarisgisteren,
    gisteren.volume as 'ongeregistreerde derving in volume',
    format(gisteren.mw,2,'nl_NL') as 'ongeregistreerde derving in EUR',
    case
        when
            inventarisgisteren.volume is not null
                and inventarisvw.volume is not null
        then
            '2x geteld'
        when
            inventarisgisteren.volume is null
                and inventarisvw.volume is null
        then
            '0x geteld'
        when
            inventarisgisteren.volume is not null
                and inventarisvw.volume is null
        then
            '1x geteld; alleen gisteren'
        when
            inventarisgisteren.volume is null
                and inventarisvw.volume is not null
        then
            '1x geteld; alleen vorige week'
    end as aantalkeergeteld,
    case
        when
            round(ifnull(inventarisvw.volume, 0) - ifnull(opgeboekt.volume, 0) - ifnull(verwaarden.volume, 0) - ifnull(sampling.volume, 0) - ifnull(derving.volume, 0) - ifnull(omzet.volume, 0) + ifnull(ontvangen.volume, 0) - ifnull(inventarisgisteren.volume, 0) - ifnull(gisteren.volume, 0)) <> 0
                and inventarisgisteren.volume is not null
                and inventarisvw.volume is not null
        then
            '@finance: '
        when
            inventarisgisteren.volume = 0
                and inventarisvw.volume = 0
                and omzet.volume is null
        then
            'geen verkopen en inventarissen op 0; wordt dit artikel bnog verkocht?'
    end as 'Opmerking'
from
    (select 
        artikel.id as artikelid,
            artikel.artikelnummer,
            if(artikel.verkoopvariant = 1, 'verkoopvariant', if(artikel.inkoopvariant = 1, 'inkoopvariant', 'normaal')) as artikeltype,
            artikel.omschrijving1 as artikelomschrijving,
            og3.omschrijving as productcluster,
            og3.id as og3id
    from
        artikel, product, operationelegroep og2, operationelegroep og3
    where
        artikel.productid = product.id
            and product.operationelegroepid = og3.id
            and og3.operationelegroepparentid = og2.id
            and og2.id = 412
            and og3.id != 490
            and artikel.statusid in (3 , 4)) artikelen
        left join
    (select 
        artikelid,
            resultaat.totaalvolume as volume,
            resultaat.TotaalMarqtWaarde as mw
    from
        resultaat, artikel, rubriek
    where
        resultaat.artikelid = artikel.id
            and winkelid = 9
            and rubriekid = rubriek.id
            and rubriek.nummer = 4000
            and label = 'base'
            and datum = date_sub(curdate(), interval 1 day)) gisteren ON gisteren.artikelid = artikelen.artikelid
        left join
    (select 
        artikelid, sum(verkoopregel.Hoeveelheid) as volume
    from
        verkoop, verkoopregel, artikel, rubriek
    where
        verkoop.id = verkoopregel.verkoopid
            and verkoopregel.artikelid = artikel.id
            and verkoop.winkelid = 9
            and rubriekid = rubriek.id
            and rubriek.nummer = 3606
            and verkoop.datum between date_sub(curdate(), interval 8 day) and date_sub(curdate(), interval 2 day)
    group by artikelid) verwaarden ON verwaarden.artikelid = artikelen.artikelid
        left join
    (select 
        artikelid, sum(verkoopregel.Hoeveelheid) as volume
    from
        verkoop, verkoopregel, artikel, rubriek
    where
        verkoop.id = verkoopregel.verkoopid
            and rubriekid = rubriek.id
            and verkoopregel.artikelid = artikel.id
            and verkoop.winkelid = 9
            and rubriek.nummer = 100
            and verkoop.datum between date_sub(curdate(), interval 8 day) and date_sub(curdate(), interval 2 day)
    group by artikelid) omzet ON omzet.artikelid = artikelen.artikelid
        left join
    (select 
        artikelid, sum(verkoopregel.Hoeveelheid) as volume
    from
        verkoop, verkoopregel, artikel, rubriek
    where
        verkoop.id = verkoopregel.verkoopid
            and verkoopregel.artikelid = artikel.id
            and verkoop.winkelid = 9
            and rubriekid = rubriek.id
            and rubriek.nummer = 3600
            and verkoop.datum between date_sub(curdate(), interval 8 day) and date_sub(curdate(), interval 2 day)
    group by artikelid) sampling ON sampling.artikelid = artikelen.artikelid
        left join
    (select 
        artikelid, sum(verkoopregel.Hoeveelheid) as volume
    from
        verkoop, verkoopregel, rubriek, artikel
    where
        verkoop.id = verkoopregel.verkoopid
            and verkoopregel.artikelid = artikel.id
            and verkoop.winkelid = 9
            and rubriekid = rubriek.id
            and rubriek.derving = 1
            and verkoop.datum between date_sub(curdate(), interval 8 day) and date_sub(curdate(), interval 2 day)
    group by artikelid) derving ON derving.artikelid = artikelen.artikelid
        left join
    (select 
        artikelid, sum(verkoopregel.Hoeveelheid) as volume
    from
        verkoop, verkoopregel, artikel, rubriek
    where
        verkoop.id = verkoopregel.verkoopid
            and verkoopregel.artikelid = artikel.id
            and verkoop.winkelid = 9
            and rubriekid = rubriek.id
            and rubriek.nummer = 3699
            and verkoop.datum between date_sub(curdate(), interval 8 day) and date_sub(curdate(), interval 2 day)
    group by artikelid) opgeboekt ON opgeboekt.artikelid = artikelen.artikelid
        left join
    (select 
        artikelid, sum(totaalgeteld) as volume
    from
        inventaris, inventarisregel
    where
        inventaris.id = inventarisid
            and inventaris.type = 'DEELTELLING'
            and inventaris.winkelid = 9
            and date(statusdatumtijd) = date_sub(curdate(), interval 1 day)
    group by artikelid) inventarisgisteren ON inventarisgisteren.artikelid = artikelen.artikelid
        left join
    (select 
        artikelid, sum(totaalgeteld) as volume
    from
        inventaris, inventarisregel
    where
        inventaris.id = inventarisid
            and inventaris.winkelid = 9
            and inventaris.type = 'DEELTELLING'
            and date(statusdatumtijd) = date_sub(curdate(), interval 8 day)
    group by artikelid) inventarisvw ON inventarisvw.artikelid = artikelen.artikelid
        left join
    (select 
        artikelid, sum(aantalce) as volume
    from
        inkooporder, inkooporderregel
    where
        inkooporder.id = inkooporderregel.orderid
            and inkooporder.winkelid = 9
            and date(inkooporder.goederenontvangstdatumtijd) between date_sub(curdate(), interval 8 day) and date_sub(curdate(), interval 2 day)
    group by artikelid) ontvangen ON ontvangen.artikelid = artikelen.artikelid
order by aantalkeergeteld desc , productcluster asc, gisteren.mw desc