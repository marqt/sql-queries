select
    artikel.artikelNummer,
    winkelartikel.bestelCodePartner,
    if(Artikel.bestelbaar, 'bestelbaar', 'niet-bestelbaar') AS 'bestelbaar',
    artikel.omschrijving1 as 'artikelomschrijving',
	format(winkelartikel.prijsinkoop,2,'NL_nl') as 'prijsinkoop',
    (select 
            format(besteleenheid.BestelHoeveelheid,0) as 'bestelhoeveelheid'
        from
            besteleenheid
        where
            besteleenheid.artikelId = artikel.Id
        order by besteleenheid.Volgnummer asc , besteleenheid.id desc
        limit 1) as bestelHoeveelheid,
    (select 
            barcode.barcode
        from
            barcode
        where
            barcode.ArtikelId = artikel.Id
and barcode.datumvanaf <= curdate()
and barcode.besteleenheidid IS NULL
        order by barcode.datumvanaf DESC
        limit 1) as CEbarcode,
	    (select 
            barcode.barcode
        from
            barcode
        where
            barcode.ArtikelId = artikel.Id
and barcode.datumvanaf <= curdate()
and barcode.besteleenheidid IS NOT NULL
        order by barcode.datumvanaf DESC
        limit 1) as BEbarcode
from
    artikel,
    product,
    winkelartikel,
    winkelpartnerafdeling,
    partner,
    barcode,
    grondslag,
    meeteenheid,
    status,
    winkel
where
    artikel.productid = product.id
        and winkelartikel.artikelid = artikel.id
        and winkelpartnerafdeling.id = winkelartikel.winkelpartnerafdelingid
        and winkelpartnerafdeling.partnerid = partner.id
        and barcode.artikelid = artikel.id
        and winkelpartnerafdeling.winkelid = winkel.id
        and meeteenheid.id = product.inhoudmeeteenheidid
        and winkelartikel.statusid = status.id
        and partner.nummer = 48
and winkelartikel.bestelCodePartner NOT LIKE 'vv%'
		and artikel.bestelbaar = 1
		#and winkelartikel.statusid = 4
        and grondslag.id = product.grondslagId
group by artikel.id
order by artikel.omschrijving1