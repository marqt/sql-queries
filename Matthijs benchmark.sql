select 
    artikel.artikelnummer,
    artikel.omschrijving1,
    artikel.merk,
    product.inhoud,
    grondslag.code,
    concat(com1.Nummer,
            '.',
            com2.nummer,
            '.',
            com3.nummer,
            ' ',
            com1.Omschrijving,
            '-',
            com2.Omschrijving,
            '-',
            com3.Omschrijving) as commercielegroep,
    format(winkelartikel.marge / 100, 3) as 'BM',
    format(winkelartikel.prijsinkoop, 2) as 'MW',
    format(winkelartikel.prijsadviesincbtw,
        2) as 'VKP',
    winkel.nummer as 'winkelnummer',
    resultaat.week as 'week',
    format(sum(resultaat.totaalbedrag), 2) as 'Omzet',
    sum(resultaat.totaalmarqtvolume) as 'scans'
from
    resultaat,
    winkel,
    artikel,
    commercielegroep as com1,
    commercielegroep as com2,
    commercielegroep as com3,
    winkelartikel,
    product,
    grondslag
where
    resultaat.artikelid = artikel.id
        and resultaat.winkelid = winkel.id
        and artikel.id = winkelartikel.artikelid
        and winkel.id = winkelartikel.winkelid
        and artikel.productid = product.id
        and product.grondslagid = grondslag.id
        AND com3.id = product.commercieleGroepId
        AND com2.id = com3.commercieleGroepParentId
        AND com1.id = com2.commercieleGroepParentId
        and label = 'OPSW'
        and jaar = 2014
        and week = 8
        and artikel.artikelnummer in (140395,180005,100038,100269,601162,160735,103990,113106,491529,921028,201557,132329,125932,101246,115744,115610,112703,142003,125145,149770,224691,118989,133917,121084,104454,123039,111179,530226,116226,109121,224309,118998,115081,121288,484707,485395,104029,112624,125136,920342,140465,180157,360065,121358,520384,480309,116031,157904,110925,137467,140410,132408,224707,240059,122919,160726,104834,127480,110271,116323,159083,119009,560711,143354,149813 ,146483,160665,141992,144915,118916,301822,133078,153153,100117,116581,125792,920412,122636,118846,127057,140474,159913,100214,111203,360302,485836,119832,122195,224558,920962,121570,107565,102056,160489,111878,485854,112794,116040,128371,109316,121543,180041,118712,125428,122937,148465,114563,154592,145008,920555,140429,180023,159968,100728,152033,180139,115443,114998,103486,120104,140173,147965,152723,102755,123312,115805,224619,122177,142146,401540,224433,180032,138541,135379,157126,240022,240086,111133,100579,135227,160036,126469,123473,101015,158556,481517,106135,104816,128706,160328)
group by resultaat.week , resultaat.artikelid
order by artikel.artikelnummer