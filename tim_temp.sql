select 
    '1',
    year(curdate()),
    month(curdate()),
    week(curdate(), 3),
    winkelartikelen.nummer,
    winkelartikelen.artikelnummer,
    winkelartikelen.voorraadstand,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    if(winkelartikelen.nummer = 141,
        0,
        winkelartikelen.voorraadstand),
    NULL,
    NULL,
    resultaatdezemaand.TotaalVolume,
    NULL,
    winkelartikelen.omschrijving1,
    NULL,
    round(dayofmonth(curdate()) / 7 + 0.5),
    winkelartikelen.nummer,
    winkelartikelen.naam,
    (select 
            BestelHoeveelheid
        from
            besteleenheid
        where
            besteleenheid.ArtikelId = artikel.Id
                and DatumVanaf <= curdate()
        order by datumvanaf desc
        limit 1),
    (select 
            BestelHoeveelheid
        from
            besteleenheid
        where
            besteleenheid.ArtikelId = artikel.Id
                and DatumVanaf <= curdate()
        order by datumvanaf desc
        limit 1),
    NULL,
    winkelartikelen.PrijsInkoop,
    winkelartikelen.PrijsAdviesIncBtw,
    winkelartikelen.schapinhoud,
    '100%',
    winkelartikelen.BestelCodePartner,
    (select 
            Barcode
        from
            barcode
        where
            barcode.artikelid = artikel.id
                and DatumVanaf <= curdate()
                and BestelEenheidId is null
        order by datumvanaf desc
        limit 1),
    if(winkelartikelen.Seizoensassortiment = 1,
        'j',
        'n'),
    1,
    winkelartikelen.Bestelbaar,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    verkoopregels.count,
    verkoopregels.sum,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL
from
    (select 
        *
    from
        winkel, artikel, winkelartikel, winkelpartnerafdeling, partner, afdeling, product
    where
        winkelartikel.artikelid = artikel.id
            and winkelartikel.winkelid = winkel.id
            and winkelartikel.WinkelPartnerAfdelingId = winkelpartnerafdeling.Id
            and winkelpartnerafdeling.partnerid = partner.Id
            and winkelpartnerafdeling.afdelingid = afdeling.Id
            and product.id = artikel.productid
            and artikel.statusid in (3 , 4, 6)
    group by winkelartikel.id) as winkelartikelen
        inner join
    (select 
        winkelid, ArtikelId, sum(TotaalVolume) as totaalvolume
    from
        resultaat
    where
        resultaat.label = 'OPSD'
            and resultaat.jaar = 2014
            and resultaat.week = 29
    group by winkelid , ArtikelId) as resultaatdezemaand ON resultaatdezemaand.winkelid = winkelartikelen.winkelid
        and resultaatdezemaand.artikelid = winkelartikelen.artikelid
        inner join
    (select 
        verkoop.winkelid,
            verkoopregel.artikelid,
            count(verkoopregel.hoeveelheid) as count,
            sum(verkoopregel.hoeveelheid) as sum
    from
        verkoop, verkoopregel
    where
        verkoop.id = verkoopregel.verkoopid
            and verkoop.jaar = 2014
            and verkoop.week = 29
            and verkoopregel.rubriekId = 1
    group by verkoop.winkelid , verkoopregel.artikelid) as verkoopregels ON verkoopregels.winkelid = winkelartikelen.winkelid
        and verkoopregels.artikelid = winkelartikelen.artikelid