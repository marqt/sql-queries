select 
    3 as 'Recordtype',
    year(curdate()) as 'Jaar',
    month(curdate()) as 'Maand',
    week(curdate(), 3) as 'Week',
    concat(cast(winkel.nummer as char), '_') as 'Warehouse code',
    concat('GRO3-', og3.omschrijving) as 'Artikel Code',
    '' as 'Fysieke Voorraad',
    '' as 'Na te Leveren',
    '' as 'Totale voorraad in bestelling',
    '' as 'Aantal in bestelling',
    '' as 'Verwachte leverdatum',
    '' as 'Aantal in bestelling: (2)',
    '' as 'Verwachte leverdatum: (2)',
    '' as 'Aantal in bestelling: (3)',
    '' as 'Verwachte leverdatum: (3)',
    '' as 'Aantal in bestelling: (4)',
    '' as 'Verwachte leverdatum: (4)',
    '' as 'Aantal in bestelling: (5)',
    '' as 'Verwachte leverdatum: (5)',
    '' as 'Aantal in bestelling: (6)',
    '' as 'Verwachte leverdatum: (6)',
    '' as 'Criterium 1',
    '' as 'Criterium 2',
    '' as 'Criterium 3',
    '' as 'Criterium 4',
    '' as 'ABC Klasse',
    '' as 'Ijzeren voorraad',
    '' as 'Ijzeren voorraad type',
    '' as 'Bevestigde vraag',
    '' as 'Afzet Deze Maand',
    '' as 'Levertijd Afwijking',
    concat('Groepsartikel-', og3.omschrijving) as 'Artikel Omschrijving',
    '' as 'Cycle time',
    '' as 'dagen tot volgende besteldag na vandaag',
    '' as 'Service Level',
    round(dayofmonth(curdate()) / 7 + 0.5) as 'Run Code',
    concat('O3-', og3.nummer) as 'Leverancier Nr',
    concat('O3-', og3.omschrijving) as 'Leverancier',
    '' as 'MOQ',
    '' as 'IOQ',
    '' as 'Economische Bestelhoeveelheid',
    '' as 'Prijs',
    '' as 'Verkoop Prijs',
    '' as 'Analyse Code 1',
    '' as 'Analyse Code 2',
    '' as 'Analyse Code 3',
    '' as 'Analyse Code 4',
    '' as 'Analyse Code 5',
    '' as 'Analyse Code 6',
    '' as 'Prijs Per',
    'N' as 'Voorraad Artikel',
    '' as 'Analyse Code 7',
    '' as 'Analyse Code 8',
    '' as 'Analyse Code 9',
    '' as 'Analyse Code 10',
    '' as 'Analyse Code 11',
    '' as 'Analyse Code 12',
    '' as 'Analyse Code 13',
    '' as 'Analyse Code 14',
    '' as 'Analyse Code 15',
    '' as 'Analyse Code 16',
    '' as 'Analyse Code 17',
    '' as 'Analyse Code 18',
    '' as 'Analyse Code 19',
    '' as 'Analyse Code 20',
    '' as 'Analyse Code 21',
    '' as 'Voorloper / Opvolger',
    '' as 'PLC datum',
    '' as 'Orderregels vorige week',
    '' as 'Afzet vorige week',
    '' as 'PLC percentage',
    '' as 'EOQ Logistieke eenheid 1',
    '' as 'EOQ Logistieke eenheid 2',
    '' as 'EOQ Logistieke eenheid 3',
    '' as 'EOQ Logistieke eenheid 4',
    '' as 'EOQ Logistieke eenheid 5'
from
    winkel
        inner join
    winkelartikel ON winkel.id = winkelartikel.winkelid
        inner join
    artikel ON artikel.id = winkelartikel.artikelid
        inner join
    product ON product.id = artikel.ProductId
        inner join
    operationelegroep og3 ON og3.id = product.OperationeleGroepId
        inner join
    operationelegroep og2 ON og2.id = og3.OperationeleGroepParentId
        inner join
    operationelegroep og1 ON og1.id = og2.OperationeleGroepParentId
where
    og1.id = 402 and winkel.nummer != 103
group by winkel.id , og3.id
union all
select 
    3 as 'Recordtype',
    year(curdate()) as 'Jaar',
    month(curdate()) as 'Maand',
    week(curdate(), 3) as 'Week',
    '150_' as 'Warehouse code',
    concat('GRO3-', og3.omschrijving) as 'Artikel Code',
    '' as 'Fysieke Voorraad',
    '' as 'Na te Leveren',
    '' as 'Totale voorraad in bestelling',
    '' as 'Aantal in bestelling',
    '' as 'Verwachte leverdatum',
    '' as 'Aantal in bestelling: (2)',
    '' as 'Verwachte leverdatum: (2)',
    '' as 'Aantal in bestelling: (3)',
    '' as 'Verwachte leverdatum: (3)',
    '' as 'Aantal in bestelling: (4)',
    '' as 'Verwachte leverdatum: (4)',
    '' as 'Aantal in bestelling: (5)',
    '' as 'Verwachte leverdatum: (5)',
    '' as 'Aantal in bestelling: (6)',
    '' as 'Verwachte leverdatum: (6)',
    '' as 'Criterium 1',
    '' as 'Criterium 2',
    '' as 'Criterium 3',
    '' as 'Criterium 4',
    '' as 'ABC Klasse',
    '' as 'Ijzeren voorraad',
    '' as 'Ijzeren voorraad type',
    '' as 'Bevestigde vraag',
    '' as 'Afzet Deze Maand',
    '' as 'Levertijd Afwijking',
    concat('Groepsartikel-', og3.omschrijving) as 'Artikel Omschrijving',
    '' as 'Cycle time',
    '' as 'dagen tot volgende besteldag na vandaag',
    '' as 'Service Level',
    round(dayofmonth(curdate()) / 7 + 0.5) as 'Run Code',
    concat('O3-', og3.nummer) as 'Leverancier Nr',
    concat('O3-', og3.omschrijving) as 'Leverancier',
    '' as 'MOQ',
    '' as 'IOQ',
    '' as 'Economische Bestelhoeveelheid',
    '' as 'Prijs',
    '' as 'Verkoop Prijs',
    '' as 'Analyse Code 1',
    '' as 'Analyse Code 2',
    '' as 'Analyse Code 3',
    '' as 'Analyse Code 4',
    '' as 'Analyse Code 5',
    '' as 'Analyse Code 6',
    '' as 'Prijs Per',
    'N' as 'Voorraad Artikel',
    '' as 'Analyse Code 7',
    '' as 'Analyse Code 8',
    '' as 'Analyse Code 9',
    '' as 'Analyse Code 10',
    '' as 'Analyse Code 11',
    '' as 'Analyse Code 12',
    '' as 'Analyse Code 13',
    '' as 'Analyse Code 14',
    '' as 'Analyse Code 15',
    '' as 'Analyse Code 16',
    '' as 'Analyse Code 17',
    '' as 'Analyse Code 18',
    '' as 'Analyse Code 19',
    '' as 'Analyse Code 20',
    '' as 'Analyse Code 21',
    '' as 'Voorloper / Opvolger',
    '' as 'PLC datum',
    '' as 'Orderregels vorige week',
    '' as 'Afzet vorige week',
    '' as 'PLC percentage',
    '' as 'EOQ Logistieke eenheid 1',
    '' as 'EOQ Logistieke eenheid 2',
    '' as 'EOQ Logistieke eenheid 3',
    '' as 'EOQ Logistieke eenheid 4',
    '' as 'EOQ Logistieke eenheid 5'
from
    winkel
        inner join
    winkelartikel ON winkel.id = winkelartikel.winkelid
        inner join
    artikel ON artikel.id = winkelartikel.artikelid
        inner join
    product ON product.id = artikel.ProductId
        inner join
    operationelegroep og3 ON og3.id = product.OperationeleGroepId
        inner join
    operationelegroep og2 ON og2.id = og3.OperationeleGroepParentId
        inner join
    operationelegroep og1 ON og1.id = og2.OperationeleGroepParentId
where
    og1.id = 402 and winkel.nummer != 103
group by og3.id
