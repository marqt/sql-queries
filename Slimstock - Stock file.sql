select 
    3 as 'Recordtype',
    year(curdate()) as 'Jaar',
    month(curdate()) as 'Maand',
    week(curdate(), 3) as 'Week',
    concat(winkelartikelen.winkelnummer,'_') as 'Warehouse code',
    winkelartikelen.artikelnummer as 'Artikel Code',
    round(winkelartikelen.voorraadstand) as 'Fysieke Voorraad',
    '' as 'Na te Leveren',
    '' as 'Totale voorraad in bestelling',
    '' as 'Aantal in bestelling',
    '' as 'Verwachte leverdatum',
    '' as 'Aantal in bestelling: (2)',
    '' as 'Verwachte leverdatum: (2)',
    '' as 'Aantal in bestelling: (3)',
    '' as 'Verwachte leverdatum: (3)',
    '' as 'Aantal in bestelling: (4)',
    '' as 'Verwachte leverdatum: (4)',
    '' as 'Aantal in bestelling: (5)',
    '' as 'Verwachte leverdatum: (5)',
	'' as 'Aantal in bestelling: (5)',
    '' as 'Verwachte leverdatum: (5)',
    '' as 'Criterium 1',
    '' as 'Criterium 2',
    '' as 'Criterium 3',
    '' as 'Criterium 4',
    '' as 'ABC Klasse',
    if(winkelartikelen.winkelnummer = 141,
        0,
        round(winkelartikelen.schapinhoud)) as 'Ijzeren voorraad',
    '' as 'Ijzeren voorraad type',
    '' as 'Bevestigde vraag',
    round(resultaatdezemaand.TotaalVolume) as 'Afzet Deze Maand',
    '' as 'Levertijd Afwijking',
    winkelartikelen.artikelomschrijving as 'Artikel Omschrijving',
	ifnull((select 
                    orderschemaitem.besteldag+orderschemaitem.leverdagwinkel
                from
                    winkelpartnerafdeling,
                    levergroep,
                    orderschema,
                    orderschemaitem
                where
                     levergroep.id = ifnull(winkelartikelen.LeverGroepId,
                            winkelartikelen.DefaultLeverGroepId)
                        and orderschema.LeverGroepId = levergroep.Id
                        and orderschemaitem.OrderSchemaId = orderschema.Id
                        and orderschema.Actief = 1
                        and ((orderschema.IngangsDatum <= curdate()
                        and orderschema.EindDatum >= curdate())
                        or (orderschema.IngangsDatum <= curdate()
                        and orderschema.EindDatum is null))
                        and besteldag > weekday(curdate()) + 2
                order by BestelDag asc
                limit 0 , 1),
            (select 
                    orderschemaitem.besteldag+orderschemaitem.leverdagwinkel
                from
                    levergroep,
                    orderschema,
                    orderschemaitem
                where
                        levergroep.id = ifnull(winkelartikelen.LeverGroepId,
                            winkelartikelen.DefaultLeverGroepId)
                        and orderschema.LeverGroepId = levergroep.Id
                        and orderschemaitem.OrderSchemaId = orderschema.Id
                        and orderschema.Actief = 1
                        and ((orderschema.IngangsDatum <= date_add(curdate(),
                        interval (7 - weekday(curdate())) day)
                        and orderschema.EindDatum >= date_add(curdate(),
                        interval (7 - weekday(curdate())) day))
                        or (orderschema.IngangsDatum <= date_add(curdate(),
                        interval (7 - weekday(curdate())) day)
                        and orderschema.EindDatum is null))
                order by BestelDag asc
                limit 0 , 1)) as 'Levertijd: Leverdag_eerstvolgende_bestelling',
	ifnull((select 
                    orderschemaitem.besteldag
                from
                    winkelpartnerafdeling,
                    levergroep,
                    orderschema,
                    orderschemaitem
                where
                     levergroep.id = ifnull(winkelartikelen.LeverGroepId,
                            winkelartikelen.DefaultLeverGroepId)
                        and orderschema.LeverGroepId = levergroep.Id
                        and orderschemaitem.OrderSchemaId = orderschema.Id
                        and orderschema.Actief = 1
                        and ((orderschema.IngangsDatum <= curdate()
                        and orderschema.EindDatum >= curdate())
                        or (orderschema.IngangsDatum <= curdate()
                        and orderschema.EindDatum is null))
                        and besteldag > weekday(curdate()) + 2
                order by BestelDag asc
                limit 0 , 1),
            (select 
                    orderschemaitem.besteldag
                from
                    levergroep,
                    orderschema,
                    orderschemaitem
                where
                        levergroep.id = ifnull(winkelartikelen.LeverGroepId,
                            winkelartikelen.DefaultLeverGroepId)
                        and orderschema.LeverGroepId = levergroep.Id
                        and orderschemaitem.OrderSchemaId = orderschema.Id
                        and orderschema.Actief = 1
                        and ((orderschema.IngangsDatum <= date_add(curdate(),
                        interval (7 - weekday(curdate())) day)
                        and orderschema.EindDatum >= date_add(curdate(),
                        interval (7 - weekday(curdate())) day))
                        or (orderschema.IngangsDatum <= date_add(curdate(),
                        interval (7 - weekday(curdate())) day)
                        and orderschema.EindDatum is null))
                order by BestelDag asc
                limit 0 , 1)) as 'Cycle Tijd: Eerst volgende besteldag na vandaag',
    '' as 'Service Level',
    round(dayofmonth(curdate()) / 7 + 0.5) as 'Run Code',
    winkelartikelen.partnernummer as 'Leverancier Nr',
    winkelartikelen.partnernaam as 'Leverancier',
    (select 
            BestelHoeveelheid
        from
            besteleenheid
        where
            besteleenheid.ArtikelId = winkelartikelen.artikelid
                and DatumVanaf <= curdate()
        order by datumvanaf desc
        limit 1) as 'MOQ',
    (select 
            BestelHoeveelheid
        from
            besteleenheid
        where
            besteleenheid.ArtikelId = winkelartikelen.artikelid
                and DatumVanaf <= curdate()
        order by datumvanaf desc
        limit 1) as 'IOQ',
    '' as 'Economische Bestelhoeveelheid',
    winkelartikelen.PrijsInkoop as 'Prijs',
    winkelartikelen.PrijsAdviesIncBtw as 'Verkoop Prijs',
    winkelartikelen.afdelingsomschrijving as 'Analyse Code 1',
	winkelartikelen.facings as 'Analyse Code 3',
    '100%' as 'Analyse Code 3',
    winkelartikelen.BestelCodePartner as 'Analyse Code 4',
    (select 
            Barcode
        from
            barcode
        where
            barcode.artikelid = winkelartikelen.artikelid
                and DatumVanaf <= curdate()
                and BestelEenheidId is null
        order by datumvanaf desc
        limit 1) as 'Analyse Code 5',
    if(winkelartikelen.Seizoensassortiment = 1,
        'Y',
        'N') as 'Analyse Code 6',
    1 as 'Prijs Per',
    if(winkelartikelen.Bestelbaar = 1,'Y','N') as 'Voorraad Artikel',
    '' as 'Analyse Code 7',
    '' as 'Analyse Code 8',
    '' as 'Analyse Code 9',
    '' as 'Analyse Code 10',
    '' as 'Analyse Code 11',
    '' as 'Analyse Code 12',
    '' as 'Analyse Code 13',
    '' as 'Analyse Code 14',
    '' as 'Analyse Code 15',
    '' as 'Analyse Code 16',
    '' as 'Analyse Code 17',
    '' as 'Analyse Code 18',
    '' as 'Analyse Code 19',
    '' as 'Analyse Code 20',
    '' as 'Analyse Code 21',
    '' as 'Voorloper / Opvolger',
    '' as 'PLC datum',
    verkoopregels.count as 'Orderregels vorige week',
    verkoopregels.sum as 'Afzet vorige week',
    '' as 'PLC percentage',
    '' as 'EOQ Logistieke eenheid 1',
    '' as 'EOQ Logistieke eenheid 2',
    '' as 'EOQ Logistieke eenheid 3',
    '' as 'EOQ Logistieke eenheid 4',
    '' as 'EOQ Logistieke eenheid 5'
from
    (select 
        winkel.nummer as winkelnummer,
            winkel.id as winkelid,
            artikel.artikelnummer,
            winkelartikel.voorraadstand,
            artikel.omschrijving1 as artikelomschrijving,
            partner.nummer as partnernummer,
            partner.naam as partnernaam,
            artikel.id as artikelid,
            winkelartikel.PrijsInkoop,
            winkelartikel.PrijsAdviesIncBtw,
            winkelartikel.schapinhoud,
            winkelartikel.bestelcodepartner,
            product.Seizoensassortiment,
            artikel.bestelbaar,
			winkelartikel.LeverGroepId,
			winkelpartnerafdeling.DefaultLeverGroepId,
			afdeling.Omschrijving as afdelingsomschrijving,
			winkelartikel.facings
    from
        winkel, artikel, winkelartikel, winkelpartnerafdeling, partner, afdeling, product
    where
        winkelartikel.artikelid = artikel.id
            and winkelartikel.winkelid = winkel.id
            and winkelartikel.WinkelPartnerAfdelingId = winkelpartnerafdeling.Id
            and winkelpartnerafdeling.partnerid = partner.Id
            and winkelpartnerafdeling.afdelingid = afdeling.Id
            and product.id = artikel.productid
            and artikel.statusid in (3 , 4, 6)) as winkelartikelen
        inner join
    (select 
        winkelid, ArtikelId, sum(TotaalVolume) as totaalvolume
    from
        resultaat
    where
        resultaat.label = 'OPSD'
            and resultaat.jaar = 2014
            and resultaat.week = 29
    group by winkelid , ArtikelId) as resultaatdezemaand ON resultaatdezemaand.winkelid = winkelartikelen.winkelid
        and resultaatdezemaand.artikelid = winkelartikelen.artikelid
        inner join
    (select 
        verkoop.winkelid,
            verkoopregel.artikelid,
            count(verkoopregel.hoeveelheid) as count,
            sum(verkoopregel.hoeveelheid) as sum
    from
        verkoop, verkoopregel
    where
        verkoop.id = verkoopregel.verkoopid
            and verkoop.jaar = 2014
            and verkoop.week = 29
            and verkoopregel.rubriekId = 1
    group by verkoop.winkelid , verkoopregel.artikelid) as verkoopregels ON verkoopregels.winkelid = winkelartikelen.winkelid
        and verkoopregels.artikelid = winkelartikelen.artikelid
limit 200