SELECT
	artikel.artikelnummer,
    recordId,
    WaardeOud,
    WaardeNieuw,
    (WaardeNieuw-WaardeOud) as Verschil,
    date(historie.LaatsteWijziging)
FROM
    central.historie,winkelartikel, artikel
WHERE
	RecordId=winkelartikel.id
    and winkelartikel.ArtikelId=artikel.id
    and
    Tabel = 'Winkelartikel'
	AND Veld = 'prijsAdviesIncBtw'
    #dit zijn wijzigingen na 01-01-2018
    and historie.id > 115664699
    and date(historie.LaatsteWijziging) >= '2018-01-04'
    
    group by artikel.ArtikelNummer