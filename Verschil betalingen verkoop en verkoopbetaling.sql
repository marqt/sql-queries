SELECT 
    verkoopbetaling.week,
    winkel.nummer,
    verkoopbetaling.omzet,
    omzet.omzet,
    (verkoopbetaling.omzet - omzet.omzet) AS 'Verschil'
FROM
    (select 
        verkoop.winkelid,
            verkoop.week,
            sum(verkoopbetaling.bedrag) AS 'omzet'
    from
        verkoop
    inner join winkel ON winkel.id = verkoop.winkelid
    inner join verkoopbetaling ON verkoopbetaling.verkoopid = verkoop.id
    inner join Betaalmiddel ON Betaalmiddel.id = verkoopbetaling.betaalmiddelId
    where
        verkoop.TransactieSoort = 'KAS'
            and verkoop.jaar = 2013
            and verkoop.week between 40 and 41
    group by verkoop.week , verkoop.winkelid) verkoopbetaling,
    (SELECT 
        resultaat.winkelid,
            resultaat.week,
            sum(totaalbedrag) as 'Omzet'
    from
        resultaat
    where
        resultaat.label = 'OPSW' and jaar = 2013
            and week between 40 and 41
    group by resultaat.winkelid , resultaat.week) omzet,
    winkel
where
    verkoopbetaling.winkelid = omzet.winkelid
        and verkoopbetaling.week = omzet.week
        and verkoopbetaling.winkelid = winkel.id
        and (verkoopbetaling.omzet - omzet.omzet) > 0
order by verkoopbetaling.week