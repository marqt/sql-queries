select 
    resultaat.jaar,
    resultaat.periode,
    resultaat.week,
    winkel.nummer as 'Winkelnummer',
    resultaat.label,
    sum(Totaalbedrag) as Omzet,
    sum(Totaalbtw) as BTW,
    sum(totaalmarqtwaarde) as Marqtwaarde,
    sum(Totaalbedrag) - sum(Totaalbtw) - sum(totaalmarqtwaarde) as BrutoMarge,
    sum(Totaalbedrag) - sum(Totaalbtw) as NettoOmz,
    sum(TotaalMARQTvolume) as Volume
from
    resultaat
        inner join
    winkel ON winkel.id = resultaat.winkelid
        inner join
    partner ON resultaat.partnerid = partner.id
where
    resultaat.label in ('DPPW' , 'OPPW')
        and resultaat.jaar in (2012 , 2013)
group by resultaat.jaar , resultaat.week , winkel.nummer , resultaat.label