select 
    winkel.nummer,
    count(verkoop.id) as 'aantal toeristen',
    format(sum(verkoop.bedragtotaal),
        2,
        'nl_NL') as 'omzet toeristen',
    format(sum(verkoop.bedragtotaal) / count(verkoop.id),
        2,
        'nl_NL') as 'OPT toeristen'
from
    verkoop,
    winkel
where
    verkoop.winkelid = winkel.id
        and datum >= '2014-02-26'
        #and postcode REGEXP 
group by winkel.id
