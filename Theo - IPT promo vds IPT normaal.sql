select 
    winkel.nummer,
    verkoop.datum,
    format(sum(if(product.GrondslagId = 2,
            1,
            verkoopregel.Hoeveelheid)) / count(distinct (verkoop.id)),
        1) as 'IPT'
from
    verkoop,
    verkoopregel,
    artikel,
    product,
    winkel
where
    verkoop.id = verkoopregel.verkoopid
        and verkoopregel.artikelid = artikel.id
        and artikel.productid = product.id
        and verkoop.winkelid = winkel.Id
        and TransactieSoort = 'KAS'
        and jaar = year(curdate())
        and week = week(date_sub(curdate(), interval 1 week),
        3)
group by winkelid , datum