select 
    logistiekepartner.naam,
    administratievepartner.naam,
    levergroep.id,
    levergroep.naam,
    artikel.artikelnummer,
    artikel.omschrijving1,
    winkelartikelstatus.omschrijving,
	logistiekepartner.OrderEDIFACT
from
    winkelartikel,
    winkel,
    artikel,
    winkelpartnerafdeling,
    partner administratievepartner,
    levergroep,
    partner logistiekepartner,
    status winkelartikelstatus
where
    winkelartikel.winkelid = winkel.id
        and winkelartikel.artikelid = artikel.id
        and winkelartikel.statusid = winkelartikelstatus.Id
        and winkelartikel.winkelpartnerafdelingid = winkelpartnerafdeling.id
        and winkelpartnerafdeling.partnerid = administratievepartner.Id
        and ifnull(winkelartikel.levergroepId,
            winkelpartnerafdeling.DefaultLeverGroepId) = levergroep.id
        and levergroep.PartnerId = logistiekepartner.Id
        and winkel.nummer = 103
order by logistiekepartner.naam , administratievepartner.naam , artikel.omschrijving1
