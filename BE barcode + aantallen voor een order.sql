select 
    BEbesteld,
	(SELECT barcode FROM barcode where barcode.artikelid = inkooporderregel.artikelid and barcode.datumvanaf < now() order by datumvanaf desc limit 1)
from
    inkooporder,
    inkooporderregel
where
    inkooporder.id = inkooporderregel.orderid
        AND orderkenmerk = 17818