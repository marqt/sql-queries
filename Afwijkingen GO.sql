select 
    partner.naam as 'Leverancier',
    inkooporder.orderkenmerk as 'Ordernummer',
    inkooporder.goederenontvangstdatumtijd as 'Datum ontvangen',
	artikel.omschrijving1 as 'artikelomschrijving',
    format(inkooporderregel.CEbesteld,
        2,
        'nl_NL') as 'CEbesteld',
    format(inkooporderregel.MWbesteld,
        2,
        'nl_NL') as 'MWbesteld',
    format(inkooporderregel.CEgeleverd,
        2,
        'nl_NL') as 'CEgeleverd',
    format(inkooporderregel.MWgeleverd,
        2,
        'nl_NL') as 'MWgeleverd',
    format(if(inkooporderregel.MWgeleverd > inkooporderregel.MWbesteld,
            inkooporderregel.MWbesteld,
            inkooporderregel.MWgeleverd),
        2,
        'nl_NL') as 'max te factureren bedrag excl. BTW',
    format(if(inkooporderregel.CEgeleverd > inkooporderregel.CEbesteld,
            inkooporderregel.CEbesteld,
            inkooporderregel.CEgeleverd),
        2,
        'nl_NL') as 'max te factureren aantallen',
    winkelartikel.bestelcodepartner as 'Artikelcode leverancier',
    artikel.artikelnummer as 'Artikelcode'
from
    inkooporder,
    inkooporderregel,
    winkelartikel,
    artikel,
    partner
where
    inkooporder.id = inkooporderregel.orderid
        and inkooporderregel.artikelid = artikel.id
        and inkooporder.winkelid = winkelartikel.winkelid
        and artikel.id = winkelartikel.artikelid
        and inkooporder.partnerid = partner.id
		and ABS(inkooporderregel.CEgeleverd-inkooporderregel.CEbesteld) > 0.20*inkooporderregel.CEbesteld
        and partner.nummer = 121
        and inkooporder.goederenontvangstdatumtijd >= date_sub(curdate(), interval (7) week)
        and inkooporder.goederenontvangstdatumtijd <= date_sub(curdate(), interval (1) week)