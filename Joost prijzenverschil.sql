select 
    artikel.omschrijving1,
    huidig.prijsInkoop,
    eerder.prijsInkoop,
    huidig.prijsInkoop - eerder.prijsInkoop as verschil
from
    artikel
        inner join
    winkelartikel ON artikel.id = winkelartikel.artikelid
        inner join
    winkelpartnerafdeling ON winkelartikel.winkelpartnerafdelingid = winkelpartnerafdeling.id
        inner join
    partner ON winkelpartnerafdeling.partnerid = partner.id
        left join
    winkelartikelexporthistorie huidig ON winkelartikel.id = huidig.winkelartikelid
        left join
    winkelartikelexporthistorie eerder ON winkelartikel.id = eerder.winkelartikelid
where
    partner.nummer = 15
        and winkelpartnerafdeling.winkelid = 3
        and eerder.datumGeldigTot > '2014-03-01'
        and eerder.datumGeldigVanaf <= '2014-03-01'
        and huidig.datumGeldigTot > '2014-12-17'
        and huidig.datumGeldigVanaf <= '2014-12-17'
having verschil > 0