select 
    *
from
    (select 
        winkelid, afdelingid, count(distinct (verkoop.id))
    from
        verkoop, verkoopregel
    where
        verkoop.id = verkoopregel.verkoopid
            and verkoop.jaar = 2014
            and verkoop.week = week(date_sub(curdate(), interval 1 week), 3)
    group by verkoop.winkelid , verkoopregel.afdelingid) vorigeweekafdeling,
    (select 
        winkelid, count((verkoop.id))
    from
        verkoop
    where
        verkoop.jaar = 2014
            and verkoop.week = week(date_sub(curdate(), interval 1 week), 3)
    group by winkelid) as vorigeweekwinkel
where
    vorigeweekafdeling.winkelid = vorigeweekwinkel.winkelid