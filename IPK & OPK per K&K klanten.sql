select 
    count(verkoop.id),
    format((sum(verkoop.marqtvolume) / count(verkoop.id)),
        2) AS 'IKP',
    format((sum(verkoop.bedragtotaal) / count(verkoop.id)),
        2) as 'OPK',
    (select 
            sum(verreg.bedragtotaal)
        from
            verkoop ver,
            verkoopregel verreg
        where
            ver.id = verreg.verkoopid
                and ver.id = verkoop.id
                and verreg.afdelingid = 7),
    (select 
            sum(verreg.bedragtotaal)
        from
            verkoop ver,
            verkoopregel verreg
        where
            ver.id = verreg.verkoopid
                and ver.id = verkoop.id
                and verreg.afdelingid != 7) AS ''
from
    verkoop
where
    jaar = 2013 and week = 40
        and not exists( select distinct
            (verkoop.id)
        from
            verkoop ver,
            verkoopregel verreg
        where
            ver.id = verreg.verkoopid
                and verreg.afdelingid = 7
                and ver.jaar = verkoop.jaar
                and ver.week = verkoop.week
                and ver.id = verkoop.id)
