select 
    *
from
    verkoop,
	verkoopregel,
    winkel,
    artikel,
    operationelegroep og1,
    operationelegroep og2,
    operationelegroep og3,
	partner,
	rubriek
where
    verkoop.winkelid = winkel.id
        and verkoopregel.verkoopid = verkoop.id
        and verkoopregel.artikelid = artikel.id
        and verkoopregel.OperationeleGroepId = og3.id
        and og3.OperationeleGroepParentId = og2.id
        and og2.OperationeleGroepParentId = og1.id
and verkoopregel.rubriekid = rubriek.Id
and verkoopregel.partnerid = partner.Id
		and datum = curdate()
limit 10