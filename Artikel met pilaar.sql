SELECT

Omschrijving1 as Artikelomschrijving, 
ArtikelNummer,
winkelartikel.id,
og1.Omschrijving as Pilaar,
artikel.StatusId

FROM central.artikel
 inner join
    product ON product.id = artikel.ProductId
        inner join
    operationelegroep og3 ON og3.id = product.OperationeleGroepId
        inner join
    operationelegroep og2 ON og2.id = og3.OperationeleGroepParentId
        inner join
    operationelegroep og1 ON og1.id = og2.OperationeleGroepParentId
    join central.winkelartikel on winkelartikel.artikelid=artikel.id
 
WHERE

ArtikelNummer in (175317)
#winkelartikel.id in ()


order by pilaar