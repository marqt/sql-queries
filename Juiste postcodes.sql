select 
    winkel.nummer,
    verkoop.postcode,
    verkoop.bedragtotaal,
    verkoop.datum
from
    verkoop,
    winkel
where
    verkoop.winkelid = winkel.id
        and datum >= '2014-02-26'
        and postcode is not null