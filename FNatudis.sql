#Definieer artikelnummers:

SET @A = '175964';

#header vullen query

(select

'' as 'Artikelnummer Marqt',
'Material Description (Short Text)' as 'MAKTX',
'Numerator for Conversion to Base Units of Measure' as  'UMREZ',
'Alternative Unit of Measure for Stockkeeping Unit' as 'MEINH',
'International Article Number (EAN/UPC)' as  'EAN11',
'' as '',
'Country of origin' as 'HERKL',
'Min. Lotsize' as 'BSTMI',  
'Rounding value' as 'BSTRF',
'Planned Delivery Time in Days' as 'PLIFZ',
'Maximum storage period' as 'MAXLZ',
'Minimum Remaining Shelf Life' as 'MHDRZ',
'Certification' as "MVGR1")

#koppelen

union all


#Query 1 


(select 


#Artikelnummer Marqt
artikel.ArtikelNummer as "Artikelnummer Marqt",
#Artikelomschrijving
artikel.omschrijving1 as "MAKTX",
#1 van 1 stuks
1 as 'UMREZ',
#1 van 1 stuks
'stuks' as 'MEINH',
#Ean nummer collo (CE)
    (SELECT 
            barcode
        FROM
            barcode
        WHERE
            barcode.artikelid = artikel.id
                AND besteleenheidid IS NULL
                AND datumvanaf <= NOW()
        ORDER BY datumvanaf DESC , barcode.id ASC
        LIMIT 1) as "EAN11",
#ACTUELE BE
  (SELECT 
            barcode
        FROM
            barcode
        WHERE
            barcode.artikelid = artikel.id
                AND besteleenheidid IS NOT NULL
                AND datumvanaf <= NOW()
        ORDER BY datumvanaf DESC , barcode.id ASC
        LIMIT 1) AS '',
#Land van Oorsprong
land.Omschrijving as "HERKL",
#Bestelgroote
winkelartikel.bestelgrootte as 'BSTMI',  
#Bestelgroote nogmaals
winkelartikel.bestelgrootte as 'BSTRF',
#leadtime
'' as 'PLIFZ',
#tht aanlevering aan Marqt
product.houdbaarheid as 'MAXLZ',
#HoudbaarheidDC
product.houdbaarheidDC as 'MHDRZ',
#Certification
artikel.verkoopinformatie1 as "MVGR1"

from central.artikel

join artikeltype on artikel.artikelTypeId=artikeltype.id
join winkelartikel on winkelartikel.artikelid=artikel.id
join barcode CE on CE.ArtikelId=artikel.id and curdate() >= CE.datumvanaf and CE.besteleenheidId is null
join barcode BE on BE.ArtikelId=artikel.id and curdate() >= BE.datumvanaf and BE.besteleenheidId is not null 
join besteleenheid on artikel.id=besteleenheid.artikelid and curdate() >= besteleenheid.datumvanaf
join product on product.id=artikel.productid
join meeteenheid on product.inhoudmeeteenheidid=meeteenheid.id
join land on product.landid=land.id
join grondslag on grondslag.id=product.grondslagid
join btw on product.btwid=btw.id
    

where 

artikel.artikelnummer in (@A)

group by artikel.artikelnummer)