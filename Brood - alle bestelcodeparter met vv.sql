select 
    artikel.artikelnummer,
    artikel.omschrijving1,
    winkelartikel.bestelcodepartner,
    artikel.actief,
    artikel.inkoopvariant,
    artikel.verkoopvariant
from
    afdeling,
    winkelpartnerafdeling,
    winkelartikel,
    artikel
where
    afdeling.id = winkelpartnerafdeling.afdelingid
        and winkelpartnerafdeling.id = winkelartikel.winkelpartnerafdelingid
        and winkelartikel.artikelid = artikel.id
        and afdeling.nummer = 5
        and winkelartikel.bestelcodepartner like '%vv%'
group by artikel.id