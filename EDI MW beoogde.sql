select 
    count(distinct (inkooporder.id)),
    count(inkooporderregel.id),
    if(orderedifact = 1
            or partner.nummer in (401 , 96, 15),
        'Ja',
        'Nee') as EDI
from
    inkooporder,
    inkooporderregel,
    partner
where
    inkooporder.id = inkooporderregel.orderid
        and inkooporder.partnerid = partner.id
        and inkooporder.statusdatumtijd between date_sub(curdate(), interval 5 week) and date_sub(curdate(), interval 1 week)
group by EDI