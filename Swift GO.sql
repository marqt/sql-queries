select 
    inkooporder.orderkenmerk as 'OrderID',
    inkooporderregel.id as 'DeliveryLineID',
    winkelartikel.BestelCodePartner as 'DeliveryLineID',
    cast(if(partner.BEVerplicht,
        inkooporderregel.CEgeleverd / (inkooporderregel.CEbesteld / inkooporderregel.BEbesteld),
        inkooporderregel.CEgeleverd) AS DECIMAL(8,2)) as 'QtyDelivered',
    cast(inkooporderregel.CEgeleverd AS DECIMAL(8,2)) as 'QtyDeliveredCentric',
    cast(if(partner.BEVerplicht,
        inkooporderregel.CEgeleverd / (inkooporderregel.CEbesteld / inkooporderregel.BEbesteld),
        inkooporderregel.CEgeleverd) AS DECIMAL(8,2)) as 'QtyUnmatched',
    cast(if(partner.BEVerplicht,
        inkooporderregel.BEbesteld,
        inkooporderregel.CEbesteld) AS DECIMAL(8,2)) as 'QtyOrdered',
    cast(inkooporderregel.CEbesteld AS DECIMAL(8,2)) as 'QtyOrderedCentric',
    cast(inkooporderregel.BEbesteld AS DECIMAL(8,2)) as 'QtyOrderedBE',
   cast( if(partner.BEVerplicht,
        inkooporderregel.MWbesteld / inkooporderregel.BEbesteld,
        inkooporderregel.MWbesteld / inkooporderregel.CEbesteld) AS DECIMAL(8,2)) as 'unitPrice',
   cast( inkooporderregel.MWbesteld / inkooporderregel.CEbesteld AS DECIMAL(8,2)) as 'unitPriceCentric',
    cast(inkooporderregel.MWbesteld AS DECIMAL(37,2)) as 'LineAmount',
    4 as 'AdministrationID',
    partner.CrediteurNummer as 'Supplier',
    artikel.artikelnummer as 'ItemID',
    artikel.omschrijving1 as 'ItemName',
    product.inhoud as 'Volume',
    grondslag.Omschrijving as 'UnitOfMeasure',
    NULL as 'ProjID',
    '' as 'LedgerAccount',
    0 as 'Dimension1',
    NULL as 'Dimension2',
    '' as 'TaxCode',
    inkooporder.statusdatumtijd as 'Orderdate',
    winkel.nummer as 'Winkelnummer',
    if(partner.BEVerplicht, 'Be', 'Ce') as 'BeCe',
    NULL as 'Import_AdministrationID',
    NULL as 'Import_Supplier'
from
    inkooporder,
    inkooporderregel,
    winkelartikel,
    partner,
    artikel,
    product,
    grondslag,
    winkel
where
    inkooporder.id = inkooporderregel.orderid
        and winkelartikel.winkelid = inkooporder.winkelId
        and winkelartikel.ArtikelId = inkooporderregel.ArtikelId
        and winkelartikel.ArtikelId = artikel.Id
        and artikel.productid = product.Id
        and product.grondslagid = grondslag.id
        and inkooporder.partnerid = partner.id
        and inkooporder.winkelid = winkel.id
        and inkooporder.StatusId = 9
		and inkooporder.statusdatumtijd > date_sub(curdate(),interval 12 week)
#and winkel.nummer = 157
        #and orderkenmerk = 10008
#order by artikel.artikelnummer
limit 1000