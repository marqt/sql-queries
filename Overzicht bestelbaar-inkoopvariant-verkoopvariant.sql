select 
    afdeling.nummer,
    afdeling.omschrijving,
    artikel.artikelnummer,
    artikel.Omschrijving1,
if(artikel.bestelbaar = 1,"bestelbaar","nietbestelbaar") as "bestelbaar",
if(artikel.verkoopvariant = 1,"verkoopvariant","") as "verkoopvariant",
if(artikel.inkoopvariant = 1,"inkoopvariant","") as "inkoopvariant",
    partner.naam,
    winkelartikel.bestelcodepartner
from
    artikel
        inner join
    winkelartikel ON winkelartikel.artikelid = artikel.id
        inner join
    winkelpartnerafdeling ON winkelpartnerafdeling.id = winkelartikel.winkelpartnerafdelingid
        inner join
    afdeling ON winkelpartnerafdeling.afdelingId = afdeling.Id
        inner join
    partner ON partner.id = winkelpartnerafdeling.partnerid
        inner join
    winkel ON winkelartikel.winkelId = winkel.Id
where
    winkelartikel.winkelid = 3
        and artikel.statusid in (3 , 4)
        and afdeling.nummer = 5
order by afdeling.nummer , artikel.Omschrijving1