select 
    artikel.omschrijving1,
    artikel.artikelnummer,
    status.Omschrijving,
    historie.WaardeOud,
    historie.WaardeNieuw,
    date(historie.LaatsteWijziging)
from
    partner,
    winkelpartnerafdeling,
    winkelartikel,
    historie,
    artikel,
    status
where
    partner.id = winkelpartnerafdeling.partnerid
        and winkelpartnerafdeling.id = winkelartikel.WinkelPartnerAfdelingId
        and winkelartikel.artikelid = artikel.id
        and winkelartikel.id = historie.recordid
        and artikel.statusid = status.id
        and partner.nummer = 57
        and historie.tabel = 'winkelartikel'
        and historie.veld = 'prijsadviesincbtw'
        and historie.laatstewijziging > '2013-01-01 00:00:00'
group by artikel.id, date(historie.laatstewijziging)