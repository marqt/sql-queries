select 
    partner.naam,
	levergroep.naam,
    logistiekestroom.naam,
    orderschema . *,
    orderschemaitem . *,
    week(volgendebesteldatum, 3) as week
from
    orderschemaitem,
    orderschema,
    v_bestellevermomenttim,
    levergroep,
    partner,
    logistiekestroom
where
    orderschemaitem.id = v_bestellevermomenttim.orderschemaitemid
        and orderschema.id = v_bestellevermomenttim.orderschemaid
        and levergroep.id = orderschema.LeverGroepId
        and partner.id = levergroep.PartnerId
        and levergroep.logistiekeStroomId = logistiekestroom.id
        and volgendebesteldatum between '2016-12-19' and '2017-01-08'
		and logistiekestroom.naam = 'overslag'
		and levergroep.actief = 1

order by week, partner.id, levergroep.id, besteldag
