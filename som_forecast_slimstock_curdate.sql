SELECT 

    winkel.naam,
    Omschrijving1 AS Artikelomschrijving,
    ArtikelNummer,
    og1.Omschrijving AS Pilaar,
    sum(besteladviesregel.adviesCE) as ce,
    round(sum(besteladviesregel.adviesCE)*winkelartikel.PrijsInkoop,2) as marqtwaardeadvies

    
    
FROM
    central.artikel
        INNER JOIN
    product ON product.id = artikel.ProductId
        INNER JOIN
    operationelegroep og3 ON og3.id = product.OperationeleGroepId
        INNER JOIN
    operationelegroep og2 ON og2.id = og3.OperationeleGroepParentId
        INNER JOIN
    operationelegroep og1 ON og1.id = og2.OperationeleGroepParentId
        JOIN
    winkelartikel ON artikel.id = winkelartikel.ArtikelId
		JOIN
	besteladviesregel on besteladviesregel.winkelartikelId=winkelartikel.id
		join
	besteladvies on besteladviesregel.besteladviesId = besteladvies.id
		join
	besteladviesfile on besteladvies.besteladviesfileId=besteladviesfile.id
    join winkel on winkel.id=winkelartikel.WinkelId

WHERE

date(besteladviesfile.datum) = curdate() and winkel.Type='Winkel' 

group by winkel.nummer,ArtikelNummer

having marqtwaardeadvies> 200
     
Order by ArtikelNummer