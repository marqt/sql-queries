select 
    cg,
    week,
    grondslag,
    artikeltype,
    sum(Omzet2013) as 'Omzet2013',
    sum(Omzet2014) as 'Omzet2014',
    sum(Scans2013) as 'Scans2013',
    sum(Scans2014) as 'Scans2014',
    sum(Omzet2013) / sum(Scans2013) as PPI2013,
    sum(Omzet2014) / sum(Scans2014) as PPI2014
from
    (select 
        ifnull(max(Res2014.week), max(Res2013.week)) as week,
            artikel.artikelnummer,
            artikel.omschrijving1 as artikelomschrijving,
            grondslag.omschrijving as grondslag,
            case
                when
                    sum(Res2014.Totaalbedrag) is not null
                        and sum(Res2013.Totaalbedrag) is not null
                then
                    'identiek'
                when
                    sum(Res2014.Totaalbedrag) is not null
                        and sum(Res2013.Totaalbedrag) is null
                then
                    'nieuw'
                else 'gesaneerd'
            end as artikeltype,
            concat(com1.Nummer, '.', com2.nummer, '.', com3.nummer, ' ', com1.Omschrijving, '-', com2.Omschrijving, '-', com3.Omschrijving) as commercielegroep,
            com2.Omschrijving as cg,
            sum(Res2013.Totaalbedrag) as Omzet2013,
            sum(Res2013.Totaalbtw) as BTW2013,
            sum(Res2013.Totaalmarqtwaarde) as Marqtwaarde2013,
            sum(Res2013.Totaalvolume) as Volume2013,
            sum(Res2013.TotaalMarqtvolume) as scans2013,
            sum(Res2014.Totaalbedrag) as Omzet2014,
            sum(Res2014.Totaalbtw) as BTW2014,
            sum(Res2014.Totaalmarqtwaarde) as Marqtwaarde2014,
            sum(Res2014.Totaalvolume) as Volume2014,
            sum(Res2014.TotaalMarqtvolume) as scans2014
    from
        artikel
    inner join winkelartikel ON artikel.id = winkelartikel.ArtikelId
    inner join product ON product.id = artikel.productid
    inner join grondslag ON grondslag.id = product.grondslagid
    inner join commercielegroep as com3 ON com3.id = product.commercieleGroepId
    inner join commercielegroep as com2 ON com2.id = com3.commercieleGroepParentId
    inner join commercielegroep as com1 ON com1.id = com2.commercieleGroepParentId
    left join resultaat res2014 ON winkelartikel.artikelid = res2014.artikelid
        and winkelartikel.winkelid = res2014.winkelid
        and res2014.jaar = 2014
        and res2014.week = 48
        and res2014.label = 'OPSW'
    left join resultaat res2013 ON winkelartikel.artikelid = res2013.artikelid
        and winkelartikel.winkelid = res2013.winkelid
        and res2013.jaar = 2013
        and res2013.week = 48
        and res2013.label = 'OPSW'
    group by artikel.id
    having Omzet2013 > 0 or Omzet2014 > 0) res
group by cg , grondslag , artikeltype
order by cg , grondslag , artikeltype