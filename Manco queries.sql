select 
    inkooporder.orderkenmerk as 'Ordernummer',
    partner.naam as 'Leverancier',
    winkel.nummer as 'Kostenplaats',
    sum(inkooporderregel.MWbesteld) as 'MW besteld',
    sum(inkooporderregel.CEbesteld) as 'CE besteld',
    sum(inkooporderregel.MWgeleverd) as 'MW geleverd',
    sum(inkooporderregel.CEgeleverd) as 'CE geleverd',
    sum(if(inkooporderregel.MWgeleverd > 1.1 * inkooporderregel.MWbesteld,
        1.1 * inkooporderregel.MWbesteld,
        inkooporderregel.MWgeleverd)) as 'max te factureren bedrag',
    sum(if(inkooporderregel.CEgeleverd > 1.1 * inkooporderregel.CEbesteld,
        1.1 * inkooporderregel.CEbesteld,
        inkooporderregel.CEgeleverd)) as 'max te factureren aantallen',
    sum(if(inkooporderregel.MWgeleverd > 1.1 * inkooporderregel.MWbesteld,
        1.1 * inkooporderregel.MWbesteld,
        inkooporderregel.MWgeleverd) - inkooporderregel.MWgeleverd) as verschilbedrag,
    sum(if(inkooporderregel.CEgeleverd > 1.1 * inkooporderregel.CEbesteld,
        1.1 * inkooporderregel.CEbesteld,
        inkooporderregel.CEgeleverd) - inkooporderregel.CEgeleverd) as verschilaantal
from
    inkooporder,
    inkooporderregel,
    winkelartikel,
    artikel,
    partner,
    winkel,
    product,
    grondslag
where
    inkooporder.id = inkooporderregel.orderid
        and inkooporderregel.artikelid = artikel.id
        and inkooporder.winkelid = winkelartikel.winkelid
        and artikel.id = winkelartikel.artikelid
        and artikel.productid = product.id
        and product.grondslagid = grondslag.id
        and inkooporder.partnerid = partner.id
        and inkooporder.winkelid = winkel.id
        and inkooporder.goederenontvangstdatumtijd >= date_sub(curdate(), interval (7) week)
        and inkooporder.goederenontvangstdatumtijd <= date_sub(curdate(), interval (1) week)
        and ABS(inkooporderregel.CEgeleverd-inkooporderregel.CEbesteld) > 0

        and grondslag.code = 'st'
group by inkooporder.orderkenmerk , inkooporder.winkelid
having verschilbedrag < 0
order by verschilbedrag ASC