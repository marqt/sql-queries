select 
    partner.naam, levergroep.naam, orderschema.*, orderschemaitem.*
from
    partner,
    levergroep,
    v_bestellevermomenttim,
	orderschema,
	orderschemaitem
where
    partner.id = levergroep.partnerid
        and levergroep.id = v_bestellevermomenttim.levergroepid
		and orderschema.id = v_bestellevermomenttim.orderschemaid
		and v_bestellevermomenttim.orderschemaitemid = orderschemaitem.id
        and v_bestellevermomenttim.volgendebesteldatum between '2015-12-14' and '2015-12-20'