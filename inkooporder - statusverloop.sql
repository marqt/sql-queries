select 
    inkooporder.orderkenmerk,
    inkooporder.verwachteLeverDatumWinkelTijd,
    WinkelId,
    inkooporder.LeverGroepId,
    group_concat(inkooporderhistorie.StatusId),
    group_concat(inkooporderhistorie.Opmerking) as user
from
    inkooporder,
    inkooporderhistorie,
    partner
where
    inkooporder.partnerid = partner.Id
        and inkooporder.id = inkooporderhistorie.inkooporderId
        and partner.OrderEDIFACT = 1
        and date(statusdatumtijd) >= '2014-08-01'
        and inkooporder.StatusId = 9
        and inkooporderhistorie.StatusId = 8
        and winkelid not in (2 , 3, 6)
group by inkooporder.id
having user != "Door null"
order by inkooporder.LeverGroepId
