select 
    betaalmiddel.omschrijving, count(distinct(verkoop.id))
from
    verkoop,
    verkoopbetaling,
    betaalmiddel
where
    verkoop.id = verkoopbetaling.verkoopid
        and verkoopbetaling.betaalmiddelid = betaalmiddel.id
and verkoop.jaar = 2013
and verkoop.week = 46
and verkoopbetaling.betaalmiddelid != 1
group by betaalmiddel.id
limit 10