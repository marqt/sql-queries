/* -------------------------------------------------------------------------------
-- Author       Bob Hentenaar
-- Created      19/08/2017
-- Purpose      Query om een check te doen of forecast of masterdata goed staat              
--              Vooraad = 0 en adviesCE = 0
-- Copyright © 2017, Marqt B.V., All Rights Reserved
-------------------------------------------------------------------------------
-- Modification History
--
-- 05/07/0000  developer full name  
--      A comprehensive description of the changes. The description may use as 
--      many lines as needed.
---------------------------------------------------------------------------- */
SELECT 
    ArtikelNummer,
    Omschrijving1,
    winkel.Naam,
    voorraad,
    buffer,
    adviesCE,
    onderweg,
    forecastCE
FROM
    besteladviesregel,
    winkelartikel,
    artikel,
    winkel,
    besteladvies,
    besteladviesfile
WHERE
    winkelartikelId = winkelartikel.id
        AND winkelartikel.ArtikelId = artikel.id
        AND winkelartikel.winkelid = winkel.id
        AND besteladviesId = besteladvies.id
        AND besteladvies.besteladviesfileId = besteladviesfile.id
        AND voorraad <= 0
        AND adviesCE <= 0
        AND besteladviesfile.verwijderd = 0
        AND winkelartikel.Bestelbaar = 1
        AND DATE(besteladviesfile.laatsteWijziging) = CURDATE()