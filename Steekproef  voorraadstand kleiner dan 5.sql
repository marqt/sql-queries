select 
    artikel.omschrijving1,
    voorraadstand.voorraadstanddatum,
    voorraadstand.voorraadstand,
	voorraadstand.winkelartikelid
from
    voorraadstand,
    (select 
        max(voorraadstanddatum) as datum, winkelartikelid
    from
        voorraadstand
    group by winkelartikelid) maxdatum,
    winkelartikel,
    artikel,
    winkel
where
    voorraadstand.winkelartikelId = winkelartikel.Id
        and voorraadstand.voorraadstanddatum = maxdatum.datum
        and voorraadstand.winkelartikelId = maxdatum.winkelartikelId
        and winkelartikel.artikelid = artikel.id
        and winkelartikel.WinkelId = winkel.id
        and winkel.nummer = 159
        and artikel.statusid in (3 , 4)
        and artikel.verkoopvariant != 1
        #and voorraadstand.voorraadstand < 5