select 
    winkel.nummer,
    format(sum(verkoopregel.BedragInkoop * verkoopregel.Hoeveelheid),
        2) as marqtwaarde,
    date(verkoopregel.LaatsteWijziging) as verwerkingsdatum_in_MC,
	date(verkoop.datum) as dervingsdatum_in_mc
from
    verkoopregel,
    verkoop,
    artikel,
    rubriek,
    winkel
where
    verkoopregel.verkoopid = verkoop.id
        and verkoopregel.artikelid = artikel.id
        and verkoopregel.rubriekid = rubriek.Id
        and verkoop.winkelid = winkel.id
        and rubriek.derving = 1
        and verkoop.jaar = 2014
        and verkoop.week = 30
		and week(verkoopregel.LaatsteWijziging,3) != 30
group by verkoop.id