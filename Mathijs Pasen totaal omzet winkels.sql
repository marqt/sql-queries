select 
    winkel.nummer as 'winkelnummer',
    resultaat.week as 'week',
    sum(resultaat.totaalbedrag) as 'Omzet',
    sum(resultaat.totaalmarqtvolume) as 'scans'
from
    resultaat,
    winkel,
    artikel
where
    resultaat.artikelid = artikel.id
        and winkel.id = resultaat.winkelid
        and label = 'OPSW'
        and jaar = 2014
        and week >= 9
group by resultaat.week , resultaat.winkelid
order by resultaat.week , resultaat.winkelid