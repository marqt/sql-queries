/* -------------------------------------------------------------------------------
-- Author       Bob Hentenaar
-- Created      19/078/2017
-- Purpose      Query om een check te doen op het aantal uitgevoerde
--              inventarisaties
--              
-- Copyright © 2017, Marqt B.V., All Rights Reserved
-------------------------------------------------------------------------------
-- Modification History
--
-- 05/07/0000  developer full name  
--      A comprehensive description of the changes. The description may use as 
--      many lines as needed.
---------------------------------------------------------------------------- */
select
	winkel.naam,
    #og1.omschrijving as pilaar,
    date(inventaris.LaatsteWijziging) as datum,
    count(*) as aantalgeteld
from
    winkel,
    artikel,
    product,
    operationelegroep og1,
    operationelegroep og2,
    operationelegroep og3,
    inventaris,
    inventarisregel
where
    inventaris.id = inventarisregel.inventarisId
        and inventarisregel.artikelid = artikel.id
        and inventaris.winkelid = winkel.id
        and artikel.productid = product.id
        and product.OperationeleGroepId = og3.id
        and og3.OperationeleGroepParentId = og2.Id
        and og2.OperationeleGroepParentId = og1.Id
        and date(inventaris.laatstewijziging) >= date_sub(curdate(), interval 7 day)
        and inventaris.status = 'AKKOORD'
        and inventaris.type = 'DEELTELLING'
group by winkel.naam, 
#og1.id, 
date(inventaris.LaatsteWijziging)