SELECT 
    Shopname,
    EmployeeName,
    yearweek,
    Dayname,
    departmentname,
    RemunerationTypeName,
    sum(NettMinutes),
    sum(ExtraMinutes),
    sum(Allowance)
FROM
    datawarehouse.realizationemployeereport
where
    realizationemployeereport.year = 2016
        and realizationemployeereport.Week between 1 and 16
        and realizationemployeereport.RemunerationTypeName like '%toeslag%'
group by Shopname , EmployeeName , yearweek , PeriodDay , departmentname , RemunerationTypeName
order by Shopname , EmployeeName , yearweek , PeriodDay , departmentname , RemunerationTypeName