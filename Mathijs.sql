select 
    artikel.artikelnummer,
    artikel.omschrijving1,
    artikel.merk,
    product.inhoud,
    grondslag.code,
    format(winkelartikel.marge / 100, 3) as 'BM',
    format(winkelartikel.prijsinkoop, 2) as 'MW',
    format(winkelartikel.prijsadviesincbtw,
        2) as 'VKP',
    winkel.nummer as 'winkelnummer',
    resultaat.week as 'week',
    format(sum(resultaat.totaalbedrag), 2) as 'Omzet',
    sum(resultaat.totaalmarqtvolume) as 'scans'
from
    resultaat,
    winkel,
    artikel,
    winkelartikel,
    product,
    grondslag
where
    resultaat.artikelid = artikel.id
        and resultaat.winkelid = winkel.id
        and artikel.id = winkelartikel.artikelid
        and winkel.id = winkelartikel.winkelid
        and artikel.productid = product.id
        and product.grondslagid = grondslag.id
        and label = 'OPSW'
        and jaar = 2014
        and week = 10
        and artikel.artikelnummer in (132693 , 162294, 152422, 162258, 162267, 162276)
group by resultaat.week, resultaat.winkelid , resultaat.artikelid
order by artikel.artikelnummer