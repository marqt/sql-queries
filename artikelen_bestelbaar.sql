SELECT

Omschrijving1 as Artikelomschrijving, 
ArtikelNummer, 
inAssortiment as 'Datum vanaf bestelbaar'

FROM central.artikel
 
WHERE

inAssortiment >= DATE(CURDATE())
AND
inAssortiment < DATE(DATE_SUB(CURDATE(), INTERVAL -5 DAY))