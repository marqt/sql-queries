select 
    verkoop.id, winkelid, verkoopregel.bedragtotaal, verkoop.datum, verkoop.starttijd
from
    verkoop,
    verkoopregel
where
    verkoop.id = verkoopregel.verkoopid
        and verkoop.jaar = 2013
        and verkoop.week = 51
        and verkoopregel.artikelid = 8035 # 9392#
        and verkoop.transactiesoort = 'KAS'
#and verkoopregel.bedragtotaal in (2.42,2.64)
and winkelid in (1,3,9,10,11)


