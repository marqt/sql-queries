select 
    inkooporder.orderkenmerk,
    count(inkooporderregel.id) as orderregels,
    count(if(inkooporderregel.CEgeleverd = 0,
        inkooporderregel.id,
        null)) as mancosregels
from
    inkooporder,
    inkooporderregel
where
    inkooporder.id = inkooporderregel.orderid
        and inkooporder.statusdatumtijd > date_sub(curdate(), interval (1) week)
group by inkooporder.id
having orderregels = mancosregels