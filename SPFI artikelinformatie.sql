SELECT 
    CEbarcode.barcode,
    winkelartikelexporthistorie.bestelCodePartner,
    artikel.Omschrijving1,
    BEbarcode.BarCode
FROM
    winkelartikelexporthistorie
        inner join
    winkelartikel ON winkelartikelexporthistorie.winkelartikelid = winkelartikel.id
        inner join
    artikel ON winkelartikel.ArtikelId = artikel.Id
        left join
    barcode BEbarcode ON artikel.id = BEbarcode.ArtikelId
        and BEbarcode.BestelEenheidId is not null
        left join
    barcode CEbarcode ON artikel.id = CEbarcode.ArtikelId
        and CEbarcode.BestelEenheidId is null
where
    winkelartikel.WinkelId = 3
        and winkelartikelexporthistorie.datumGeldigVanaf < now()
        and winkelartikelexporthistorie.datumGeldigTot > now()