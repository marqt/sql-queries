select artikel.artikelnummer, artikel.omschrijving1, winkel.nummer, resultaat.week, rubriek.nummer, format(sum(totaalmarqtwaarde),2,'NL_nl'), sum(totaalmarqtvolume) 
from artikel, winkelartikel, levergroep, resultaat, winkel, rubriek
where artikel.id = winkelartikel.artikelid
and winkelartikel.levergroepid = levergroep.id
and artikel.id = resultaat.artikelid
and resultaat.winkelid = winkel.id
and resultaat.rubriekid = rubriek.id
and levergroep.id in (96,166,190,191,432)
and resultaat.jaar = 2013
and resultaat.week = 38
and resultaat.label = 'BASE'
group by artikel.id, winkel.id, rubriek.id, resultaat.week
limit 10