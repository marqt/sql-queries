select winkel.nummer, verkoop.kassaid, verkoop.marqtvolume, verkoop.datum, format(verkoop.bedragtotaal,2,"NL_nl"), verkoop.uur
from verkoop, winkel
where verkoop.winkelid = winkel.id
and winkel.nummer = 151
and verkoop.jaar = 2013
and verkoop.week = 39
and verkoop.transactiesoort = 'KAS'