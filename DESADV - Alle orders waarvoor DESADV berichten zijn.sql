select 
    inkooporder.orderkenmerk,
par.Naam,
partner.Naam
from
    inkooporder,
    inkooporderhistorie,
	partner,
levergroep,
partner par
where
    inkooporder.id = inkooporderhistorie.inkooporderid
and inkooporder.partnerId = partner.Id
and inkooporder.LeverGroepId = levergroep.Id
and levergroep.PartnerId = par.Id
and inkooporderhistorie.statusId = 5
and inkooporder.statusdatumtijd > date_sub(curdate(),interval(1)month)