# Query om een dagfile uit het verleden op te halen
# Geef on de Parameter @ via de interval aan hoveel dagen je terug wilt vanaf vandaag(curdate)
# bestand opslaan zoner headers als tab gescheiden txt file
set @dagdatum = DATE_SUB(CURDATE(), INTERVAL 8 DAY);

#Dagfile query slimstock
(select 
    CONCAT(CAST(winkelnummer AS CHAR), '_') AS 'Warehouse code',
    artikelnummer AS 'Artikel Code',
    date_format(datum, '%Y%m%d') as 'AfzetDatum',
    IFNULL(round(sum(dagafzet), 1), 0) AS 'DagAfzet',
    IFNULL(sum(DagRegels), 0) AS 'DagRegels',
    IFNULL(round(sum(derving), 1), 0) AS 'Derving',
    IFNULL(round(sum(Neeverkopen), 1), 0) AS 'Neeverkopen',
    0 AS 'Promotionele verkopen',
    0 AS 'Open',
    max(open) AS 'Afzetkanaal 1',
    '' AS 'Afzetkanaal 2',
    '' AS 'Afzetkanaal 3',
    '' AS 'Afzetkanaal 4',
    '' AS 'Afzetkanaal 5'
from
    ((select 
        winkel.nummer as winkelnummer,
            artikel.artikelnummer,
            verkoop.datum,
            sum(case
                when
                    verkoopregel.rubriekid = 1
                        and product.GrondslagId = 2
                        and verkoopregel.hoeveelheid > 0
                then
                    1
                when
                    verkoopregel.rubriekid = 1
                        and product.GrondslagId = 2
                        and verkoopregel.hoeveelheid < 0
                then
                    - 1
                when
                    verkoopregel.rubriekid = 1
                        and product.GrondslagId = 2
                        and verkoopregel.hoeveelheid = 0
                then
                    0
                when
                    verkoopregel.rubriekid = 1
                        and product.GrondslagId = 1
                then
                    verkoopregel.Hoeveelheid
                else 0
            end) as DagAfzet,
            count(distinct (if(verkoop.transactiesoort = 'KAS',verkoop.id,null))) AS DagRegels,
            sum(case
                when
                    rubriekid in (12 , 23)
                        and product.GrondslagId = 2
                        and hoeveelheid > 0
                then
                    verkoopregel.Hoeveelheid / ifnull(product.normgewicht, product.inhoud) * 1000
                when
                    rubriekid in (12 , 23)
                        and product.GrondslagId = 2
                        and hoeveelheid < 0
                then
                    -1 * verkoopregel.Hoeveelheid / ifnull(product.normgewicht, product.inhoud) * 1000
                when
                    rubriekid in (12 , 23)
                        and product.GrondslagId = 2
                        and hoeveelheid = 0
                then
                    0
                when
                    rubriekid in (12 , 23)
                        and product.GrondslagId = 1
                then
                    verkoopregel.Hoeveelheid
                else 0
            end) AS Derving,
            sum(case
                when
                    rubriekid = 20
                        and product.GrondslagId = 2
                        and hoeveelheid > 0
                then
                    1
                when
                    rubriekid = 20
                        and product.GrondslagId = 2
                        and hoeveelheid < 0
                then
                    - 1
                when
                    rubriekid = 20
                        and product.GrondslagId = 2
                        and hoeveelheid = 0
                then
                    0
                when
                    rubriekid = 20
                        and product.GrondslagId = 1
                then
                    verkoopregel.Hoeveelheid
                else 0
            end) AS Neeverkopen,
            max(verkoop.uur) AS Open
    from
        winkelartikel
    inner join winkel ON winkel.id = winkelartikel.winkelid
    inner join artikel ON artikel.id = winkelartikel.artikelid
    inner join product ON product.id = artikel.productid
    inner join verkoop ON verkoop.winkelid = winkelartikel.winkelId
    inner join verkoopregel ON verkoopregel.verkoopid = verkoop.id
        and verkoopregel.artikelid = winkelartikel.artikelid
    where
        verkoop.datum = @dagdatum
            and not exists( select 
                *
            from
                verhoudingartikelnaarartikel
            where
                verhoudingartikelnaarartikel.VerhoudingArtikelId = artikel.id)
    group by winkelartikel.id
    having (DagAfzet <> 0 or Derving <> 0)) union all (select 
        winkel.nummer as winkelnummer,
            artnaar.artikelnummer,
            date_format(verkoop.datum, '%Y%m%d') as datum,
            sum(case
                when
                    verkoopregel.rubriekid = 1
                        and product.GrondslagId = 2
                        and verkoopregel.hoeveelheid > 0
                then
                    1 * verhoudingartikelnaarartikel.AantalNaar
                when
                    verkoopregel.rubriekid = 1
                        and product.GrondslagId = 2
                        and verkoopregel.hoeveelheid < 0
                then
                    - 1 * verhoudingartikelnaarartikel.AantalNaar
                when
                    verkoopregel.rubriekid = 1
                        and product.GrondslagId = 2
                        and verkoopregel.hoeveelheid = 0
                then
                    0
                when
                    verkoopregel.rubriekid = 1
                        and product.GrondslagId = 1
                then
                    verkoopregel.Hoeveelheid * verhoudingartikelnaarartikel.AantalNaar
                else 0
            end) as DagAfzet,
            count(distinct (if(verkoop.transactiesoort = 'KAS',verkoop.id,null))) AS DagRegels,
            sum(case
                when
                    rubriekid in (12 , 23)
                        and product.GrondslagId = 2
                        and hoeveelheid > 0
                then
                    1 * verkoopregel.Hoeveelheid / ifnull(product.normgewicht, product.inhoud) * 1000 * verhoudingartikelnaarartikel.AantalNaar
                when
                    rubriekid in (12 , 23)
                        and product.GrondslagId = 2
                        and hoeveelheid < 0
                then
                    -1 * verkoopregel.Hoeveelheid / ifnull(product.normgewicht, product.inhoud) * 1000 * verhoudingartikelnaarartikel.AantalNaar
                when
                    rubriekid in (12 , 23)
                        and product.GrondslagId = 2
                        and hoeveelheid = 0
                then
                    0
                when
                    rubriekid in (12 , 23)
                        and product.GrondslagId = 1
                then
                    verkoopregel.Hoeveelheid * verhoudingartikelnaarartikel.AantalNaar
                else 0
            end) AS Derving,
            sum(case
                when
                    rubriekid = 20
                        and product.GrondslagId = 2
                        and hoeveelheid > 0
                then
                    1 * verhoudingartikelnaarartikel.AantalNaar
                when
                    rubriekid = 20
                        and product.GrondslagId = 2
                        and hoeveelheid < 0
                then
                    - 1 * verhoudingartikelnaarartikel.AantalNaar
                when
                    rubriekid = 20
                        and product.GrondslagId = 2
                        and hoeveelheid = 0
                then
                    0
                when
                    rubriekid = 20
                        and product.GrondslagId = 1
                then
                    verkoopregel.Hoeveelheid * verhoudingartikelnaarartikel.AantalNaar
                else 0
            end) AS Neeverkopen,
            max(verkoop.uur) AS Open
    from
        winkelartikel
    inner join winkel ON winkel.id = winkelartikel.winkelid
    inner join artikel artvan ON artvan.id = winkelartikel.artikelid
    inner join verhoudingartikelnaarartikel ON verhoudingartikelnaarartikel.VerhoudingArtikelId = artvan.id
    inner join artikel artnaar ON artnaar.id = verhoudingartikelnaarartikel.ArtikelNaarId
    inner join product ON product.id = artvan.productid
    inner join verkoop ON verkoop.winkelid = winkelartikel.winkelId
    inner join verkoopregel ON verkoopregel.verkoopid = verkoop.id
        and verkoopregel.artikelid = winkelartikel.artikelid
    where
        verkoop.datum = @dagdatum
    group by winkelartikel.id
    having DagAfzet <> 0 or Derving <> 0)) alles
group by winkelnummer , artikelnummer) union all (select 
    '150_' AS 'Warehouse code',
    artikelnummer AS 'Artikel Code',
    date_format(datum, '%Y%m%d') as 'AfzetDatum',
    IFNULL(round(sum(dagafzet), 1), 0) AS 'DagAfzet',
    IFNULL(sum(DagRegels), 0) AS 'DagRegels',
    IFNULL(round(sum(derving), 1), 0) AS 'Derving',
    IFNULL(round(sum(Neeverkopen), 1), 0) AS 'Neeverkopen',
    0 AS 'Promotionele verkopen',
    0 AS 'Open',
    max(open) AS 'Afzetkanaal 1',
    '' AS 'Afzetkanaal 2',
    '' AS 'Afzetkanaal 3',
    '' AS 'Afzetkanaal 4',
    '' AS 'Afzetkanaal 5'
from
    ((select 
        winkel.nummer as winkelnummer,
            artikel.artikelnummer,
            verkoop.datum,
            sum(case
                when
                    verkoopregel.rubriekid = 1
                        and product.GrondslagId = 2
                        and verkoopregel.hoeveelheid > 0
                then
                    1
                when
                    verkoopregel.rubriekid = 1
                        and product.GrondslagId = 2
                        and verkoopregel.hoeveelheid < 0
                then
                    - 1
                when
                    verkoopregel.rubriekid = 1
                        and product.GrondslagId = 2
                        and verkoopregel.hoeveelheid = 0
                then
                    0
                when
                    verkoopregel.rubriekid = 1
                        and product.GrondslagId = 1
                then
                    verkoopregel.Hoeveelheid
                else 0
            end) as DagAfzet,
            count(distinct (if(verkoop.transactiesoort = 'KAS',verkoop.id,null))) AS DagRegels,
            sum(case
                when
                    rubriekid in (12 , 23)
                        and product.GrondslagId = 2
                        and hoeveelheid > 0
                then
                    1 * verkoopregel.Hoeveelheid / ifnull(product.normgewicht, product.inhoud) * 1000
                when
                    rubriekid in (12 , 23)
                        and product.GrondslagId = 2
                        and hoeveelheid < 0
                then
                    -1 * verkoopregel.Hoeveelheid / ifnull(product.normgewicht, product.inhoud) * 1000
                when
                    rubriekid in (12 , 23)
                        and product.GrondslagId = 2
                        and hoeveelheid = 0
                then
                    0
                when
                    rubriekid in (12 , 23)
                        and product.GrondslagId = 1
                then
                    verkoopregel.Hoeveelheid
                else 0
            end) AS Derving,
            sum(case
                when
                    rubriekid = 20
                        and product.GrondslagId = 2
                        and hoeveelheid > 0
                then
                    1
                when
                    rubriekid = 20
                        and product.GrondslagId = 2
                        and hoeveelheid < 0
                then
                    - 1
                when
                    rubriekid = 20
                        and product.GrondslagId = 2
                        and hoeveelheid = 0
                then
                    0
                when
                    rubriekid = 20
                        and product.GrondslagId = 1
                then
                    verkoopregel.Hoeveelheid
                else 0
            end) AS Neeverkopen,
            max(verkoop.uur) AS Open
    from
        winkelartikel
    inner join winkel ON winkel.id = winkelartikel.winkelid
    inner join artikel ON artikel.id = winkelartikel.artikelid
    inner join product ON product.id = artikel.productid
    inner join verkoop ON verkoop.winkelid = winkelartikel.winkelId
    inner join verkoopregel ON verkoopregel.verkoopid = verkoop.id
        and verkoopregel.artikelid = winkelartikel.artikelid
    where
        verkoop.datum = @dagdatum
            and not exists( select 
                *
            from
                verhoudingartikelnaarartikel
            where
                verhoudingartikelnaarartikel.VerhoudingArtikelId = artikel.id)
    group by artikel.id
    having (DagAfzet <> 0 or Derving <> 0)) union all (select 
        winkel.nummer as winkelnummer,
            artnaar.artikelnummer,
            verkoop.datum,
            sum(case
                when
                    verkoopregel.rubriekid = 1
                        and product.GrondslagId = 2
                        and verkoopregel.hoeveelheid > 0
                then
                    1 * verhoudingartikelnaarartikel.AantalNaar
                when
                    verkoopregel.rubriekid = 1
                        and product.GrondslagId = 2
                        and verkoopregel.hoeveelheid < 0
                then
                    - 1 * verhoudingartikelnaarartikel.AantalNaar
                when
                    verkoopregel.rubriekid = 1
                        and product.GrondslagId = 2
                        and verkoopregel.hoeveelheid = 0
                then
                    0
                when
                    verkoopregel.rubriekid = 1
                        and product.GrondslagId = 1
                then
                    verkoopregel.Hoeveelheid * verhoudingartikelnaarartikel.AantalNaar
                else 0
            end) as DagAfzet,
            count(distinct (if(verkoop.transactiesoort = 'KAS',verkoop.id,null))) AS DagRegels,
            sum(case
                when
                    rubriekid in (12 , 23)
                        and product.GrondslagId = 2
                        and hoeveelheid > 0
                then
                    1 * verkoopregel.Hoeveelheid / ifnull(product.normgewicht, product.inhoud) * 1000 * verhoudingartikelnaarartikel.AantalNaar
                when
                    rubriekid in (12 , 23)
                        and product.GrondslagId = 2
                        and hoeveelheid < 0
                then
                    -1 * verkoopregel.Hoeveelheid / ifnull(product.normgewicht, product.inhoud) * 1000 * verhoudingartikelnaarartikel.AantalNaar
                when
                    rubriekid in (12 , 23)
                        and product.GrondslagId = 2
                        and hoeveelheid = 0
                then
                    0
                when
                    rubriekid in (12 , 23)
                        and product.GrondslagId = 1
                then
                    verkoopregel.Hoeveelheid * verhoudingartikelnaarartikel.AantalNaar
                else 0
            end) AS Derving,
            sum(case
                when
                    rubriekid = 20
                        and product.GrondslagId = 2
                        and hoeveelheid > 0
                then
                    1 * verhoudingartikelnaarartikel.AantalNaar
                when
                    rubriekid = 20
                        and product.GrondslagId = 2
                        and hoeveelheid < 0
                then
                    - 1 * verhoudingartikelnaarartikel.AantalNaar
                when
                    rubriekid = 20
                        and product.GrondslagId = 2
                        and hoeveelheid = 0
                then
                    0
                when
                    rubriekid = 20
                        and product.GrondslagId = 1
                then
                    verkoopregel.Hoeveelheid * verhoudingartikelnaarartikel.AantalNaar
                else 0
            end) AS Neeverkopen,
            max(verkoop.uur) AS Open
    from
        winkelartikel
    inner join winkel ON winkel.id = winkelartikel.winkelid
    inner join artikel artvan ON artvan.id = winkelartikel.artikelid
    inner join verhoudingartikelnaarartikel ON verhoudingartikelnaarartikel.VerhoudingArtikelId = artvan.id
    inner join artikel artnaar ON artnaar.id = verhoudingartikelnaarartikel.ArtikelNaarId
    inner join product ON product.id = artvan.productid
    inner join verkoop ON verkoop.winkelid = winkelartikel.winkelId
    inner join verkoopregel ON verkoopregel.verkoopid = verkoop.id
        and verkoopregel.artikelid = winkelartikel.artikelid
    where
        verkoop.datum = @dagdatum
    group by artnaar.id
    having DagAfzet <> 0 or Derving <> 0)) alles
group by artikelnummer) union all (SELECT 
    '141_' AS 'Warehouse code',
    artikel.artikelnummer AS 'Artikel Code',
    date_format(@dagdatum,
            '%Y%m%d') as 'AfzetDatum',
    IFNULL(SUM(afzet.TotaalMarqtVolume), 0) AS 'DagAfzet',
    IFNULL(count(distinct (winkel.nummer)), 0) AS 'DagRegels',
    IFNULL(SUM(derving.TotaalMarqtVolume), 0) AS 'Derving',
    IFNULL(SUM(korting.TotaalMarqtVolume), 0) AS 'Neeverkopen',
    0 AS 'Promotionele verkopen',
    0 AS 'Open',
    0 AS 'Afzetkanaal 1',
    '' AS 'Afzetkanaal 2',
    '' AS 'Afzetkanaal 3',
    '' AS 'Afzetkanaal 4',
    '' AS 'Afzetkanaal 5'
FROM
    winkelartikel
        INNER JOIN
    winkelpartnerafdeling ON winkelartikel.WinkelPartnerAfdelingId = winkelpartnerafdeling.id
        INNER JOIN
    levergroep ON IFNULL(winkelartikel.levergroepId,
            winkelpartnerafdeling.DefaultLeverGroepId) = levergroep.id
        INNER JOIN
    partner ON partner.id = levergroep.PartnerId
        LEFT JOIN
    resultaat afzet ON afzet.datum = @dagdatum
        AND afzet.WinkelId = winkelartikel.WinkelId
        AND afzet.ArtikelId = winkelartikel.ArtikelId
        AND afzet.label = 'OPSD'
        LEFT JOIN
    resultaat derving ON (derving.Datum = @dagdatum
        AND derving.WinkelId = winkelartikel.WinkelId
        AND derving.ArtikelId = winkelartikel.ArtikelId
        AND derving.label = 'BASE'
        AND derving.rubriekid in (12 , 23))
        LEFT JOIN
    resultaat korting ON (korting.Datum = @dagdatum
        AND korting.WinkelId = winkelartikel.WinkelId
        AND korting.ArtikelId = winkelartikel.ArtikelId
        AND korting.label = 'BASE'
        AND korting.rubriekid = 20)
        INNER JOIN
    winkel ON winkel.id = winkelartikel.winkelid
        INNER JOIN
    artikel ON artikel.id = winkelartikel.artikelid
WHERE
    partner.nummer = 96
GROUP BY winkelartikel.artikelid
HAVING (DagAfzet <> 0 OR Derving <> 0))