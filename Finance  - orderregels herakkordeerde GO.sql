select 
    inkooporder.orderkenmerk,
    artikel.omschrijving1,
    inkooporderstatus.naam,
    inkooporderregelhistorie.aantalCE,
    inkooporderregelhistorie.aantalBE,
    inkooporderregelhistorie.marqtWaarde,
	inkooporderregelhistorie.LaatsteWijziging
from
    inkooporder,
    inkooporderhistorie,
    inkooporderstatus,
    inkooporderregel,
    inkooporderregelhistorie,
    artikel
where
    inkooporder.id = inkooporderhistorie.inkooporderid
        and inkooporder.id = inkooporderregel.orderid
        and inkooporderregel.id = inkooporderregelhistorie.inkooporderregelId
        and inkooporderregelhistorie.inkooporderhistorieid = inkooporderhistorie.id
        and inkooporderhistorie.statusid = inkooporderstatus.id
        and inkooporderregel.artikelid = artikel.id
        and inkooporder.orderkenmerk = 150126
        and inkooporderhistorie.statusid = 9
order by artikel.id, inkooporderregelhistorie.LaatsteWijziging asc


