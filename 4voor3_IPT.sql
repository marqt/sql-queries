select * from (select 
    winkel.nummer,
    verkoop.datum,
    count(distinct (verkoop.id)) as klanten,
    format(sum(if(product.GrondslagId = 2,
            1,
            verkoopregel.Hoeveelheid)) / count(distinct (verkoop.id)),
        1) as 'IPT'
from
    verkoop,
    verkoopregel,
    artikel,
    product,
    winkel
where
    verkoop.id = verkoopregel.verkoopid
        and verkoopregel.artikelid = artikel.id
        and artikel.productid = product.id
        and verkoop.winkelid = winkel.Id
        and verkoopregel.RubriekId = 1
        and jaar = year(curdate())
        and datum between '2014-05-23' and '2014-05-29'
        and exists( select 
            ver.id, count(verkoopregel.id) as aantal
        from
            verkoop ver,
            verkoopregel,
            artikel
        where
            ver.id = verkoopregel.verkoopid
                and verkoopregel.artikelid = artikel.id
                and verkoopregel.RubriekId = 1
                and verkoopregel.bedragtotaal > 0
                and artikelnummer in (158352 , 158361,
                158343,
                224266,
                160665,
                160674,
                150529,
                126946,
                126937,
                126928,
                109422,
                142137,
                142146,
                142313,
                147789,
                147798,
                154556,
                154565,
                111115,
                111124,
                159560)
                and ver.jaar = year(curdate())
                and ver.datum between '2014-05-23' and '2014-05-29'
                and ver.id = verkoop.id
        group by verkoop.id
        having aantal >= 4)
group by winkelid) wel4voor3
inner join
(select 
    winkel.nummer,
    verkoop.datum,
    count(distinct (verkoop.id)) as klanten,
    format(sum(if(product.GrondslagId = 2,
            1,
            verkoopregel.Hoeveelheid)) / count(distinct (verkoop.id)),
        1) as 'IPT'
from
    verkoop,
    verkoopregel,
    artikel,
    product,
    winkel
where
    verkoop.id = verkoopregel.verkoopid
        and verkoopregel.artikelid = artikel.id
        and artikel.productid = product.id
        and verkoop.winkelid = winkel.Id
        and verkoopregel.RubriekId = 1
        and jaar = year(curdate())
        and datum between '2014-05-23' and '2014-05-29'
group by winkelid) niet4voor3 on wel4voor3.nummer = niet4voor3.nummer