select winkel.nummer AS 'Winkelnummer', afdeling.omschrijving as 'Afdeling', artikel.omschrijving1, rubriek.omschrijving AS 'Rubriek', REPLACE(FORMAT(sum(totaalmarqtwaarde),2),'.',',') AS 'Marqtwaarde', sum(totaalmarqtvolume) AS 'Marqtvolume'
from resultaat, rubriek, winkel, artikel, afdeling 
where resultaat.rubriekid = rubriek.id
and resultaat.artikelid = artikel.id
and resultaat.winkelid = winkel.id
and resultaat.afdelingid = afdeling.id
and rubriek.nummer = 3600
and jaar = 2013
and periode = 9
#and week = 31
and label = 'BASE'
group by winkel.nummer, afdeling.omschrijving, artikel.omschrijving1