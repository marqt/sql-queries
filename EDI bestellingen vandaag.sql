SELECT 
    winkel.nummer AS 'Winkelnummer',
    partner.naam AS 'Partnernaam',
    inkooporder.orderkenmerk AS 'Ordernummer',
    inkooporder.levergroepid AS 'Levergroepid',
    inkooporder.statusdatumtijd AS 'Besteltijd'
FROM
    inkooporder,
    winkel,
    partner
WHERE
    inkooporder.statusdatumtijd > DATE_SUB(CURDATE(), INTERVAL 0 DAY)
        AND inkooporder.statusdatumtijd < DATE_ADD(CURDATE(), INTERVAL 1 DAY)
        AND inkooporder.partnerid = partner.id
        AND inkooporder.winkelid = winkel.id
        AND partner.OrderEDIFACT = 1
        AND partner.nummer != 66
ORDER BY inkooporder.statusdatumtijd ASC