select 
    winkel.nummer,
    format(sum(verkoopregel.BedragInkoop * verkoopregel.Hoeveelheid),
        2) as marqtwaarde,
    date(verkoopregel.LaatsteWijziging) as verwerkingsdatum_in_MC,
    count(distinct (verkoop.bonnummer)) as aantal_dervingsbonnen,
    group_concat(distinct (date(verkoopregel.datumtijd))) as inschietdatum_dervingsbonnen
from
    verkoopregel,
    verkoop,
    artikel,
    rubriek,
    winkel
where
    verkoopregel.verkoopid = verkoop.id
        and verkoopregel.artikelid = artikel.id
        and verkoopregel.rubriekid = rubriek.Id
        and verkoop.winkelid = winkel.id
        and rubriek.derving = 1
        and verkoop.jaar = year(curdate())
        and verkoop.week = week(date_sub(curdate(), interval (1) week),
        3)
        and verkoopregel.LaatsteWijziging > concat(date(date_add(curdate(),
                    interval (3 - dayofweek(curdate())) day)),
            ' 07:00:00')
group by winkel.id , date(verkoopregel.LaatsteWijziging)
order by verwerkingsdatum_in_MC , winkel.Id
