select 
    winkel.nummer as 'winkelnummer',
    verkoop.kassaid as 'kassaid',
    betaalmiddel.omschrijving as 'betaalnummer',
    format(sum(bedragtotaal), 2, 'NL_nl') as 'som bedragen transacties',
    count(verkoop.id) as 'aantal transacties'
from
    verkoop,
    winkel,
    verkoopbetaling,
    betaalmiddel
where
    verkoop.id = verkoopbetaling.verkoopid
        and verkoopbetaling.betaalmiddelid = betaalmiddel.id
        and verkoop.winkelid = winkel.id
        and verkoop.jaar = 2014
        and verkoop.week = 6
        and verkoopbetaling.betaalmiddelid != 1
group by winkel.id , verkoop.kassaid , betaalmiddel.id