	select 
    artikel.ArtikelNummer AS 'Artikelnummer',
    artikel.Omschrijving1 AS 'Artikelomschrijving',
    artikel.actief AS 'Actief',
    artikel.id AS 'Artikelid',
    artikel.Merk AS 'Artikelmerk',
    product.Houdbaarheid AS 'Houdbaarheid',
    opgr1.Nummer AS 'OG1 nummer',
    opgr2.Nummer AS 'OG2 nummer',
    opgr3.Nummer AS 'OG3 nummer',
    opgr1.Omschrijving AS 'OG1 Omschrijving',
    opgr2.Omschrijving AS 'OG2 Omschrijving',
    opgr3.Omschrijving AS 'OG3 Omschrijving',
    afdeling.Omschrijving AS 'Afdelingomschrijving',
    grondslag.omschrijving AS 'Grondslagomschrijving',
    partner.Naam AS 'Partnernaam',
    partner.Opmerking2 AS 'Besteldagen',
    levergroep.id AS 'Levergroepid',
    winkelartikel.BestelCodePartner AS 'Bestelcodepartner',
    winkelartikel.WinkelId AS 'Winkelid',
    (select 
            bestelHoeveelheid
        from
            besteleenheid
        where
            besteleenheid.artikelId = artikel.Id
                and datumvanaf <= now()
        order by datumvanaf desc , besteleenheid.id asc
        limit 1) as 'Besteleenheid',
    (select 
            barcode
        from
            barcode
        where
            barcode.artikelid = artikel.id
                AND besteleenheidid is null
                AND datumvanaf <= now()
        ORDER BY datumvanaf DESC
        limit 1) AS 'BEbarcode',
    (select 
            Opmerking
        from
            besteleenheid
        where
            besteleenheid.artikelId = artikel.Id
        order by id DESC
        limit 1) AS 'BEOpmerking',
    concat(product.Inhoud, ' ', meeteenheid.Code) AS 'Productinhoud',
    sum(case
        when (deel.weekdag = 1) then deel.marqtvolume
    end),
    sum(case
        when (deel.weekdag = 2) then deel.marqtvolume
    end),
    sum(case
        when (deel.weekdag = 3) then deel.marqtvolume
    end),
    sum(case
        when (deel.weekdag = 4) then deel.marqtvolume
    end),
    sum(case
        when (deel.weekdag = 5) then deel.marqtvolume
    end),
    sum(case
        when (deel.weekdag = 6) then deel.marqtvolume
    end),
    sum(case
        when (deel.weekdag = 7) then deel.marqtvolume
    end),
    sum(case
        when (half.weekdag = 1) then round(half.marqtvolume)
    end),
    sum(case
        when (half.weekdag = 2) then round(half.marqtvolume)
    end),
    sum(case
        when (half.weekdag = 3) then round(half.marqtvolume)
    end),
    sum(case
        when (half.weekdag = 4) then round(half.marqtvolume)
    end),
    sum(case
        when (half.weekdag = 5) then round(half.marqtvolume)
    end),
    sum(case
        when (half.weekdag = 6) then round(half.marqtvolume)
    end),
    sum(case
        when (half.weekdag = 7) then round(half.marqtvolume)
    end),
    (select 
            date_add(curdate(),
                    interval (4 - (weekday(curdate()) + 2) + (4 - 4) + orderschemaitem.leverdagwinkel) day)
        from
            orderschema,
            orderschemaitem
        where
            levergroep.id = orderschema.levergroepid
                and orderschema.id = orderschemaitem.orderschemaid
                and orderschema.actief = 1
                and orderschemaitem.besteldag = 4
                and orderschema.ingangsdatum < curdate()
                and (orderschema.einddatum > curdate()
                OR orderschema.einddatum IS NULL)) AS 'BesteldagWoensdag',
    (select 
            date_add(curdate(),
                    interval (4 - (weekday(curdate()) + 2) + (5 - 4) + orderschemaitem.leverdagwinkel) day)
        from
            orderschema,
            orderschemaitem
        where
            levergroep.id = orderschema.levergroepid
                and orderschema.id = orderschemaitem.orderschemaid
                and orderschema.actief = 1
                and orderschemaitem.besteldag = 5
                and orderschema.ingangsdatum < curdate()
                and (orderschema.einddatum > curdate()
                OR orderschema.einddatum IS NULL)) AS 'BesteldagDonderdag',
    (select 
            date_add(curdate(),
                    interval (4 - (weekday(curdate()) + 2) + (6 - 4) + orderschemaitem.leverdagwinkel) day)
        from
            orderschema,
            orderschemaitem
        where
            levergroep.id = orderschema.levergroepid
                and orderschema.id = orderschemaitem.orderschemaid
                and orderschema.actief = 1
                and orderschemaitem.besteldag = 6
                and orderschema.ingangsdatum < curdate()
                and (orderschema.einddatum > curdate()
                OR orderschema.einddatum IS NULL)) AS 'BesteldagVrijdag',
    (select 
            date_add(curdate(),
                    interval (4 - (weekday(curdate()) + 2) + (7 - 4) + orderschemaitem.leverdagwinkel) day)
        from
            orderschema,
            orderschemaitem
        where
            levergroep.id = orderschema.levergroepid
                and orderschema.id = orderschemaitem.orderschemaid
                and orderschema.actief = 1
                and orderschemaitem.besteldag = 7
                and orderschema.ingangsdatum < curdate()
                and (orderschema.einddatum > curdate()
                OR orderschema.einddatum IS NULL)) AS 'BesteldagZaterdag',
    (select 
            date_add(curdate(),
                    interval (4 - (weekday(curdate()) + 2) + (9 - 4) + orderschemaitem.leverdagwinkel) day)
        from
            orderschema,
            orderschemaitem
        where
            levergroep.id = orderschema.levergroepid
                and orderschema.id = orderschemaitem.orderschemaid
                and orderschema.actief = 1
                and orderschemaitem.besteldag = 2
                and orderschema.ingangsdatum < curdate()
                and (orderschema.einddatum > curdate()
                OR orderschema.einddatum IS NULL)) AS 'BesteldagMaandag',
    (select 
            date_add(curdate(),
                    interval (4 - (weekday(curdate()) + 2) + (10 - 4) + orderschemaitem.leverdagwinkel) day)
        from
            orderschema,
            orderschemaitem
        where
            levergroep.id = orderschema.levergroepid
                and orderschema.id = orderschemaitem.orderschemaid
                and orderschema.actief = 1
                and orderschemaitem.besteldag = 3
                and orderschema.ingangsdatum < curdate()
                and (orderschema.einddatum > curdate()
                OR orderschema.einddatum IS NULL)) AS 'BesteldagDinsdag'
from
    winkelartikel
        inner join
    artikel ON winkelartikel.artikelId = artikel.Id
        inner join
    product ON artikel.ProductId = product.Id
        inner join
    meeteenheid ON product.InhoudMeeteenheidId = meeteenheid.Id
        inner join
    grondslag ON grondslag.Id = product.GrondslagId
        inner join
    winkel ON winkelartikel.WinkelId = winkel.Id
        inner join
    winkelpartnerafdeling ON winkelpartnerafdeling.id = winkelartikel.WinkelpartnerafdelingId
        inner join
    afdeling ON afdeling.id = winkelpartnerafdeling.afdelingId
        inner join
    levergroep ON levergroep.id = ifnull(winkelartikel.levergroepId,
            winkelpartnerafdeling.DefaultLeverGroepId)
        inner join
    partner ON partner.Id = levergroep.PartnerId
        inner join
    operationelegroep as opgr3 ON opgr3.id = product.OperationeleGroepId
        inner join
    operationelegroep as opgr2 ON opgr2.id = opgr3.OperationeleGroepParentId
        inner join
    operationelegroep as opgr1 ON opgr1.id = opgr2.OperationeleGroepParentId
        inner join
    status as statusProduct ON product.StatusId = statusProduct.Id
        inner join
    status as statusArtikel ON artikel.StatusId = statusArtikel.Id
        left outer join
    (select 
        resultaat.artikelid,
            resultaat.weekdag,
            resultaat.afdelingid,
            sum(resultaat.totaalmarqtvolume) AS 'marqtvolume'
    from
        resultaat, artikel
    where
        resultaat.artikelid = artikel.id
            and artikel.actief = 1
            and resultaat.jaar = YEAR(CURDATE())
            and resultaat.week = 1
            and resultaat.afdelingid = 6
            and resultaat.label = 'OPSD'
    group by resultaat.artikelid , resultaat.weekdag) deel ON deel.artikelid = artikel.id
        left join
    (select 
        verhoudingartikelnaarartikel.artikelNaarId,
            resultaat.weekdag,
            sum(verhoudingartikelnaarartikel.AantalNaar * resultaat.totaalmarqtvolume) as 'marqtvolume'
    from
        resultaat, verhoudingartikelnaarartikel
    where
        resultaat.artikelid = verhoudingartikelnaarartikel.VerhoudingArtikelId
            and resultaat.jaar = YEAR(CURDATE())
            and resultaat.week = 1
            and resultaat.label = 'OPSD'
    group by resultaat.artikelid , resultaat.weekdag) half ON (half.artikelNaarId = artikel.id
        and half.weekdag = deel.weekdag)
where
    winkelartikel.winkelid = 1
        and artikel.actief = 1
        and afdeling.id = 15
        and (artikel.inkoopvariant = 1
        or (artikel.inkoopvariant = 0
        AND artikel.verkoopvariant = 0))
group by winkelartikel.artikelid
order by afdeling.Omschrijving , levergroep.id , opgr1.Nummer , opgr2.Nummer , opgr3.Nummer , artikel.Omschrijving1