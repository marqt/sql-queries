select 
    (select 
            barcode
        from
            barcode
        where
            barcode.artikelid = artikel.id
                and datumvanaf <= curdate()
                and besteleenheidid is not null
        order by datumvanaf desc
        limit 1) as bebarcode,
    partner.gtin,
    artikel.omschrijving1,
    artikel.ArtikelNummer,
    winkelartikel.BestelCodePartner,
    land.Code,
    product.Houdbaarheid,
	winkelartikel.PrijsInkoop,
	btw.Percentage
from
    partner,
    winkelpartnerafdeling,
    winkelartikel,
    artikel,
    product,
btw,
land
where
    partner.id = winkelpartnerafdeling.PartnerId
        and winkelpartnerafdeling.id = winkelartikel.WinkelPartnerAfdelingId
        and winkelartikel.ArtikelId = artikel.Id
        and artikel.ProductId = product.Id
and product.BtwId = btw.id
and product.LandId = land.id
        and artikel.bestelbaar = 1
        and partner.Nummer = 272
group by artikel.id
order by BestelCodePartner
