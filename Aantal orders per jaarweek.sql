select 
    concat(year(inkooporder.statusdatumtijd),'-',lpad(month(inkooporder.statusdatumtijd),2,0)),
    count(distinct(inkooporder.id)),
	count(inkooporderregel.id),
count(if(partner.OrderEDIFACT = 1,inkooporderregel.id,NULL))
from
    inkooporder,
inkooporderregel,
levergroep,
partner
where inkooporder.id = inkooporderregel.orderId
and inkooporder.LeverGroepId = levergroep.Id
and levergroep.PartnerId = partner.Id
group by year(inkooporder.statusdatumtijd) , month(inkooporder.statusdatumtijd)