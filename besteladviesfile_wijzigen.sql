# Query om een besteladviesfile te veranderen bijv. tbv testomgeving
# child tabels hoeven niet aangepast te worden.

update central.besteladviesfile,
    central.besteladvies 
set 
    datum = '2017-02-27	 06:00:00',
    inkooporderid = null,
    statusid = 1
where
    besteladviesfile.id = 7712
        and besteladviesfileid = besteladviesfile.id
        
        