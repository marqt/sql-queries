select 
    (select 
            barcode
        from
            barcode
        where
            barcode.artikelid = artikel.id
                and besteleenheidid is null
                and datumvanaf <= curdate()
        order by datumvanaf desc , barcode.id
        limit 1) as 'gtin',
    artikel.artikelnummer as 'buyer_item_code',
    artikel.omschrijving1 as 'description',
    1 as 'returnable_packaging_code_quantity',
    land.code as 'country_of_origin',
    40 as 'container_maximum_quantity',
    't' as 'expiry_date_mandatory',
    'f' as 'batch_number_mandatory',
    'f' as 'country_code_mandatory',
    1 as 'returnable_packaging_code_id',
    partner.gtin as 'gln',
    't' as 'allow_expiry_date_calculation',
    product.Houdbaarheid as 'expiry_date_days',
    '' as 'specific_logistic_unit_code_id',
    winkelartikel.BestelCodePartner as 'supplier_item_code',
    '' as 'volume',
    (select 
            BestelHoeveelheid
        from
            besteleenheid
        where
            besteleenheid.artikelid = artikel.id
        order by datumvanaf <= curdate() order by datumvanaf desc , id desc limit 1) * winkelartikel.prijsinkoop as 'unit_price',
    btw.percentage as 'vat',
    'S' as 'vat_code',
    't' as 'homogeneous_pallet'
from
    artikel,
    winkelartikel,
    winkelpartnerafdeling,
    partner,
    product,
    land,
    btw
where
    artikel.id = winkelartikel.artikelid
        and winkelartikel.WinkelPartnerAfdelingId = winkelpartnerafdeling.Id
        and winkelpartnerafdeling.PartnerId = partner.id
        and artikel.productid = product.id
        and product.landid = land.id
        and product.btwid = btw.id
        and partner.nummer = 261
group by artikel.id