select 
    cg1.nummer AS 'cg nivo 0 nummer',
    cg1.omschrijving AS 'cg nivo 0 omschrijving',
    cg2.nummer AS 'cg nivo 1 nummer',
    cg2.omschrijving AS 'cg nivo 1 omschrijving',
    cg3.id,
    cg3.nummer AS 'cg nivo 2 nummer',
    cg3.omschrijving AS 'cg nivo 2 omschrijving',
    concat(cg1.omschrijving,
            ' - ',
            cg2.omschrijving,
            ' - ',
            cg3.omschrijving),
    cg3.marketinginformatie1,
    cg3.marketinginformatie2,
    cg3.marketinginformatie3,
    cg3.marketinginformatie4,
    cg3.marketinginformatie5,
    cg3.marketinginformatie6,
    count(product.id) AS 'Aantal producten'
from
    commercielegroep cg1,
    commercielegroep cg2,
    commercielegroep cg3,
    product,
    artikel
where
    cg2.id = cg3.commercielegroepparentid
        and cg1.id = cg2.commercielegroepparentid
        and cg3.id = product.commercielegroepid
        and product.id = artikel.productid
        and artikel.statusid in (3 , 4)
group by product.commercielegroepid
order by cg1.omschrijving , cg2.omschrijving , cg3.omschrijving