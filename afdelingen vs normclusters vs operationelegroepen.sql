select 
    afdeling.omschrijving as 'afdeling',
    normcluster.omschrijving as 'normcluster',
    operationelegroep.omschrijving as 'operationele groep',
    count(artikel.id) as '#artikelen'
from
    artikel,
    winkelartikel,
    winkelpartnerafdeling,
    afdeling,
    product,
    operationelegroep,
    normcluster
where
    artikel.id = winkelartikel.artikelid
        and winkelartikel.winkelpartnerafdelingid = winkelpartnerafdeling.id
        and winkelpartnerafdeling.afdelingid = afdeling.id
        and artikel.productid = product.id
        and product.operationelegroepid = operationelegroep.id
        and operationelegroep.normclusterid = normcluster.id
        and winkelartikel.winkelid = 3
		and artikel.statusid = 4
group by afdeling.id , operationelegroep.id , normcluster.id