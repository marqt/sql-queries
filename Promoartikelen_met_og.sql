select 
    artikel.artikelnummer as 'Article Code',
    artikel.Omschrijving1,
    og2.id,
    date_format(promotieperiode.startdatum, '%Y%m%d') as 'Start date',
    date_format(promotieperiode.einddatum, '%Y%m%d') as 'End date'
from
    promotie,
    promotieperiode,
    promotiestatus,
    promotieartikel,
    artikel,
    product,
    operationelegroep og1,
    operationelegroep og2,
    operationelegroep og3
where
			promotie.promotieperiodeid = promotieperiode.Id
        and promotie.promotieStatusId = promotiestatus.Id
        and promotieartikel.promotieId = promotie.Id
        and promotieartikel.artikelId = artikel.Id
        and artikel.ProductId = product.id
        and product.OperationeleGroepId = og3.id
        and og3.OperationeleGroepParentId = og2.id
        and og2.OperationeleGroepParentId = og1.id
        and promotieperiode.startdatum >= date_sub(NOW(), interval 14 day)
        and promotieperiode.einddatum <= date_sub(NOW(), interval -7 day)
        and og2.id in (417 , 418, 419)
        
        order by promotieperiode.startdatum