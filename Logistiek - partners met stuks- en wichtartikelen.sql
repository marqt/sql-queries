select 
    *
from
    partner
where
    exists( select 
            *
        from
            winkelpartnerafdeling,
            winkelartikel,
            artikel,
            product
        where
            PartnerId
                and winkelpartnerafdeling.id = winkelartikel.WinkelPartnerAfdelingId
                and winkelartikel.ArtikelId = artikel.id
                and artikel.productId = product.Id
                and product.GrondslagId = 1
                and winkelpartnerafdeling.partnerid = partner.id)
        and exists( select 
            *
        from
            winkelpartnerafdeling,
            winkelartikel,
            artikel,
            product
        where
            PartnerId
                and winkelpartnerafdeling.id = winkelartikel.WinkelPartnerAfdelingId
                and winkelartikel.ArtikelId = artikel.id
                and artikel.productId = product.Id
                and product.GrondslagId = 2
                and winkelpartnerafdeling.partnerid = partner.id)