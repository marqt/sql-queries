select 
    artikelvan.artikelnummer,
    artikelvan.omschrijving1,
    wamk.prijsadviesincbtw,
    wamk.prijsinkoop,
    verhoudingartikelnaarartikel.AantalNaar,
    artikelnaar.artikelnummer,
    artikelnaar.omschrijving1,
    wapartner.prijsinkoop,
    verhoudingartikelnaarartikel.AantalNaar * wapartner.prijsinkoop as 'prijs per maaltijd'
from
    verhoudingartikelnaarartikel,
    artikel artikelvan,
    artikel artikelnaar,
    winkelartikel wamk,
    winkelartikel wapartner
where
    verhoudingartikelnaarartikel.ArtikelNaarId = artikelnaar.Id
        and verhoudingartikelnaarartikel.VerhoudingArtikelId = artikelvan.Id
        and artikelvan.Id = wamk.ArtikelId
        and artikelnaar.Id = wapartner.ArtikelId
        and wamk.winkelid = 1
        and wapartner.winkelid = 13
        and artikelnaar.ArtikelNummer = 166276
        and artikelvan.ArtikelNummer = 166160