select 
    artikel.omschrijving1
from
    historie, artikel
where
   historie.tabel = 'Artikel' and historie.type = 'I'
       and artikel.artikelnummer = SUBSTRING(historie.omschrijving, 1, 6)
        and historie.laatstewijziging > date_sub(curdate(), interval 7 day)

