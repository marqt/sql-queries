SELECT 
    bestelDatumTijd as 'Besteldeadline',
    levergroepid as 'Levergroep nummer',
    partner.naam as 'Partner',
    levergroep.orderForecast as 'Order Forecast?',
    levergroep.orderForecastAuto as 'Auto order?',
    levergroep.consolideren 'Consolideren?'
    
FROM
    central.v_bestellevermomenttim,
    levergroep,
    partner
WHERE
    v_bestellevermomenttim.levergroepid = levergroep.id
    and levergroep.PartnerId=partner.id
		#Vanaf NU
        #AND v_bestellevermomenttim.bestelDatumTijd >= NOW()
        #Vandaag 
        AND date(v_bestellevermomenttim.bestelDatumTijd) = CURDATE()
        and orderForecast = 1
ORDER BY bestelDatumTijd ASC