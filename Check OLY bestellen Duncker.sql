select 
    'Bestellingen',
    partner.naam,
    inkooporder.orderkenmerk,
    artikel.omschrijving1,
    round(inkooporderregel.CEbesteld),
    date_format(inkooporder.statusdatumtijd,
            '%Y-%m-%d')
from
    inkooporder,
    inkooporderregel,
    winkelartikel,
    winkel,
    artikel,
    partner
where
    artikel.id = winkelartikel.artikelid
        and winkel.id = winkelartikel.winkelid
        and inkooporder.winkelid = winkel.id
        and inkooporder.id = inkooporderregel.orderid
        and inkooporderregel.artikelid = artikel.id
        and inkooporder.partnerid = partner.id
        and inkooporder.levergroepid in (96 , 166, 190, 191)
        and winkel.nummer = 160
        and inkooporder.verwachteLeverDatumWinkelTijd > curdate() 
union select 
    'Omzetten',
    partner.naam,
    'Order',
    artikel.omschrijving1,
    - 1 * round(resultaat.totaalmarqtvolume),
    resultaat.datum
from
    resultaat,
    winkel,
    artikel,
    winkelartikel,
    partner
where
    winkel.id = resultaat.winkelid
        and artikel.id = resultaat.artikelid
        and winkel.id = winkelartikel.winkelid
        and artikel.id = winkelartikel.artikelid
        and resultaat.partnerid = partner.id
        and resultaat.label = 'OPSD'
        and resultaat.jaar = year(curdate())
        and resultaat.week = week(curdate(), 3) - 1
        and winkel.nummer = 160
        and winkelartikel.levergroepid in (96 , 166, 190, 191)