select 
    mei2014.omschrijving1,
    mei2014.IKP2014,
    mei2013.IKP2013,
    format((mei2014.IKP2014 / mei2013.IKP2013 - 1)*100,
        1) as percentage
from
    (SELECT 
        artikel.id as artikelid,
            artikel.omschrijving1,
            winkelartikelexporthistorie.prijsinkoop as IKP2014
    FROM
        partner, winkelpartnerafdeling, winkelartikel, artikel, winkelartikelexporthistorie
    where
        partner.id = winkelpartnerafdeling.partnerid
            and winkelpartnerafdeling.id = winkelartikel.winkelpartnerafdelingid
            and winkelartikel.artikelid = artikel.id
            and winkelartikelexporthistorie.winkelartikelid = winkelartikel.id
            and winkelartikel.WinkelId = 3
            and partner.nummer = 3
            and artikel.statusid in (3 , 4)
            and winkelartikelexporthistorie.datumGeldigVanaf < '2014-15-05'
            and winkelartikelexporthistorie.datumGeldigTot > '2014-15-05') mei2014
        left join
    (SELECT 
        artikel.id as artikelid,
            artikel.omschrijving1,
            winkelartikelexporthistorie.prijsinkoop as IKP2013
    FROM
        partner, winkelpartnerafdeling, winkelartikel, artikel, winkelartikelexporthistorie
    where
        partner.id = winkelpartnerafdeling.partnerid
            and winkelpartnerafdeling.id = winkelartikel.winkelpartnerafdelingid
            and winkelartikel.artikelid = artikel.id
            and winkelartikelexporthistorie.winkelartikelid = winkelartikel.id
            and winkelartikel.WinkelId = 3
            and partner.nummer = 3
            and artikel.statusid in (3 , 4)
            and winkelartikelexporthistorie.datumGeldigVanaf < '2013-15-05'
            and winkelartikelexporthistorie.datumGeldigTot > '2013-15-05') mei2013 ON mei2013.artikelid = mei2014.artikelid

