SELECT 

og1.omschrijving,
date(besteladviesfile.datum),
sum(besteladviesregel.adviesCE)

FROM central.besteladviesregel

left join besteladviesfile on besteladviesfile.id=besteladviesregel.besteladviesId
left join winkel on winkel.id=besteladviesfile.winkelid
left join winkelartikel on winkelartikel.id=besteladviesregel.winkelartikelid
left join artikel on artikel.id=winkelartikel.artikelid
left join product ON product.id = artikel.ProductId
left join operationelegroep og3 ON og3.id = product.OperationeleGroepId
left join operationelegroep og2 ON og2.id = og3.OperationeleGroepParentId
left join operationelegroep og1 ON og1.id = og2.OperationeleGroepParentId

where date(besteladviesfile.datum)>='2017-01-01' and og1.id=405

group by date(besteladviesfile.datum), og1.omschrijving

order by date(besteladviesfile.datum) desc