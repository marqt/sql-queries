select 
    concat(cast(winkel.nummer as char), '_') as 'Warehouse code',
    artikel.artikelnummer as 'Artikel Code',
omschrijving1,
    date_format(verkoop.datum, '%Y%m%d') as 'AfzetDatum',
    verkoopregel.hoeveelheid as 'DagAfzet',
    verkoopregel.hoeveelheid as 'DagRegels',
    0 as 'Derving',
    0 as 'Neeverkopen',
    0 as 'Promotionele verkopen',
    '' as 'Open',
    '' as 'Afzetkanaal 1',
    '' as 'Afzetkanaal 2',
    '' as 'Afzetkanaal 3',
    '' as 'Afzetkanaal 4',
    '' as 'Afzetkanaal 5'
from
    verkoop,
    verkoopregel,
    artikel,
    winkel
where
    verkoop.id = verkoopid
        and winkelid = winkel.id
        and rubriekid = 27
        and artikel.id = artikelid
        and verkoop.jaar = 2015
