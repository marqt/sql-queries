select 
    artikel.artikelnummer,
    artikel.omschrijving1,
    artikel.inkoopvariant,
    artikel.verkoopvariant,
    winkelartikel.bestelcodepartner,
    if(artikel.actief,'actief','inactief') AS 'Artikel bestelbaar',
    status.omschrijving AS 'Artikelstatus'
from
    winkelpartnerafdeling,
    winkelartikel,
    artikel,
    status
where
    winkelpartnerafdeling.id = winkelartikel.winkelpartnerafdelingid
        and winkelartikel.artikelid = artikel.id
        and artikel.statusid = status.id
        and winkelpartnerafdeling.afdelingid = 6
group by artikel.id