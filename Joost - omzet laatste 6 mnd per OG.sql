select 
    concat(og1.nummer,
            '.',
            og2.nummer,
            '.',
            og3.nummer,
            '_',
            og1.omschrijving,
            '-',
            og2.omschrijving,
            '-',
            og3.omschrijving) AS opertionelegroep,
    count(distinct (artikel.id)) as aantalSKUs,
    count(distinct (artikel.merk)) as aantalmerken,
    format(sum(resultaat.TotaalBedrag),
        2,
        'nl_NL') as omzet,
    format(sum(resultaat.TotaalBtw),
        2,
        'nl_NL') as btw,
    format(sum(resultaat.TotaalMarqtWaarde),
        2,
        'nl_NL') as mw,
    format(sum(resultaat.TotaalMarqtVolume),
        2,
        'nl_NL') as scans
from
    resultaat,
    artikel,
    product,
    operationelegroep og1,
    operationelegroep og2,
    operationelegroep og3
where
    resultaat.artikelid = artikel.id
        and artikel.ProductId = product.id
        and product.operationelegroepid = og3.id
        and og2.id = og3.OperationeleGroepParentId
        and og1.id = og2.OperationeleGroepParentId
        and label = 'OPSW'
        and jaar = 2014
        and week >= 19
group by og3.id