select 
    winkel.nummer,
    verkoop.bedragtotaal,
	verkoop.datum,
	postcode
from
    verkoop,
    winkel
where
    verkoop.winkelid = winkel.id
        and datum >= '2014-02-24'
        and postcode is not null
