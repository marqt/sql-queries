select 
    winkel.nummer,
    artikel.artikelnummer,
    date_format(afzet.datum,'%Y%m%d'),
    afzet.totaalvolume as afzet,
    verkoopbonnen.aantallen as aantalbonnen,
    derving.totaalvolume as derving,
    '',
    0,
    '',
    '',
    '',
    '',
    '',
    ''
from
    (select 
        resultaat.artikelid,
            resultaat.winkelid,
            resultaat.datum,
            resultaat.totaalvolume
    from
        resultaat
    where
        resultaat.jaar = 2014
            and resultaat.week = 30
            and resultaat.label = 'OPSD') afzet
        left join
    (select 
        resultaat.artikelid,
            resultaat.winkelid,
            resultaat.datum,
            resultaat.totaalvolume
    from
        resultaat
    where
        resultaat.jaar = 2014
            and resultaat.week = 30
            and resultaat.label = 'DVSD') derving ON (afzet.winkelid = derving.winkelid
        and afzet.artikelid = derving.artikelid
        and afzet.datum = derving.datum)
        inner join
    (select 
        verkoop.winkelid,
            verkoopregel.artikelid,
            verkoop.datum,
            count(verkoopregel.id) as aantallen
    from
        verkoop, verkoopregel
    where
        verkoopregel.verkoopid = verkoop.id
            and verkoop.jaar = 2014
            and verkoop.week = 30
            and verkoopregel.rubriekid = 1
    group by verkoop.winkelid , verkoopregel.artikelid , verkoop.datum) verkoopbonnen ON afzet.winkelid = verkoopbonnen.winkelid
        and afzet.artikelid = verkoopbonnen.artikelid
        and afzet.datum = verkoopbonnen.datum
        inner join
    winkel ON winkel.id = afzet.winkelid
        inner join
    artikel ON artikel.id = afzet.artikelid
limit 1000