SELECT


artikel.id,
artikelnummer,
Omschrijving1,
artikel.verkoopinformatie9,
artikel.verkoopinformatie10,
artikel.verkoopinformatie11,
artikel.verkoopinformatie12,
status.omschrijving as status,
og1.omschrijving as pilaar

 FROM central.artikel
 
 join status on artikel.statusid=status.id
 
         inner join
    product ON product.id = artikel.ProductId
        inner join
    operationelegroep og3 ON og3.id = product.OperationeleGroepId
        inner join
    operationelegroep og2 ON og2.id = og3.OperationeleGroepParentId
        inner join
    operationelegroep og1 ON og1.id = og2.OperationeleGroepParentId
 
 
 where artikel.StatusId in (1,2,3,4)