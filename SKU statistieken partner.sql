select 
    winkel.naam,
    artikel.id,
    artikel.omschrijving1,
    resultaat.jaar,
    resultaat.week,
    resultaat.totaalmarqtvolume as 'Volume'
from
    resultaat,
    winkel,
    artikel,
    winkelartikel,
    winkelpartnerafdeling,
    partner,
	partner huidig
where
    partner.id = winkelpartnerafdeling.partnerid
        and winkelpartnerafdeling.id = winkelartikel.winkelpartnerafdelingid
        and winkelartikel.winkelid = winkel.id
        and winkelartikel.artikelid = artikel.id
        and winkelartikel.winkelid = resultaat.winkelid
        and winkelartikel.artikelid = resultaat.artikelid
        and resultaat.jaar = YEAR(CURDATE())
        #and resultaat.week = 8
        and resultaat.label = 'OPSW'
        and partner.id = 223
order by winkel.nummer ASC , artikel.id ASC , resultaat.week ASC
