select 
    nultelling.winkelnummer,
    nultelling.week,
    nultelling.artikelomschrijving,
    ifnull(manco.manco, 'geen manco')
from
    (select 
        winkel.nummer as 'winkelnummer',
            week(statusdatumtijd) as 'week',
            artikel.id as 'artikelid',
            artikel.omschrijving1 as 'artikelomschrijving'
    from
        inventaris, inventarisregel, artikel, winkel
    where
        inventarisregel.inventarisid = inventaris.id
            and inventarisregel.artikelid = artikel.Id
            and inventaris.winkelid = winkel.id
            and inventaris.statusdatumtijd between date_sub(curdate(), interval (3) day) and date_sub(curdate(), interval (1) day)
            and inventarisregel.totaalgeteld = 0) nultelling
        left join
    (select 
        inkooporderregel.artikelid, 'manco'
    from
        inkooporder, inkooporderregel
    where
        inkooporderregel.orderid = inkooporder.id
            and inkooporder.goederenontvangstdatumtijd between date_sub(curdate(), interval (10) day) and date_sub(curdate(), interval (3) day)
            and inkooporder.orderstatus = 'GELEVERD'
            and inkooporderregel.servicepercentage != 100.00) manco ON nultelling.artikelid = manco.artikelid