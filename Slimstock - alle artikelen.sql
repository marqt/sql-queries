select distinct
    (artikel.id)
from
    resultaat,
    artikel,
    afdeling
where
    resultaat.artikelid = artikel.id
        and resultaat.AfdelingId = afdeling.Id
        and resultaat.label = 'OPSW'
        and resultaat.jaar = 2014
        and artikel.statusid in (5 , 6)
        and artikel.inkoopvariant != 1
        and afdeling.nummer not in (7 , 12, 13) 
union select 
    artikel.id
from
    artikel,
    winkelartikel,
    winkelpartnerafdeling,
    afdeling
where
    artikel.id = winkelartikel.artikelid
        and winkelartikel.WinkelPartnerAfdelingId = winkelpartnerafdeling.Id
        and winkelpartnerafdeling.AfdelingId = afdeling.Id
        and artikel.statusid in (3 , 4)
        and artikel.inkoopvariant != 1
        and afdeling.nummer not in (7 , 12, 13)
group by artikel.id
union
alle nonscans weg