select 
    artikel.ArtikelNummer as 'artikelnummer',
    artikel.omschrijving1 as 'artikelomschrijving',
    partner.naam as 'partner',
    afdeling.omschrijving as 'afdeling',
    concat(cg1.nummer,
            '.',
            cg2.nummer,
            '.',
            cg3.nummer,
            ' ',
            cg1.omschrijving,
            '-',
            cg2.omschrijving,
            '-',
            cg3.omschrijving) as 'commerciele groep',
    concat(og1.nummer,
            '.',
            og2.nummer,
            '.',
            og3.nummer,
            ' ',
            og1.omschrijving,
            '-',
            og2.omschrijving,
            '-',
            og3.omschrijving) as 'operationele groep',
    artikel.marketinginformatie1,
    artikel.marketinginformatie2,
    artikel.marketinginformatie3,
    artikel.marketinginformatie4,
    artikel.marketinginformatie5,
    artikel.marketinginformatie6
from
    partner,
    winkelpartnerafdeling,
    winkelartikel,
    afdeling,
    artikel,
    product,
    commercielegroep cg1,
    commercielegroep cg2,
    commercielegroep cg3,
    operationelegroep og1,
    operationelegroep og2,
    operationelegroep og3
where
    partner.id = winkelpartnerafdeling.partnerid
        and winkelpartnerafdeling.id = winkelartikel.winkelpartnerafdelingid
        and winkelpartnerafdeling.AfdelingId = afdeling.Id
        and winkelartikel.artikelid = artikel.id
        and artikel.ProductId = product.id
        and cg3.id = product.CommercieleGroepId
        and cg2.id = cg3.CommercieleGroepParentId
        and cg1.id = cg2.CommercieleGroepParentId
        and og3.id = product.OperationeleGroepId
        and og2.id = og3.OperationeleGroepParentId
        and og1.id = og2.OperationeleGroepParentId
        and artikel.statusid in (3 , 4)
        and afdeling.id = 2
group by artikel.id