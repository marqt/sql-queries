select 
    ediorders,
    orders,
    format(ediorders / orders, 3) * 100 as '% orders',
    ediorderregels,
    orderregels,
    format(ediorderregels / orderregels, 3) * 100 as '% orderregels',
    ediMW,
    MW,
    format(ediMW / MW, 3) * 100 as '% MW'
from
    (select 
        count(distinct (inkooporder.id)) as 'ediorders',
            count(inkooporderregel.id) as 'ediorderregels',
            sum(inkooporderregel.MWbesteld) as 'ediMW'
    from
        inkooporder, inkooporderregel, partner
    where
        yearweek(inkooporder.statusdatumtijd,3) = 201608
            AND inkooporder.partnerid = partner.id
            AND inkooporder.id = inkooporderregel.orderid
            AND (partner.OrderEDIFACT = 1 or partner.nummer in (403,96))) edi,
    (select 
        count(distinct (inkooporder.id)) as 'orders',
            count(inkooporderregel.id) as 'orderregels',
            sum(inkooporderregel.MWbesteld) as 'MW'
    from
        inkooporder, inkooporderregel, partner
    where
        yearweek(inkooporder.statusdatumtijd) = 201608
            AND inkooporder.partnerid = partner.id
            AND inkooporder.id = inkooporderregel.orderid) totaal