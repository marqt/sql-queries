select 
			winkel.nummer as winkelnummer,
			artikel.PluCode,
			afdeling.nummer as afdelingsnummer,
			artikel.Omschrijving1,
			winkelartikelexporthistorie.prijsadviesIncBtw as verkoopprijs,
			grondslag.code as grondslagecode,
			artikel.KassaOmschrijving,
			product.Temperatuur,
			product.houdbaarheid,
			barcode.barcode,
			btw.Percentage,
			product.ingredienten,
			if(winkelartikel.StatusId in (3 , 4),
				'update',
				'delete') as actie,
			artikel.verkoopinformatie9,
			artikel.verkoopinformatie10
		from
			artikel,
			product,
			btw,
			meeteenheid,
			grondslag,
			barcode,
			winkelartikel,
			winkel,
			winkelpartnerafdeling,
			winkelartikelexporthistorie,
			afdeling
		where
			artikel.id = barcode.artikelid
				and artikel.productid = product.id
				and product.grondslagid = grondslag.id
				and product.inhoudmeeteenheidid = meeteenheid.id
				and product.btwid = btw.id
				and winkelartikel.artikelid = artikel.id
				and winkelartikel.winkelid = winkel.id
				and winkelartikelexporthistorie.datumGeldigTot >= date_sub(now(),interval 0 hour)
				and winkelartikel.id = winkelartikelexporthistorie.winkelartikelid	
				and winkelartikel.WinkelPartnerAfdelingId = winkelpartnerafdeling.Id
				and winkelpartnerafdeling.AfdelingId = afdeling.id
				and barcode.BestelEenheidId is null
				and afdeling.nummer in (1,2,3,4,5,6,15)
				and winkel.type = 'winkel'
				and artikel.inkoopvariant != 1
				and artikel.PluCode > 0
				and barcode.barcode like '22%'
				and winkelartikel.id in (select 
							winkelartikelid
						from
							winkelartikelexporthistorie
						where
							winkelartikelexporthistorie.winkelArtikelId
								and winkelartikelexporthistorie.datumGeldigVanaf >= date_sub(now(),interval 1 day) union select 
							winkelartikel.id
						from
							historie,
							product,
							artikel,
							winkelartikel
						where
							winkelartikel.artikelid = artikel.id
								and artikel.productid = product.id 
								and product.id = historie.recordid 
								and historie.tabel = 'product'
								and historie.veld in ('houdbaarheid' , 'temperatuur', 'ingredienten', 'operationelegroep')
								and historie.laatstewijziging >= date_sub(now(), interval 1 day) union select 
							winkelartikel.id
						from
							historie,
							artikel,
							winkelartikel
						where
							winkelartikel.artikelid = artikel.id
								and artikel.id = historie.recordid
								and historie.tabel = 'artikel'
								and historie.veld in ('PluCode' , 'Omschrijving1')
								and historie.laatstewijziging >= date_sub(now(), interval 1 day))
				order by winkel.id, artikel.id