select 
    verkoop.week, verkoop.datum, winkel.nummer, verkoop.starttijd
from
    verkoop,
    winkel
where
    verkoop.winkelid = winkel.id
        and verkoop.jaar = 2013
        and verkoop.week > week(curdate(), 3) - 2
        and winkel.nummer in (151 , 153, 158)
        and exists( select distinct
            (verkoop.id)
        from
            verkoop ver,
            verkoopregel verreg
        where
            ver.id = verreg.verkoopid
                and verreg.commercielegroepid = 12658
                and ver.jaar = verkoop.jaar
                and ver.week = verkoop.week
                and ver.winkelid = verkoop.winkelid
                and ver.id = verkoop.id)
        and exists( select distinct
            (verkoop.id)
        from
            verkoop ver,
            verkoopregel verreg
        where
            ver.id = verreg.verkoopid
                and verreg.commercielegroepid = 12731
                and ver.jaar = verkoop.jaar
                and ver.week = verkoop.week
                and ver.winkelid = verkoop.winkelid
                and ver.id = verkoop.id)
