SELECT distinct(ArtikelNummer), Omschrijving1, winkelartikel.ForceerUpdate FROM central.winkelartikelexporthistorie 

join winkelartikel on winkelartikel.id=winkelArtikelId
join artikel on artikel.id=winkelartikel.ArtikelId

where date(winkelartikel.ForceerUpdate) = curdate()

order by ForceerUpdate desc