explain select jaar, periode, week,winkel.nummer as winkelNr, winkel.naam as Winkel, 
afdeling.omschrijving as afdeling, 
concat(og1.nummer,'.',og2.nummer,'.',og3.nummer,' ',og1.omschrijving,'-',og2.omschrijving,'-',og3.omschrijving) as operationelegroep,
concat(com1.Nummer , '.',com2.nummer, '.', com3.nummer, ' ',  com1.Omschrijving , '-' , com2.Omschrijving, '-' ,com3.Omschrijving) as commercielegroep, 
com2.Omschrijving, 
rubriek.nummer as rubriekNr, 
rubriek.omschrijving as rubriek,
sum(Totaalbedrag) as Omzet,
sum(Totaalbtw) as BTW, 
sum(totaalmarqtwaarde) as Marqtwaarde,
sum(Totaalbedrag) - sum(Totaalbtw) - sum(totaalmarqtwaarde) as BrutoMarge, 
sum(Totaalbedrag) - sum(Totaalbtw) as NettoOmz, 
sum(TotaalMARQTvolume) as Volume

from resultaat

inner join artikel on resultaat.artikelid=artikel.id
inner join product on product.id=artikel.productid
inner join grondslag on grondslag.id=product.grondslagid
inner join winkel on winkel.id=resultaat.winkelid
inner join afdeling on afdeling.id=resultaat.afdelingid
inner join rubriek on rubriek.id=resultaat.rubriekid
inner join partner on resultaat.partnerid=partner.id
inner join operationelegroep as og3 on og3.id = product.OperationeleGroepId
inner join operationelegroep as og2 on og2.id = og3.OperationeleGroepParentId
inner join operationelegroep as og1 on og1.id = og2.OperationeleGroepParentId 
inner join commercielegroep as com3 on com3.id = product.commercieleGroepId
inner join commercielegroep as com2 on com2.id = com3.commercieleGroepParentId
inner join commercielegroep as com1 on com1.id = com2.commercieleGroepParentId 

where resultaat.label ='BASE'
and jaar in (2012, 2013)
and rubriek.nummer in (100, 3200, 3300, 3301, 3700, 3800, 3900, 3302, 3601, 3903)
and week < WEEKOFYEAR(CURDATE()) +1

group by jaar, week, winkel.nummer, rubriek.nummer, og3.id, com3.id
