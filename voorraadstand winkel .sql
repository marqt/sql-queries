SELECT 
    artikel.omschrijving1,
    voorraadstand.voorraadstanddatum,
    voorraadstand.voorraadstand,
    voorraadstand.winkelartikelid,
    winkel.nummer
FROM
    voorraadstand,
    (SELECT 
        MAX(voorraadstanddatum) AS datum, winkelartikelid
    FROM
        voorraadstand
    GROUP BY winkelartikelid) maxdatum,
    winkelartikel,
    artikel,
    winkel,
    product,
    operationelegroep og1,
    operationelegroep og2,
    operationelegroep og3
WHERE
    voorraadstand.winkelartikelId = winkelartikel.Id
        AND voorraadstand.voorraadstanddatum = maxdatum.datum
        AND voorraadstand.winkelartikelId = maxdatum.winkelartikelId
        AND winkelartikel.artikelid = artikel.id
        AND winkelartikel.WinkelId = winkel.id
        AND artikel.ProductId = product.id
        AND og2.id = og3.operationelegroepparentid
        AND og1.id = og2.operationelegroepparentid
        AND og3.id = product.operationelegroepid
        AND artikel.statusid IN ( 4)
        and winkel.nummer not in (103, 141)
        AND artikel.verkoopvariant != 1
        AND og1.id IN (405)
        AND og3.id NOT IN (567 , 569, 570, 571, 572, 573, 575)
        AND voorraadstand.voorraadstand=0
        AND winkelartikel.Bestelbaar=1
        
        order by winkelartikelid	