		select distinct
		promotie.id as 'EventId',
		'150_' as 'Warehouse Code',
		artikel.artikelnummer as 'Article Code',
		concat(promotie.naam,'-',promotieperiode.thema) as 'Event name',
		date_format(promotieperiode.startdatum, '%Y%m%d') as 'Start date',
		date_format(promotieperiode.einddatum, '%Y%m%d') as 'End date',
		winkelartikel.PrijsAdviesIncBtw as 'Price from',
		promotiewinkelartikel.promotiePrijsIncBtw as 'Price for',
                '' as 'Promotion Volume',
		'' as 'Promotion liftup',
		'' as 'Presentation stock',
		'' as 'Presentation stock start date',
		'' as 'Presentation stock end date',
		'' as 'Additional leadtime',
		'' as 'Order all at once ',
		'' as 'Realised Sales'
	from
		promotie,
		promotieperiode,
		promotiestatus,
		promotiemechanisme,
		promotieartikel,
		promotiewinkelartikel,
		artikel,
		winkelartikel,
		winkel
	where
		promotie.promotieperiodeid = promotieperiode.Id
			and promotieperiode.einddatum >= now()
			and promotie.promotieStatusId = promotiestatus.Id
			and promotie.promotieMechanismeId = promotiemechanisme.Id
			and promotieartikel.promotieId = promotie.Id
			and promotieartikel.artikelId = artikel.Id
			and promotiewinkelartikel.promotieArtikelId = promotieartikel.Id
			and promotiewinkelartikel.winkelartikelId = winkelartikel.Id
			and winkelartikel.winkelid = winkel.id