SELECT og2.omschrijving AS 'Operationele groep', artikel.omschrijving1 AS 'Artikelomschrijving', artikel.lengte AS 'Artikeldiepte', artikel.breedte AS 'Artikelbreedte', artikel.hoogte AS 'Artikelhoogte', winkelartikel.schapnummer AS 'Schapnummer', winkelartikel.planknummer AS 'Planknummer', winkelartikel.positie AS 'Positie op plank', winkelartikel.facings AS '#facings'  
FROM winkelartikel, winkelpartnerafdeling, artikel, operationelegroep og3, operationelegroep og2
WHERE winkelartikel.artikelid = artikel.id
AND winkelartikel.winkelpartnerafdelingid = winkelpartnerafdeling.id
AND winkelpartnerafdeling.operationelegroepid = og3.id
AND og2.id = og3.operationelegroepparentid
AND winkelartikel.winkelid = 9
AND artikel.actief = 1
AND winkelartikel.statusid = 4
AND og2.nummer in (581,371,421,351,331,451,361)
ORDER BY og2.omschrijving, schapnummer, planknummer, positie ASC