select 
    top20.artikelnummer,
    top20.artikelomschrijving,
    omzet.datum,
    omzet.scans,
    omzet.maxafrekentijd,
    derving.dervingdaglater
from
    (select 
        artikel.artikelnummer,
            artikel.omschrijving1 as 'artikelomschrijving',
            resultaat.artikelid
    from
        artikel, resultaat
    where
        resultaat.artikelid = artikel.Id
            and resultaat.jaar = year(date_sub(curdate(),interval(1)week))
            and resultaat.week = week(date_sub(curdate(),interval(1)week),3)
            and AfdelingId = 15
            and label = 'OPSW'
            and winkelid = 9
    order by resultaat.totaalMarqtVolume desc
    limit 20) top20
        left join
    (select 
        verkoop.datum,
            verkoopregel.ArtikelId,
            sum(verkoopregel.Hoeveelheid) as 'scans',
            max(verkoop.StartTijd) as 'maxafrekentijd'
    from
        verkoop, verkoopregel, rubriek
    where
        verkoop.id = verkoopregel.verkoopid
            and verkoopregel.RubriekId = rubriek.id
            and verkoop.jaar = year(date_sub(curdate(),interval(1)week))
            and verkoop.week = week(date_sub(curdate(),interval(1)week),3)
            and rubriek.nummer = 100
            and verkoopregel.AfdelingId = 15
            and verkoop.WinkelId = 9
    group by verkoop.datum , verkoopregel.ArtikelId) omzet ON omzet.artikelid = top20.artikelid
        left join
    (select 
        verkoop.datum,
            verkoopregel.ArtikelId,
            sum(verkoopregel.Hoeveelheid) as 'dervingDagLater'
    from
        verkoop, verkoopregel, rubriek
    where
        verkoop.id = verkoopregel.verkoopid
            and verkoopregel.RubriekId = rubriek.id
            and verkoop.jaar = year(date_sub(curdate(),interval(1)week))
            and verkoop.week = week(date_sub(curdate(),interval(1)week),3)
            and rubriek.derving = 1
            and verkoopregel.AfdelingId = 15
            and verkoop.WinkelId = 9
    group by verkoop.datum , verkoopregel.ArtikelId) derving ON derving.artikelid = omzet.artikelid
        and date_sub(derving.datum,
        interval (1) day) = omzet.datum