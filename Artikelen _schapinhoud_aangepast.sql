SELECT

winkelartikel.id,
artikel.ArtikelNummer,
artikel.Omschrijving1,
winkel.Naam,
schapinhoud,
schapinhoudaangepast,
winkelartikel.facings,
winkelartikel.facingdiepte,
winkelartikel.facingbreedte,
og1.omschrijving as pilaar,
status.Omschrijving,
og1.id,
og1.Omschrijving,
og2.id,
og2.Omschrijving,
og3.id,
og3.Omschrijving

FROM central.winkelartikel

join artikel on artikel.id=winkelartikel.ArtikelId
join winkel on winkel.id=winkelartikel.WinkelId
join status on status.id=artikel.StatusId

        inner join
    product ON product.id = artikel.ProductId
        inner join
    operationelegroep og3 ON og3.id = product.OperationeleGroepId
        inner join
    operationelegroep og2 ON og2.id = og3.OperationeleGroepParentId
        inner join
    operationelegroep og1 ON og1.id = og2.OperationeleGroepParentId


where artikel.statusid in (3,4)
#and og1.id in ()
#and og2.id in ()
#and og3.id in ()

order by artikel.ArtikelNummer, winkel.id


