select 
    concat(cast(winkel.nummer as char), '_') as 'Warehouse code',
    artikel.artikelnummer as 'Artikel Code',
    date_format(afzet.datum, '%Y%m%d') as 'AfzetDatum',
    afzet.totaalvolume as 'DagAfzet',
    afzet.TotaalMarqtVolume as 'DagRegels',
    ifnull(derving.totaalvolume, 0) as 'Derving',
    '' as 'Neeverkopen',
    0 as 'Promotionele verkopen',
    '' as 'Open',
    '' as 'Afzetkanaal 1',
    '' as 'Afzetkanaal 2',
    '' as 'Afzetkanaal 3',
    '' as 'Afzetkanaal 4',
    '' as 'Afzetkanaal 5'
from
    resultaat afzet
        left outer join
    resultaat derving ON (derving.Datum = afzet.Datum
        and derving.WinkelId = afzet.WinkelId
        and derving.ArtikelId = afzet.ArtikelId
        and derving.label = 'DVSD')
        inner join
    winkel ON winkel.id = afzet.winkelid
        inner join
    artikel ON artikel.id = afzet.artikelid
where
    afzet.jaar = 2014
        and afzet.datum = date_sub(curdate(), interval 1 day)
        and afzet.label = 'OPSD'