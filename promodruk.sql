select 
    pilaar, jaarweek, sum(verwachteomzet)
from
    (select 
        sum(promotieartikel.verwachtvolume) / count(distinct (winkelartikel.winkelid)) / (datediff(promotieperiode.einddatum, promotieperiode.startdatum) + 1) * ifnull(promotiewinkelartikel.promotiePrijsIncBtw, 0) as verwachteomzet,
            og1.Omschrijving as pilaar,
            datuminfo.jaarweek
    from
        promotie
    inner join promotieperiode ON promotieperiode.id = promotie.promotiePeriodeId
    inner join promotieartikel ON promotieartikel.promotieId = promotie.Id
    inner join promotiewinkelartikel ON promotiewinkelartikel.promotieArtikelId = promotieartikel.id
    inner join winkelartikel ON winkelartikel.id = promotiewinkelartikel.winkelartikelid
    inner join artikel ON artikel.id = promotieartikel.artikelId
    inner join product ON artikel.ProductId = product.Id
    inner join operationelegroep og3 ON product.OperationeleGroepId = og3.id
    inner join operationelegroep og2 ON og3.OperationeleGroepParentId = og2.Id
    inner join operationelegroep og1 ON og2.OperationeleGroepParentId = og1.Id
    inner join posmateriaal ON posmateriaal.id = promotieartikel.posmateriaalid
    right join datuminfo ON datuminfo.datum between promotieperiode.startdatum and promotieperiode.einddatum
    where
        promotieperiode.startdatum between date_sub(curdate(), interval 12 week) and date_add(curdate(), interval 12 week)
    group by og1.id , datuminfo.week , artikel.id) promo
group by pilaar , jaarweek
