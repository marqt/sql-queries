select 
    og1.nummer AS 'OG nivo 0 nummer',
    og1.omschrijving AS 'OG nivo 0 omschrijving',
og2.id,
    og2.nummer AS 'OG nivo 1 nummer',
    og2.omschrijving AS 'OG nivo 1 omschrijving',
    og3.id,
    og3.nummer AS 'OG nivo 2 nummer',
    og3.omschrijving AS 'OG nivo 2 omschrijving',
    count(product.id) AS 'Aantal producten'
from
    operationelegroep og1,
    operationelegroep og2,
    operationelegroep og3,
    product
where
    og2.id = og3.operationelegroepparentid
        and og1.id = og2.operationelegroepparentid
        and og3.id = product.operationelegroepid
        and og1.id >= 401
group by product.operationelegroepid
order by og1.omschrijving , og2.omschrijving , og3.omschrijving
