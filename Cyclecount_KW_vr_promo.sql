#
# Cyclecount KW voor telling promoartikelen op vrijdag
# Parameters defineren voor testen. Voor live in BIRT @Winkelid vervangen door ?
#        
#Set @WinkelID=2;
#
# Alle artikelen die vandaag -14/+7 dagen in de promo zijn.
#

(select
og2.id,
og2.Omschrijving,
    og3.omschrijving,
    winkelartikel.schapnummer,
    winkelartikel.planknummer,
    winkelartikel.positie,
    artikel.Omschrijving1,
    artikel.merk,
    winkelartikelvoorraadstand.voorraadstand,
    product.inhoud,
    meeteenheid.code,
    (select 
            lpad(barcode, 13, 0)
        from
            barcode
        where
        	artikel.id = barcode.artikelid
            and datumvanaf <= curdate()
                and besteleenheidid is null
        order by datumvanaf desc
        limit 1) as cebarcode
from
    artikel,
    winkelartikel,
    winkelartikelvoorraadstand,
    product,
    meeteenheid,
    operationelegroep og1,
    operationelegroep og2,
    operationelegroep og3
where
    artikel.id = winkelartikel.ArtikelId
        and winkelartikel.id = winkelartikelvoorraadstand.winkelartikelId
        and artikel.ProductId = product.id
        and product.inhoudmeeteenheidid = meeteenheid.Id
        and product.OperationeleGroepId = og3.id
        and og3.OperationeleGroepParentId = og2.id
        and og2.OperationeleGroepParentId = og1.id
        and winkelartikel.winkelid =  1
        #operationelegroep is KW-Food, Wijn of Non-food
        #and og2.id in (417 , 418, 419)
        #het veld inassortiment ligt niet in de afgelopen 14 dagen of is niet ingevuld
		and (inAssortiment < date_sub(curdate(), interval 14 day)
        or inAssortiment is null)
        #het artikel is/was in de promo de afgelopen 14- of de komende 7 dagen
        and exists (
 
 select 
  *
from
    promotie,
    promotieperiode,
    promotiestatus,
    promotieartikel
where
		artikelId = artikel.id
		and	promotie.promotieperiodeid = promotieperiode.Id
        and promotie.promotieStatusId = promotiestatus.Id
        and promotieartikel.promotieId = promotie.Id
        and date(promotieperiode.startdatum) >= date(date_sub(NOW(), interval 14 day))
        and date(promotieperiode.einddatum) <= date(date_sub(NOW(), interval -10 day))))