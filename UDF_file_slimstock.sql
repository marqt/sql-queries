(select 
    5 as 'Recordtype',
    year(curdate()) as 'Jaar',
    month(curdate()) as 'Maand',
    '' as 'leeg',
    concat(cast(winkel.nummer as char), '_') as 'Warehouse code',
    artikel.artikelnummer as 'Artikel Code',
    concat(cast(levergroep.id as char),
            ' - ',
            levergroep.naam) as 'Analyse Code 7',
    concat(cast(partner.nummer as char),
            ' - ',
            partner.Naam) as 'Analyse Code 8',
    artikel.merk as 'Analyse Code 9',
    '' as 'Analyse Code 10',
    if(product.GrondslagId = 1,
        'stuks',
        'wicht') as 'Analyse Code 11',
        cast(product.houdbaarheid as char) as 'Analyse Code 12',
        cast(product.HoudbaarheidDC as char) as 'Analyse Code 13',
    date_format(artikel.inAssortiment, '%Y%m%d') as 'Analyse Code 14',
    date_format(artikel.uitAssortiment, '%Y%m%d') as 'Analyse Code 15',
    if(leverinformatie.besteldagvandaag is not null,
        'Y',
        'N') as 'besteldagvandaag',
    besteleenheid.palletlaag as 'Analyse Code 17',
    besteleenheid.pallethoogte as 'Analyse Code 18',
    logistiekestroom.code as 'Analyse Code 19',
    if(winkelartikel.statusid in (3 , 4),
        'actueel',
        if(winkelartikel.statusid = 6,
            'niet in winkel',
            'gesaneerd')) as 'Analyse Code 20',
    '' as 'Analyse Code 21',
    artikel.bewerkt as 'Analyse Code 22',
    if(leverinformatiemorgen.besteldagvandaag is not null,
        'Y',
        'N') as 'Analyse Code 23',
    '' as 'Analyse Code 24',
    concat(og1.nummer, '-', og1.omschrijving) as 'Analyse Code 25',
    concat(og2.nummer, '-', og2.omschrijving) as 'Analyse Code 26',
    concat(og3.nummer, '-', og3.omschrijving) as 'Analyse Code 27',
    CAST((ifnull(winkelartikel.schapinhoudaangepast,
                winkelartikel.schapinhoud))
        AS CHAR) as 'Analyse Code 28',
    if(artikeltype.wichtAlsStuks = 1,
        CAST(format(product.normgewicht / 1000, 3) AS CHAR),
        '') as 'Analyse Code 29',
        '' as 'Analyse Code 30',
		'' as 'Analyse Code 31',
		'' as 'Analyse Code 32',
		'' as 'Analyse Code 33',
		'' as 'Analyse Code 34',
		'' as 'Analyse Code 35',
		'' as 'Analyse Code 36',
		'' as 'Analyse Code 37',
		'' as 'Analyse Code 38',
		'' as 'Analyse Code 39',
        winkelartikel.bestelgrootte as 'Analyse Code 40'
from
    (artikel, artikeltype)
        inner join
    product ON product.id = artikel.productid
        inner join
    winkelartikel ON winkelartikel.artikelid = artikel.id
        inner join
    winkelartikelexporthistorie ON curdate() between winkelartikelexporthistorie.datumGeldigVanaf and winkelartikelexporthistorie.datumGeldigTot
        and winkelartikelexporthistorie.winkelartikelid = winkelartikel.id
        inner join
    besteleenheid ON besteleenheid.id = winkelartikelexporthistorie.bestelEenheidId
        inner join
    winkel ON winkel.id = winkelartikel.winkelid
        inner join
    winkelpartnerafdeling ON winkelartikel.winkelpartnerafdelingId = winkelpartnerafdeling.Id
        inner join
    levergroep ON levergroep.id = ifnull(winkelartikel.LeverGroepId,
            winkelpartnerafdeling.DefaultLeverGroepId)
        inner join
    logistiekestroom ON logistiekestroom.id = levergroep.logistiekeStroomId
        inner join
    partner ON levergroep.PartnerId = partner.Id
        inner join
    commercielegroep cg3 ON product.CommercieleGroepId = cg3.id
        inner join
    commercielegroep cg2 ON cg3.commercielegroepParentId = cg2.id
        inner join
    commercielegroep cg1 ON cg2.commercielegroepParentId = cg1.id
        inner join
    operationelegroep og3 ON product.OperationeleGroepId = og3.id
        inner join
    operationelegroep og2 ON og3.OperationelegroepParentId = og2.id
        inner join
    operationelegroep og1 ON og2.OperationelegroepParentId = og1.id
        left join
    (select 
        levergroepid, volgendebesteldatum as besteldagvandaag
    from
        v_bestellevermoment
    where
        volgendebesteldatum = curdate()) leverinformatie ON leverinformatie.levergroepid = levergroep.Id
        left join
    (select 
        levergroepid, volgendebesteldatum as besteldagvandaag
    from
        v_bestellevermoment
    where
        volgendebesteldatum = date_add(curdate(), interval 1 day)) leverinformatiemorgen ON leverinformatiemorgen.levergroepid = levergroep.Id
where
    artikel.statusid in (3 , 4, 6)
        and artikel.artikelTypeId = artikeltype.id
        and winkelpartnerafdeling.afdelingId not in (8 , 9, 13, 14)
        and winkel.nummer != 103
        and artikel.verkoopvariant != 1) union all (select 
    5 as 'Recordtype',
    year(curdate()) as 'Jaar',
    month(curdate()) as 'Maand',
    '' as 'leeg',
    '150_' as 'Warehouse code',
    artikel.artikelnummer as 'Artikel Code',
    concat(cast(levergroep.id as char),
            ' - ',
            levergroep.naam) as 'Analyse Code 7',
    concat(cast(partner.nummer as char),
            ' - ',
            partner.Naam) as 'Analyse Code 8',
    artikel.merk as 'Analyse Code 9',
    '' as 'Analyse Code 10',
    if(product.GrondslagId = 1,
        'stuks',
        'wicht') as 'Analyse Code 11',
    '' as 'Analyse Code 12',
    '' as 'Analyse Code 13',
    date_format(artikel.inAssortiment, '%Y%m%d') as 'Analyse Code 14',
    date_format(artikel.uitAssortiment, '%Y%m%d') as 'Analyse Code 15',
    '' as 'besteldagvandaag',
    besteleenheid.palletlaag as 'Analyse Code 17',
    besteleenheid.pallethoogte as 'Analyse Code 18',
    logistiekestroom.code as 'Analyse Code 19',
    '' as 'Analyse Code 20',
    '' as 'Analyse Code 21',
    artikel.bewerkt as 'Analyse Code 22',
    '' as 'Analyse Code 23',
    '' as 'Analyse Code 24',
    concat(og1.nummer, '-', og1.omschrijving) as 'Analyse Code 25',
    concat(og2.nummer, '-', og2.omschrijving) as 'Analyse Code 26',
    concat(og3.nummer, '-', og3.omschrijving) as 'Analyse Code 27',
    '' as 'Analyse Code 28',
    '' as 'Analyse Code 29',
    '' as 'Analyse Code 30',
	'' as 'Analyse Code 31',
	'' as 'Analyse Code 32',
	'' as 'Analyse Code 33',
	'' as 'Analyse Code 34',
	'' as 'Analyse Code 35',
	'' as 'Analyse Code 36',
	'' as 'Analyse Code 37',
	'' as 'Analyse Code 38',
	'' as 'Analyse Code 39',
    '' as 'Analyse Code 40'
from
    artikel
        inner join
    product ON product.id = artikel.productid
        inner join
    winkelartikel ON winkelartikel.artikelid = artikel.id
        inner join
    winkelartikelexporthistorie ON curdate() between winkelartikelexporthistorie.datumGeldigVanaf and winkelartikelexporthistorie.datumGeldigTot
        and winkelartikelexporthistorie.winkelartikelid = winkelartikel.id
        inner join
    besteleenheid ON besteleenheid.id = winkelartikelexporthistorie.bestelEenheidId
        inner join
    winkel ON winkel.id = winkelartikel.winkelid
        inner join
    winkelpartnerafdeling ON winkelartikel.winkelpartnerafdelingId = winkelpartnerafdeling.Id
        inner join
    levergroep ON levergroep.id = ifnull(winkelartikel.LeverGroepId,
            winkelpartnerafdeling.DefaultLeverGroepId)
        inner join
    logistiekestroom ON logistiekestroom.id = levergroep.logistiekeStroomId
        inner join
    partner ON levergroep.PartnerId = partner.Id
        inner join
    commercielegroep cg3 ON product.CommercieleGroepId = cg3.id
        inner join
    commercielegroep cg2 ON cg3.commercielegroepParentId = cg2.id
        inner join
    commercielegroep cg1 ON cg2.commercielegroepParentId = cg1.id
        inner join
    operationelegroep og3 ON product.OperationeleGroepId = og3.id
        inner join
    operationelegroep og2 ON og3.OperationelegroepParentId = og2.id
        inner join
    operationelegroep og1 ON og2.OperationelegroepParentId = og1.id
where
    artikel.statusid in (3 , 4, 6)
        and winkelpartnerafdeling.afdelingId not in (8 , 9, 13, 14)
        and artikel.verkoopvariant != 1
group by artikel.Id)