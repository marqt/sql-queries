select 
    8 as 'Recordtype',
    year(curdate()) as 'Jaar',
    month(curdate()) as 'Maand',
    week(curdate()) as 'Week',
    concat(winkel.nummer, '_') as 'Warehouse code',
    artikel.artikelnummer as 'Artikel Code',
    inkooporder.orderkenmerk as 'Inkoop Order Nummer',
    date_format(inkooporder.verwachteLeverDatumWinkelTijd,
            '%Y%m%d') as 'Verwachte leverdatum',
    round(inkooporderregel.CEbesteld) as 'Aantal in bestelling',
    concat(logistiekepartner.nummer,
            ' - ',
            logistiekepartner.naam) as 'PO leverancier details',
    concat(levergroep.id, ' - ', levergroep.naam) as 'PO opmerkingen',
    '' as 'PO oorspronkelijk aantal',
    '' as 'PO geleverd aantal',
    concat(administratievepartner.nummer,
            ' - ',
            administratievepartner.naam) as 'PO vrije tekst 1',
    inkooporderstatus.naam as 'PO vrije tekst 2',
    '' as 'PO vrij nummer 1',
    '' as 'PO vrij nummer 2'
from
    artikel,
    winkel,
    winkelartikel,
    winkelpartnerafdeling,
    partner administratievepartner,
    partner logistiekepartner,
    levergroep,
    inkooporder,
    inkooporderregel,
    inkooporderstatus
where
    winkelartikel.artikelid = artikel.id
        and winkelartikel.winkelid = winkel.id
        and inkooporder.winkelid = winkelartikel.winkelid
        and inkooporderregel.artikelid = winkelartikel.artikelid
        and inkooporder.id = inkooporderregel.orderId
        and winkelartikel.winkelpartnerafdelingId = winkelpartnerafdeling.Id
        and levergroep.id = ifnull(winkelartikel.LeverGroepId,
            winkelpartnerafdeling.DefaultLeverGroepId)
        and winkelpartnerafdeling.PartnerId = administratievepartner.Id
        and levergroep.PartnerId = logistiekepartner.Id
        and inkooporder.statusid = inkooporderstatus.id
        and inkooporder.StatusId between 2 and 7