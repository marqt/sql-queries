select 
    partner.naam, count(distinct(artikel.id))/(select count(id) from artikel where actief = 1) perc
from
    partner,
    winkelpartnerafdeling,
    winkelartikel,
    artikel
where
    partner.id = winkelpartnerafdeling.partnerid
        and winkelpartnerafdeling.id = winkelartikel.winkelpartnerafdelingid
        and winkelartikel.artikelid = artikel.id
and artikel.actief = 1
group by partner.id
order by perc desc