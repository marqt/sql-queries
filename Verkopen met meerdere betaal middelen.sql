select 
verkoop.id,
count(*) as cnt,    
winkel.nummer,
    verkoop.week,
    verkoop.datum,
    Betaalmiddel.id,
    Betaalmiddel.omschrijving#,
    #sum(verkoopbetaling.bedrag),
	#sum(verkoop.bedragtotaal),
    #count(*),
	#count(distinct(verkoop.id))
from
    verkoop
        inner join
    winkel ON winkel.id = verkoop.winkelid
        inner join
    verkoopbetaling ON verkoopbetaling.verkoopid = verkoop.id
        inner join
    Betaalmiddel ON Betaalmiddel.id = verkoopbetaling.betaalmiddelId
where
    verkoop.TransactieSoort = 'KAS'
        and Jaar = '2013'
        and week = 40
        and betaalmiddel.id > 1
group by verkoop.id having cnt > 1
order by verkoop.datum , winkel.nummer , verkoopbetaling.bedrag