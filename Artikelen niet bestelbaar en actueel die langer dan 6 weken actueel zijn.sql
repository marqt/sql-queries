select 
    partner.naam, artikel.artikelnummer, artikel.omschrijving1
from
    artikel,
    winkelartikel,
    winkelpartnerafdeling,
    partner
where
    artikel.id = winkelartikel.artikelid
        and winkelartikel.winkelpartnerafdelingid = winkelpartnerafdeling.id
        and winkelpartnerafdeling.partnerid = partner.id
        and artikel.actief != 1
        and artikel.statusid = 4
        AND NOT EXISTS( SELECT DISTINCT
            (recordid)
        FROM
            historie
        WHERE
            tabel = 'artikel' AND veld = 'statusid'
                AND laatstewijziging > date_sub(now(), INTERVAL 6 WEEK)
                AND artikel.id = historie.recordid)
group by artikel.id