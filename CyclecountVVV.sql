#cyclecount vlees vis vleeswaren


# Winkelid om te testen
set @Winkelid =9;

# voor productie @winkelid vervangen door ?

# eerste deel
# de 10 meeste verkochte wichtartikelen van gisteren
(select 
    og3.omschrijving,
    winkelartikel.schapnummer,
    winkelartikel.planknummer,
    winkelartikel.positie,
    artikel.Omschrijving1,
    artikel.merk,
    winkelartikelvoorraadstand.voorraadstand,
    ifnull(product.normgewicht, product.inhoud) as inhoud,
    meeteenheid.code,
    (select 
            lpad(barcode, 13, 0)
        from
            barcode
        where
            artikel.id = barcode.artikelid
                and datumvanaf <= curdate()
                and besteleenheidid is null
                and barcode.barcode not like '23%'
        order by datumvanaf desc
        limit 1) as cebarcode,
    winkelartikel.prijsadviesincbtw,
    product.grondslagid
from
    artikel,
    winkelartikel,
    winkelartikelvoorraadstand,
    product,
    meeteenheid,
    operationelegroep og1,
    operationelegroep og2,
    operationelegroep og3,
    winkelpartnerafdeling
where
    artikel.id = winkelartikel.ArtikelId
        and winkelartikel.id = winkelartikelvoorraadstand.winkelartikelId
        and artikel.ProductId = product.id
        and product.inhoudmeeteenheidid = meeteenheid.Id
        and product.OperationeleGroepId = og3.id
        and og3.OperationeleGroepParentId = og2.id
        and og2.OperationeleGroepParentId = og1.id
        and winkelartikel.WinkelPartnerAfdelingId = winkelpartnerafdeling.Id
        # artikelstatus gewijzigd of actueel
        and artikel.StatusId in (3 , 4)
        # winkelartikel is bestelbaar
        and winkelartikel.bestelbaar = 1
        # winkelselectie
        and winkelartikel.winkelid = @Winkelid
        # operationele groep 
        and og2.id in (411,412,413)
        # grondslag is wicht
        and grondslagid = 2
        #artikel komt voor in top 10 meest verkochte producten gisteren
        and artikel.id in (select 
            artikel.id
        from
            resultaat,
            artikel,
            product,
            operationelegroep
        where
            resultaat.artikelid = artikel.id
                and product.id = artikel.productid
                and operationelegroep.id = product.OperationeleGroepId
                and operationelegroep.OperationeleGroepParentId in (412,411,413)
                and winkelid = @Winkelid
                and label = 'OPSD'
                and datum = date_sub(curdate(), interval 1 day)
        order by TotaalMarqtWaarde desc)
        
# maximaal 10 artikelen        
limit 10) 




# toevoegen resutaten tweede deel muv als ze al in eerder deel voorkomen. Dus union ipv Union all.
union



# tweede deel
 (select 
    og3.omschrijving,
    winkelartikel.schapnummer,
    winkelartikel.planknummer,
    winkelartikel.positie,
    artikel.Omschrijving1,
    artikel.merk,
    winkelartikelvoorraadstand.voorraadstand,
    ifnull(product.normgewicht, product.inhoud) as inhoud,
    meeteenheid.code,
    (select 
            lpad(barcode, 13, 0)
        from
            barcode
        where
            artikel.id = barcode.artikelid
                and datumvanaf <= curdate()
                and barcode.barcode not like '23%'
                and besteleenheidid is null
        order by datumvanaf desc
        limit 1) as cebarcode,
    winkelartikel.prijsadviesincbtw,
    product.grondslagid
from
    artikel,
    winkelartikel,
    winkelartikelvoorraadstand,
    product,
    meeteenheid,
    operationelegroep og1,
    operationelegroep og2,
    operationelegroep og3,
    winkelpartnerafdeling
where
    artikel.id = winkelartikel.ArtikelId
        and winkelartikel.id = winkelartikelvoorraadstand.winkelartikelId
        and artikel.ProductId = product.id
        and product.inhoudmeeteenheidid = meeteenheid.Id
        and product.OperationeleGroepId = og3.id
        and og3.OperationeleGroepParentId = og2.id
        and og2.OperationeleGroepParentId = og1.id
        and winkelartikel.WinkelPartnerAfdelingId = winkelpartnerafdeling.Id
		# artikelstatus gewijzigd of actueel
        and artikel.StatusId in (3 , 4)
        # winkelartikel is bestelbaar
        and winkelartikel.bestelbaar = 1
        and winkelartikel.winkelid = @Winkelid
        # in bestreffende operationele groepen
        and og2.id in (412,411,413)
        #artikel is wicht
        and grondslagid = 2
        # artikel zit in top 5 van meest gedroven producten gisteren
        and artikel.id in (select 
            artikel.id
        from
            resultaat,
            artikel,
            product,
            operationelegroep
        where
            resultaat.artikelid = artikel.id
            	and resultaat.winkelid = @Winkelid
                and product.id = artikel.productid
                and operationelegroep.id = product.OperationeleGroepId
                and operationelegroep.OperationeleGroepParentId in (412,411,413)
                and label = 'DVSD'
                and datum = date_sub(curdate(), interval 1 day)
        order by TotaalMarqtWaarde desc)
limit 5) 


# derdel deel, willekeurig 5 producten. wederom een union
union 

(select 
    og3.omschrijving,
    winkelartikel.schapnummer,
    winkelartikel.planknummer,
    winkelartikel.positie,
    artikel.Omschrijving1,
    artikel.merk,
    winkelartikelvoorraadstand.voorraadstand,
    ifnull(product.normgewicht, product.inhoud) as inhoud,
    meeteenheid.code,
    (select 
            lpad(barcode, 13, 0)
        from
            barcode
        where
            artikel.id = barcode.artikelid
                and datumvanaf <= curdate()
                and barcode.barcode not like '23%'
                and besteleenheidid is null
        order by datumvanaf desc
        limit 1) as cebarcode,
    winkelartikel.prijsadviesincbtw,
    product.grondslagid
from
    artikel,
    winkelartikel,
    winkelartikelvoorraadstand,
    product,
    meeteenheid,
    operationelegroep og1,
    operationelegroep og2,
    operationelegroep og3,
    winkelpartnerafdeling
where
    artikel.id = winkelartikel.ArtikelId
        and winkelartikel.id = winkelartikelvoorraadstand.winkelartikelId
        and artikel.ProductId = product.id
        and product.inhoudmeeteenheidid = meeteenheid.Id
        and product.OperationeleGroepId = og3.id
        and og3.OperationeleGroepParentId = og2.id
        and og2.OperationeleGroepParentId = og1.id
        and winkelartikel.WinkelPartnerAfdelingId = winkelpartnerafdeling.Id
        # winkelartikel actueel of gewijzigd
        and artikel.StatusId in (3 , 4)
        #winkelartikel is bestelbaar
        and winkelartikel.bestelbaar = 1
        and winkelartikel.winkelid = @Winkelid
        # in betreffende operationele groep
        and og2.id in (412,411,413)
        # grondslag is wicht
        and grondslagid = 2
	order by rand()
limit 5) 


#4e deel, willekeurig 5 artikelen uit de groep gevolgelte. met een union

union (select 
    og3.omschrijving,
    winkelartikel.schapnummer,
    winkelartikel.planknummer,
    winkelartikel.positie,
    artikel.Omschrijving1,
    artikel.merk,
    winkelartikelvoorraadstand.voorraadstand,
    ifnull(product.normgewicht, product.inhoud) as inhoud,
    meeteenheid.code,
    (select 
            lpad(barcode, 13, 0)
        from
            barcode
        where
            artikel.id = barcode.artikelid
                and datumvanaf <= curdate()
                and besteleenheidid is null
                and barcode.barcode not like '23%'
        order by datumvanaf desc
        limit 1) as cebarcode,
    winkelartikel.prijsadviesincbtw,
    product.grondslagid
from
    artikel,
    winkelartikel,
    winkelartikelvoorraadstand,
    product,
    meeteenheid,
    operationelegroep og1,
    operationelegroep og2,
    operationelegroep og3,
    winkelpartnerafdeling
where
    artikel.id = winkelartikel.ArtikelId
        and winkelartikel.id = winkelartikelvoorraadstand.winkelartikelId
        and artikel.ProductId = product.id
        and product.inhoudmeeteenheidid = meeteenheid.Id
        and product.OperationeleGroepId = og3.id
        and og3.OperationeleGroepParentId = og2.id
        and og2.OperationeleGroepParentId = og1.id
        and winkelartikel.WinkelPartnerAfdelingId = winkelpartnerafdeling.Id
        #status actueel of gewijzigd
        and artikel.StatusId in (3 , 4)
        # winkelartikel is bestelbaar
        and winkelartikel.bestelbaar = 1
        and winkelartikel.winkelid = @Winkelid
        and og3.id = 477
        # artikel is wicht
        and grondslagid = 2
	order by rand()
limit 5)


#5e deel, alle artikelen met een 0 of negatieve voorraad stand

 union (select 
    og3.omschrijving,
    winkelartikel.schapnummer,
    winkelartikel.planknummer,
    winkelartikel.positie,
    artikel.Omschrijving1,
    artikel.merk,
    winkelartikelvoorraadstand.voorraadstand,
    ifnull(product.normgewicht, product.inhoud) as inhoud,
    meeteenheid.code,
    (select 
            lpad(barcode, 13, 0)
        from
            barcode
        where
            artikel.id = barcode.artikelid
                and datumvanaf <= curdate()
                and besteleenheidid is null
                and barcode.barcode not like '23%'
        order by datumvanaf desc
        limit 1) as cebarcode,
    winkelartikel.prijsadviesincbtw,
    product.grondslagid
from
    artikel,
    winkelartikel,
    winkelartikelvoorraadstand,
    product,
    meeteenheid,
    operationelegroep og1,
    operationelegroep og2,
    operationelegroep og3,
    winkelpartnerafdeling
where
    artikel.id = winkelartikel.ArtikelId
        and winkelartikel.id = winkelartikelvoorraadstand.winkelartikelId
        and artikel.ProductId = product.id
        and product.inhoudmeeteenheidid = meeteenheid.Id
        and product.OperationeleGroepId = og3.id
        and og3.OperationeleGroepParentId = og2.id
        and og2.OperationeleGroepParentId = og1.id
        and winkelartikel.WinkelPartnerAfdelingId = winkelpartnerafdeling.Id
        #status is gewijzigd of actueel
        and artikel.StatusId in (3 , 4)
        #winkelartikel is bestelbaar
        and winkelartikel.bestelbaar = 1
        and winkelartikel.winkelid = @Winkelid
        # komt voor in operationele groep
        and og1.id in (403)
        and voorraadstand <= 0
        and grondslagid = 2)