SELECT 
                artikel.artikelnummer AS 'Artikelnummer',
                artikel.omschrijving1 AS 'Artikelomschrijving',
                winkelartikel.bestelcodepartner AS 'Bestelcodepartner',
                artikel.merk AS 'Merk',
                partner.nummer AS 'Partnernummer',
                partner.naam AS 'Partnernaam',
                grondslag.omschrijving AS 'Grondslag',
                    concat(artikel.lengte,
                        'x',
                        artikel.hoogte,
                        'x',
                        artikel.breedte) AS 'Afmetingen (lxhxb)',
                product.inhoud AS 'Inhoud',
                if(winkelartikel.bestelbaar, 'ja', 'nee') AS 'Artikel bestelbaar',
                if(Artikel.bewerkt, 'ja', 'nee') AS 'Artikel bewerkt',
                format(winkelartikel.PrijsInkoop,
                    2,
                    'NL_nl') AS 'Prijsinkoop',
                format(winkelartikel.PrijsAdviesIncBtw,
                    2,
                    'NL_nl') AS 'Prijsverkoop',
                format(winkelartikel.marge / 100,
                    3,
                    'NL_nl') AS 'Brutomarge artikel',
                (SELECT 
                        format(besteleenheid.bestelhoeveelheid,
                                0,
                                'NL_nl') AS 'Bestelhoeveelheid'
                    FROM
                        besteleenheid
                    WHERE
                        artikel.id = besteleenheid.artikelid
                    ORDER BY besteleenheid.datumvanaf DESC, besteleenheid.id desc
                    LIMIT 1) AS 'Bestelhoeveelheid',
                concat(op1.Nummer,
                        '.',
                        op2.nummer,
                        '.',
                        op3.nummer,
                        ' ',
                        op1.Omschrijving,
                        '-',
                        op2.Omschrijving,
                        '-',
                        op3.Omschrijving) as 'Productcluster',
                winkel.nummer AS 'Winkelnummer',
                winkel.afkorting AS 'Winkel',
                resultaat.jaar AS 'Jaar',
                resultaat.periode AS 'Periode',
                resultaat.week AS 'Week',
                resultaat.datum AS 'Datum',
                format(resultaat.totaalbedrag,
                    2,
                    'NL_nl') AS 'Omzet',
                format(resultaat.totaalmarqtwaarde,
                    2,
                    'NL_nl') AS 'Totaalmarqtwaarde',
                format((resultaat.Totaalbedrag - resultaat.Totaalbtw),
                    2,
                    'NL_nl') AS 'Netto omzet',
                format((resultaat.Totaalbedrag - resultaat.Totaalbtw - resultaat.totaalmarqtwaarde),
                    2,
                    'NL_nl') AS 'POS marge',
                format(resultaat.Totaalvolume,
                    2,
                    'NL_nl') AS 'Volume',
                format(resultaat.totaalmarqtvolume,
                    0,
                    'NL_nl') AS 'Scans',
                if(rubriek.nummer = 100,
                    'Omzet',
                    if(rubriek.nummer = 2400,
                        'Kassakorting',
                        if(rubriek.nummer = 3600,
                            'Sampling',
                            if(rubriek.nummer = 4000,
                                'Inventarisaties',
                                if(rubriek.nummer = 3699,
                                    'Opboeking Voorraad',
                                    if(rubriek.nummer = 3606,
                                        'VleesVis-Verwaarding',
                                        if(rubriek.derving = 1,
                                            'Derving',
                                            'Overig'))))))) as rubriek,
                winkel.WinkelSoortFinancieel AS 'Winkelsoort',
                concat(rubriek.nummer,': ',rubriek.omschrijving) as rubrieknummer
            FROM
                resultaat,
                winkel,
                artikel,
                product,
                winkelartikel,
                grondslag,
                #meeteenheid,
                partner,
                rubriek,
                operationelegroep as op1,
                operationelegroep as op2,
                operationelegroep as op3
            where
                resultaat.winkelid = winkel.id
                    AND resultaat.artikelid = artikel.id
                    AND artikel.id = winkelartikel.artikelid
                    AND winkel.id = winkelartikel.winkelid
                    AND product.id = artikel.productid
                    AND grondslag.id = product.grondslagid
                    #AND meeteenheid.Id = product.InhoudMeeteenheidId
                    AND resultaat.partnerid = partner.id
                    AND resultaat.rubriekid = rubriek.id
                    AND op3.id = product.operationeleGroepId
                    AND op2.id = op3.operationeleGroepParentId
                    AND op1.id = op2.operationeleGroepParentId
					AND resultaat.label = 'OPSW' 
                    AND resultaat.jaar >= 2015
                    AND op2.id in (411,412,413)