select 
    winkel.nummer,
    winkel.NaamKort,
    week,
    weekdag,
    datum,
    concat(cg1.Omschrijving,
            '-',
            cg2.Omschrijving,
            '-',
            cg3.Omschrijving) as commercielegroep,
    round(1-sum(totaalmarqtWaarde) / sum(TotaalBedrag - TotaalBtw),3) as marge
from
    resultaat,
    commercielegroep cg1,
    commercielegroep cg2,
    commercielegroep cg3,
    winkel
where
    resultaat.winkelid = winkel.id
        and resultaat.CommercieleGroepId = cg3.id
        and cg3.CommercieleGroepParentId = cg2.id
        and cg2.CommercieleGroepParentId = cg1.Id
        and resultaat.jaar = 2014
        and resultaat.week = 30
        and resultaat.label = 'OPSD'
group by winkel.id, cg3.id, resultaat.datum