select 
id,
nummer,
Winkeltype,
winkel.id as winkelid,
resultaatdezeweek.week,
omzet_dezeweek,
brutomarge_dezeweek+kassakorting_dezeweek as brutomarge_dezeweek,
scans_dezeweek,
nettoomzet_dezeweek,
winkel.id as winkelid,
klanten_dezeweek,	
winkel.id as winkelid,
derving_dezeweek,	
winkel.id as winkelid,
omzet_vorigeweek,	
brutomarge_vorigeweek,	
scans_vorigeweek,	
nettoomzet_vorigeweek,
winkel.id as winkelid,
klanten_vorigeweek,	
winkel.id as winkelid,
derving_vorigeweek,	
winkel.id as winkelid,
omzet_vorigjaar,	
brutomarge_vorigjaar,	
scans_vorigjaar,	
nettoomzet_vorigjaar,
winkel.id as winkelid,
klanten_vorigjaar,	
winkel.id as winkelid,
derving_vorigjaar,
kassakorting_dezeweek,
kassakorting_vorigjaar,
kassakorting_vorigeweek

from
    (select 
        id, nummer, if(winkelsoortfinancieel = 'NONID','NIETID',winkelsoortfinancieel) as Winkeltype
    from
        winkel
    where
        type = 'winkel' and nummer <= 166) winkel
        left join
    (select 
        winkelid,
            week,
            sum(totaalbedrag) as 'omzet_dezeweek',
            sum(Totaalbedrag) - sum(Totaalbtw) - sum(totaalmarqtwaarde) as 'brutomarge_dezeweek' ,
            sum(Totaalmarqtvolume) as 'scans_dezeweek',
            sum(Totaalbedrag) - sum(Totaalbtw) as 'nettoomzet_dezeweek'
    from
        resultaat
    where
        resultaat.label = 'OPSW'
            and resultaat.jaar = year(date_sub(curdate(), interval 1 week))
            and resultaat.week = week(date_sub(curdate(), interval 1 week), 3)
    group by winkelid) resultaatdezeweek ON winkel.id = resultaatdezeweek.winkelid
        left join
    (select 
        winkelid, count(verkoop.id) as 'klanten_dezeweek'
    from
        verkoop
    where
        TransactieSoort = 'KAS'
            and verkoop.jaar = year(date_sub(curdate(), interval 1 week))
            and verkoop.week = week(date_sub(curdate(), interval 1 week), 3)
            and verkoop.BedragOmzet > 0
    group by winkelid) klantendezeweek ON winkel.id = klantendezeweek.winkelid
        left join
    (select 
        winkelid, sum(totaalmarqtwaarde) as 'derving_dezeweek'
    from
        resultaat
    where
        resultaat.label = 'DVSW'
            and resultaat.jaar = year(date_sub(curdate(), interval 1 week))
            and resultaat.week = week(date_sub(curdate(), interval 1 week), 3)
    group by winkelid) dervingdezeweek ON winkel.id = dervingdezeweek.winkelid
        left join
    (select 
        winkelid,
            sum(totaalbedrag) as 'omzet_vorigeweek',
            sum(Totaalbedrag) - sum(Totaalbtw) - sum(totaalmarqtwaarde) as 'brutomarge_vorigeweek',
            sum(Totaalmarqtvolume) as 'scans_vorigeweek',
            sum(Totaalbedrag) - sum(Totaalbtw) as 'nettoomzet_vorigeweek'
    from
        resultaat
    where
        resultaat.label = 'OPSW'
            and resultaat.jaar = year(date_sub(curdate(), interval 2 week))
            and resultaat.week = week(date_sub(curdate(), interval 2 week), 3)
    group by winkelid) resultaatvorigeweek ON winkel.id = resultaatvorigeweek.winkelid
        left join
    (select 
        winkelid, count(verkoop.id) as 'klanten_vorigeweek'
    from
        verkoop
    where
        TransactieSoort = 'KAS'
            and verkoop.jaar = year(date_sub(curdate(), interval 2 week))
            and verkoop.week = week(date_sub(curdate(), interval 2 week), 3)
            and verkoop.BedragOmzet > 0
    group by winkelid) klantenvorigeweek ON winkel.id = klantenvorigeweek.winkelid
        left join
    (select 
        winkelid, sum(totaalmarqtwaarde) as 'derving_vorigeweek'
    from
        resultaat
    where
        resultaat.label = 'DVSW'
            and resultaat.jaar = year(date_sub(curdate(), interval 2 week))
            and resultaat.week = week(date_sub(curdate(), interval 2 week), 3)
    group by winkelid) dervingvorigeweek ON winkel.id = dervingvorigeweek.winkelid
        left join
    (select 
        winkelid,
            sum(totaalbedrag) as 'omzet_vorigjaar',
            sum(Totaalbedrag) - sum(Totaalbtw) - sum(totaalmarqtwaarde) as 'brutomarge_vorigjaar',
            sum(Totaalmarqtvolume) as 'scans_vorigjaar',
            sum(Totaalbedrag) - sum(Totaalbtw) as 'nettoomzet_vorigjaar'
    from
        resultaat
    where
        resultaat.label = 'OPSW'
            and resultaat.jaar = year(date_sub(curdate(), interval 1 year))
            and resultaat.week = week(date_sub(curdate(), interval 1 week), 3)
    group by winkelid) resultaatdezeweekvorigjaar ON winkel.id = resultaatdezeweekvorigjaar.winkelid
        left join
    (select 
        winkelid, count(verkoop.id) as 'klanten_vorigjaar'
    from
        verkoop
    where
        TransactieSoort = 'KAS'
            and verkoop.jaar = year(date_sub(curdate(), interval 1 year))
            and verkoop.week = week(date_sub(curdate(), interval 1 week), 3)
            and verkoop.BedragOmzet > 0
    group by winkelid) klantendezeweekvorigjaar ON winkel.id = klantendezeweekvorigjaar.winkelid
        left join
    (select 
        winkelid, sum(totaalmarqtwaarde) as 'derving_vorigjaar'
    from
        resultaat
    where
        resultaat.label = 'DVSW'
            and resultaat.jaar = year(date_sub(curdate(), interval 1 year))
            and resultaat.week = week(date_sub(curdate(), interval 1 week), 3)
    group by winkelid) dervingdezeweekvorigjaar ON winkel.id = dervingdezeweekvorigjaar.winkelid
    
           left join
    (select 
        winkelid,
            week,
            sum(Totaalbedrag) - sum(Totaalbtw) as 'kassakorting_dezeweek'
    from
        resultaat, rubriek
    where
        resultaat.RubriekId=rubriek.id
        and
        resultaat.label = 'BASW'
			and rubriek.Nummer in  (2400)
            and resultaat.jaar = year(date_sub(curdate(), interval 1 week))
            and resultaat.week = week(date_sub(curdate(), interval 1 week), 3)
    group by winkelid) kassakortingdezeweek ON winkel.id = kassakortingdezeweek.winkelid
    
     left join
    (select 
        winkelid,
            week,
            sum(Totaalbedrag) - sum(Totaalbtw) as 'kassakorting_vorigjaar'
    from
        resultaat, rubriek
    where
        resultaat.RubriekId=rubriek.id
        and
        resultaat.label = 'BASW'
			and rubriek.Nummer in  (2400)
            and resultaat.jaar = year(date_sub(curdate(), interval 1 year))
            and resultaat.week = week(date_sub(curdate(), interval 1 week), 3)
    group by winkelid) kassakortingvorigjaar ON winkel.id = kassakortingvorigjaar.winkelid
    
         left join
    (select 
        winkelid,
            week,
            sum(Totaalbedrag) - sum(Totaalbtw) as 'kassakorting_vorigeweek'
    from
        resultaat, rubriek
    where
        resultaat.RubriekId=rubriek.id
        and
        resultaat.label = 'BASW'
			and rubriek.Nummer in  (2400)
            and resultaat.jaar = year(date_sub(curdate(), interval 2 week))
            and resultaat.week = week(date_sub(curdate(), interval 2 week), 3)
    group by winkelid) kassakortingvorigeweek ON winkel.id = kassakortingvorigeweek.winkelid