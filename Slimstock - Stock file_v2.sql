select 
    3 as 'Recordtype',
    year(curdate()) as 'Jaar',
    month(curdate()) as 'Maand',
    week(curdate(), 3) as 'Week',
    concat(cast(winkel.nummer as char), '_') as 'Warehouse code',
    artikel.artikelnummer as 'Artikel Code',
    round(winkelartikel.voorraadstand) as 'Fysieke Voorraad',
    '' as 'Na te Leveren',
    '' as 'Totale voorraad in bestelling',
    '' as 'Aantal in bestelling',
    '' as 'Verwachte leverdatum',
    '' as 'Aantal in bestelling: (2)',
    '' as 'Verwachte leverdatum: (2)',
    '' as 'Aantal in bestelling: (3)',
    '' as 'Verwachte leverdatum: (3)',
    '' as 'Aantal in bestelling: (4)',
    '' as 'Verwachte leverdatum: (4)',
    '' as 'Aantal in bestelling: (5)',
    '' as 'Verwachte leverdatum: (5)',
    '' as 'Aantal in bestelling: (6)',
    '' as 'Verwachte leverdatum: (6)',
    '' as 'Criterium 1',
    '' as 'Criterium 2',
    '' as 'Criterium 3',
    '' as 'Criterium 4',
    '' as 'ABC Klasse',
    if(winkel.nummer = 141,
        0,
        round(winkelartikel.schapinhoud)) as 'Ijzeren voorraad',
    '' as 'Ijzeren voorraad type',
    '' as 'Bevestigde vraag',
    round(resultaatdezemaand.TotaalVolume) as 'Afzet Deze Maand',
    '' as 'Levertijd Afwijking',
    artikel.omschrijving1 as 'Artikel Omschrijving',
	  0 as 'leverdag',
	  0 as 'volgende besteldag',
    '' as 'Service Level',
    round(dayofmonth(curdate()) / 7 + 0.5) as 'Run Code',
    partner.nummer as 'Leverancier Nr',
    partner.naam as 'Leverancier',
    (select 
            BestelHoeveelheid
        from
            besteleenheid
        where
            besteleenheid.ArtikelId = artikel.id
                and DatumVanaf <= curdate()
        order by datumvanaf desc
        limit 1) as 'MOQ',
    (select 
            BestelHoeveelheid
        from
            besteleenheid
        where
            besteleenheid.ArtikelId = artikel.id
                and DatumVanaf <= curdate()
        order by datumvanaf desc
        limit 1) as 'IOQ',
    '' as 'Economische Bestelhoeveelheid',
    winkelartikel.PrijsInkoop as 'Prijs',
    winkelartikel.PrijsAdviesIncBtw as 'Verkoop Prijs',
    afdeling.omschrijving as 'Analyse Code 1',
    ifnull(winkelartikel.facings,0) as 'Analyse Code 2',
    '100%' as 'Analyse Code 3',
    winkelartikel.BestelCodePartner as 'Analyse Code 4',
    (select 
            Barcode
        from
            barcode
        where
            barcode.artikelid = artikel.id
                and DatumVanaf <= curdate()
                and BestelEenheidId is null
        order by datumvanaf desc
        limit 1) as 'Analyse Code 5',
    if(product.Seizoensassortiment = 1,
        'Y',
        'N') as 'Analyse Code 6',
    1 as 'Prijs Per',
    if(artikel.Bestelbaar = 1, 'Y', 'N') as 'Voorraad Artikel',
    '' as 'Analyse Code 7',
    '' as 'Analyse Code 8',
    '' as 'Analyse Code 9',
    '' as 'Analyse Code 10',
    '' as 'Analyse Code 11',
    '' as 'Analyse Code 12',
    '' as 'Analyse Code 13',
    '' as 'Analyse Code 14',
    '' as 'Analyse Code 15',
    '' as 'Analyse Code 16',
    '' as 'Analyse Code 17',
    '' as 'Analyse Code 18',
    '' as 'Analyse Code 19',
    '' as 'Analyse Code 20',
    '' as 'Analyse Code 21',
    '' as 'Voorloper / Opvolger',
    '' as 'PLC datum',
    resultaat.totaalmarqtvolume as 'Orderregels vorige week',
    resultaat.totaalvolume as 'Afzet vorige week',
    '' as 'PLC percentage',
    '' as 'EOQ Logistieke eenheid 1',
    '' as 'EOQ Logistieke eenheid 2',
    '' as 'EOQ Logistieke eenheid 3',
    '' as 'EOQ Logistieke eenheid 4',
    '' as 'EOQ Logistieke eenheid 5'
from
    artikel
        inner join
    winkelartikel ON winkelartikel.artikelid = artikel.id
        inner join
    winkel ON winkelartikel.winkelid = winkel.id
        inner join
    winkelpartnerafdeling ON winkelartikel.WinkelPartnerAfdelingId = winkelpartnerafdeling.id
        inner join
    partner ON winkelpartnerafdeling.partnerid = partner.Id
        inner join
    afdeling ON winkelpartnerafdeling.afdelingid = afdeling.Id
        inner join
    product ON product.id = artikel.productid
        left join
    (select 
        winkelid, artikelid, sum(totaalvolume) as totaalvolume
    from
        resultaat
    where
        label = 'OPSW' and jaar = year(curdate())
            and periode = floor(week(curdate(), 3) / 4)
    group by winkelid , artikelid) resultaatdezemaand ON resultaatdezemaand.winkelid = winkelartikel.winkelid
        and resultaatdezemaand.artikelid = winkelartikel.artikelid
        left join
    resultaat on resultaat.winkelid = winkelartikel.winkelid and resultaat.artikelid = winkelartikel.ArtikelId and label = 'OPSW' and jaar = year(curdate()) and week = week(curdate(),3)
where
    artikel.statusid in (3 , 4, 6)