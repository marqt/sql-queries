select 
    statusartikel.omschrijving,
    statuswinkelartikel.omschrijving,
    count(*)
from
    artikel,
    winkelartikel,
    status statusartikel,
    status statuswinkelartikel
where
    winkelartikel.artikelid = artikel.id
        and artikel.statusid = statusartikel.id
        and winkelartikel.statusid = statuswinkelartikel.id
GROUP BY winkelartikel.statusid