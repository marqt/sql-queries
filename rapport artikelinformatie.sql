select 
    afdeling.id as 'afdelingid',
	afdeling.omschrijving as 'afdelingomschrijving',
    artikel.omschrijving1 as 'artikelomschrijving',
    artikel.artikelnummer as 'artikelnummer',
    partner.naam as 'partnernaam',
    (select 
            barcode
        from
            barcode
        where
            artikel.id = barcode.artikelid
                and datumvanaf <= curdate()
        order by datumvanaf desc
        limit 1) as 'barcode',
    (select 
            concat(format(bestelhoeveelheid, 0),
                        ' ',
                        meeteenheid.code)
        from
            besteleenheid,
            meeteenheid
        where
            artikel.id = besteleenheid.artikelid
                and bestelmeeteenheidid = meeteenheid.id
                and datumvanaf <= curdate()
        order by datumvanaf desc
        limit 1) as 'BE',
    concat(product.inhoud, ' ', meeteenheid.code) as 'inhoud',
    winkelartikel.prijsadviesincbtw as 'verkoopprijs',
    artikel.plucode as 'PLU'
from
    historie,
    artikel,
    winkelartikel,
    winkelpartnerafdeling,
    partner,
    product,
    meeteenheid,
    afdeling
where
    artikel.id = winkelartikel.artikelid
        and winkelartikel.winkelpartnerafdelingid = winkelpartnerafdeling.id
        and winkelpartnerafdeling.partnerid = partner.id
        and winkelpartnerafdeling.afdelingid = afdeling.id
        and artikel.productid = product.id
        and product.inhoudmeeteenheidid = meeteenheid.id
        and historie.recordid = artikel.id
        and historie.laatstewijziging > date_sub(curdate(), interval (1) week)
        and tabel = 'artikel'
        and veld = 'bestelbaar'
group by artikel.id