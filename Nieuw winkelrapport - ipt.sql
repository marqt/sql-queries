select 
    vorigeweek.ipt as 'IPT vorige week',
    (vorigeweek.ipt - gemiddelde3weken.ipt) / gemiddelde3weken.ipt as 't.o.v. trend',
    (vorigeweek.ipt - vorigjaar.ipt) / vorigjaar.ipt as 'vs VJ'
from
    (select 
        verkoop.winkelid, sum(verkoop.marqtvolume)/count(verkoop.id) as 'ipt'
    from
        verkoop
    where
        TransactieSoort = 'KAS' and jaar = 2014
            and week = week(date_sub(curdate(), interval 1 week), 3) group by verkoop.winkelid) vorigeweek,
(select 
        verkoop.winkelid, sum(verkoop.marqtvolume)/count(verkoop.id) as 'ipt'
    from
        verkoop
    where
        TransactieSoort = 'KAS' and jaar = 2014
            and week <= week(date_sub(curdate(), interval 1 week), 3) group by verkoop.winkelid) gemiddelde3weken,
(select 
        verkoop.winkelid, sum(verkoop.marqtvolume)/count(verkoop.id) as 'ipt'
    from
        verkoop
    where
        TransactieSoort = 'KAS' and jaar = 2013
            and week = week(date_sub(curdate(), interval 1 week), 3) group by verkoop.winkelid) vorigjaar
where vorigeweek.winkelid = gemiddelde3weken.winkelid
and vorigeweek.winkelid = vorigjaar.winkelid