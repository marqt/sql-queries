select 
    artikel.artikelnummer,
    artikel.Omschrijving1,
    historie.veld,
    historie.WaardeOud,
    historie.WaardeNieuw,
    historie.GebruikerOmschrijving
from
    historie,
    artikel
where
    historie.recordid = artikel.id
        and tabel = 'artikel'
        and (historie.veld = 'marketinginformatie1'
        or historie.veld = 'marketinginformatie2'
        or historie.veld = 'marketinginformatie3'
        or historie.veld = 'marketinginformatie4'
        or historie.veld = 'marketinginformatie5'
        or historie.veld = 'marketinginformatie6')
        and historie.LaatsteWijziging > date_sub(curdate(), interval (1) week)