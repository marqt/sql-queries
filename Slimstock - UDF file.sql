select 
    5 as 'Recordtype',
    year(curdate()) as 'Jaar',
    month(curdate()) as 'Maand',
    '' as 'leeg',
    concat(winkel.nummer, '_') as 'Warehouse code',
    artikel.artikelnummer as 'Artikel Code',
    concat(levergroep.id, ' - ', levergroep.naam) as 'Analyse Code 7',
    concat(partner.nummer, ' - ', partner.Naam) as 'Analyse Code 8',
    artikel.merk as 'Analyse Code 9',
    'merchandiser' as 'Analyse Code 11',
    if(product.GrondslagId = 1,
        'stuks',
        'wicht') as 'Analyse Code 12',
    product.Houdbaarheid as 'Analyse Code 13',
    'houdbaarheid_schap' as 'Analyse Code 14',
    'bestelbaar-vanaf' as 'Analyse Code 15',
    'bestelbaar-tot' as 'Analyse Code 16',
    if((select 
                orderschemaitem.besteldag
            from
                winkelpartnerafdeling,
                levergroep,
                orderschema,
                orderschemaitem
            where
                levergroep.id = ifnull(winkelartikel.LeverGroepId,
                        winkelpartnerafdeling.DefaultLeverGroepId)
                    and orderschema.LeverGroepId = levergroep.Id
                    and orderschemaitem.OrderSchemaId = orderschema.Id
                    and orderschema.Actief = 1
                    and ((orderschema.IngangsDatum <= curdate()
                    and orderschema.EindDatum >= curdate())
                    or (orderschema.IngangsDatum <= curdate()
                    and orderschema.EindDatum is null))
                    and besteldag = weekday(curdate()) + 2
            order by BestelDag asc
            limit 0 , 1) is null,
        'Nee',
        'Ja') as 'Analyse Code 16',
    'colli laag' as 'Analyse Code 17',
    'pallet laag' as 'Analyse Code 18',
    levergroep.stroom as 'Analyse Code 19',
    if(artikel.statusid in (3 , 4),
        'actueel',
        if(artikel.statusid = 6,
            'niet in winkel',
            'gesaneerd')) as 'Analyse Code 20',
    '' as 'Analyse Code 21',
    concat(cg1.nummer, '-', cg1.omschrijving) as 'Analyse Code 22',
    concat(cg2.nummer, '-', cg2.omschrijving) as 'Analyse Code 23',
    concat(cg3.nummer, '-', cg3.omschrijving) as 'Analyse Code 24',
    concat(og1.nummer, '-', og1.omschrijving) as 'Analyse Code 25',
    concat(og2.nummer, '-', og2.omschrijving) as 'Analyse Code 26',
    concat(og3.nummer, '-', og3.omschrijving) as 'Analyse Code 27'
from
    artikel,
    product,
    winkel,
    winkelartikel,
    winkelpartnerafdeling,
    levergroep,
    orderschema,
    partner,
    commercielegroep cg1,
    commercielegroep cg2,
    commercielegroep cg3,
    operationelegroep og1,
    operationelegroep og2,
    operationelegroep og3
where
    winkelartikel.artikelid = artikel.id
        and winkelartikel.winkelid = winkel.id
        and artikel.productid = product.id
        and winkelartikel.winkelpartnerafdelingId = winkelpartnerafdeling.Id
        and levergroep.id = ifnull(winkelartikel.LeverGroepId,
            winkelpartnerafdeling.DefaultLeverGroepId)
        and orderschema.LeverGroepId = levergroep.Id
        and winkelpartnerafdeling.PartnerId = partner.Id
        and product.CommercieleGroepId = cg3.id
        and cg3.commercielegroepParentId = cg2.id
        and cg2.commercielegroepParentId = cg1.id
        and product.OperationeleGroepId = og3.id
        and og3.OperationelegroepParentId = og2.id
        and og2.OperationelegroepParentId = og1.id
        and artikel.statusid in (3 , 4)