#
# Cyclecount Today voor telling maandag tm vrijdag
# De idee is dat alle artikelen onder today 1 x geteld worden in de week.
# Parameters defineren voor testen. Voor live in BIRT @Winkelid vervangen door ?
#
Set @WinkelID=2;

#
# Artikelen Today 
(select 
    og3.omschrijving,
    og2.Omschrijving,
    og1.Omschrijving,
    winkelartikel.schapnummer,
    winkelartikel.planknummer,
    winkelartikel.positie,
    artikel.Omschrijving1,
    artikel.merk,
    winkelartikelvoorraadstand.voorraadstand,
    product.inhoud,
    meeteenheid.code,
    (select 
            lpad(barcode, 13, 0)
        from
            barcode
        where
        	artikel.id = barcode.artikelid
            and datumvanaf <= curdate()
                and besteleenheidid is null
        order by datumvanaf desc
        limit 1) as cebarcode
from
    artikel,
    winkelartikel,
    winkelartikelvoorraadstand,
    product,
    meeteenheid,
    operationelegroep og1,
    operationelegroep og2,
    operationelegroep og3
where
    artikel.id = winkelartikel.ArtikelId
        and winkelartikel.id = winkelartikelvoorraadstand.winkelartikelId
        and artikel.ProductId = product.id
        and product.inhoudmeeteenheidid = meeteenheid.Id
        and product.OperationeleGroepId = og3.id
        and og3.OperationeleGroepParentId = og2.id
        and og2.OperationeleGroepParentId = og1.id
        #artikelstatus is actueel of gewijzigd
        and artikel.StatusId in (3 , 4)
        and winkelartikel.bestelbaar = 1
        and winkelartikel.winkelid = @WinkelID
        #het artikel staat niet in een inkooporder waarop wordt geleverd voor deze winkel voor deze dag
		and not exists (select 
    		artikelid
		from
   			inkooporder,
   			inkooporderregel
		where
   			inkooporder.id = inkooporderregel.orderid
        and date(inkooporder.verwachteLeverDatumWinkelTijd) = curdate()
		and winkelid = @WinkelID
		and artikelid = artikel.id)
        #het veld inassortiment ligt niet in de afgelopen 14 dagen of is niet ingevuld
		and (inAssortiment < date_sub(curdate(), interval 14 day)
        or inAssortiment is null)
        and
			#iedere dag doordeweeks een andere operationele groep
			CASE
				#maandag
				WHEN dayofweek(curdate()) = 2
					THEN og3.id in (455,456,459)
				#dinsdag
				WHEN dayofweek(curdate()) = 3
					THEN og3.id in (465,466,467)
				#woensdag
				WHEN dayofweek(curdate()) = 4
					THEN og3.id in (460,458,457)
				#donderdag
				WHEN dayofweek(curdate()) = 5
					THEN og3.id in (468,469,471)
				#vrijdag
				WHEN dayofweek(curdate()) = 6
                    THEN og3.id in (472,473,464)
				ELSE 1=1
			END 
					
        
        
        
        
        
        
        
      
        
        )