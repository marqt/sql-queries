select 
winkelid,    
artikel.artikelnummer,
    winkelartikel.schapnummer,
    winkelartikel.planknummer,
    winkelartikel.positie,
    winkelartikel.facings
from
    winkelartikel,
    artikel
where
    winkelartikel.artikelid = artikel.id
        and winkelid = 2
        and schapnummer = 55