SELECT 
	jaar2013min4week.omschrijving,
    jaar2013min4week.bm,
    jaar2013min3week.bm,
    jaar2013min2week.bm,
    jaar2013min1week.bm
FROM
    (SELECT 
        com2.omschrijving,
            (sum(totaalbedrag) - sum(totaalbtw) - sum(totaalmarqtwaarde)) / (sum(totaalbedrag) - sum(totaalbtw)) AS 'BM'
    FROM
        resultaat, artikel, product, winkel, commercielegroep com3, commercielegroep com2
    WHERE
        resultaat.artikelid = artikel.id
            AND product.id = artikel.productid
            AND winkel.id = resultaat.winkelid
            AND com3.id = product.commercieleGroepId
            AND com2.id = com3.commercieleGroepParentId
            AND resultaat.jaar = YEAR(CURDATE())
            AND resultaat.week = WEEK(CURDATE(), 3) - 4
            AND resultaat.label = 'OPSW'
    GROUP BY com2.id) jaar2013min4week,
    (SELECT 
        com2.omschrijving,
            (sum(totaalbedrag) - sum(totaalbtw) - sum(totaalmarqtwaarde)) / (sum(totaalbedrag) - sum(totaalbtw)) AS 'BM'
    FROM
        resultaat, artikel, product, winkel, commercielegroep com3, commercielegroep com2
    WHERE
        resultaat.artikelid = artikel.id
            AND product.id = artikel.productid
            AND winkel.id = resultaat.winkelid
            AND com3.id = product.commercieleGroepId
            AND com2.id = com3.commercieleGroepParentId
            AND resultaat.jaar = YEAR(CURDATE())
            AND resultaat.week = WEEK(CURDATE(), 3) - 3
            AND resultaat.label = 'OPSW'
    GROUP BY com2.id) jaar2013min3week,
    (SELECT 
        com2.omschrijving,
            (sum(totaalbedrag) - sum(totaalbtw) - sum(totaalmarqtwaarde)) / (sum(totaalbedrag) - sum(totaalbtw)) AS 'BM'
    FROM
        resultaat, artikel, product, winkel, commercielegroep com3, commercielegroep com2
    WHERE
        resultaat.artikelid = artikel.id
            AND product.id = artikel.productid
            AND winkel.id = resultaat.winkelid
            AND com3.id = product.commercieleGroepId
            AND com2.id = com3.commercieleGroepParentId
            AND resultaat.jaar = YEAR(CURDATE())
            AND resultaat.week = WEEK(CURDATE(), 3) - 2
            AND resultaat.label = 'OPSW'
    GROUP BY com2.id) jaar2013min2week,
    (SELECT 
        com2.omschrijving,
            (sum(totaalbedrag) - sum(totaalbtw) - sum(totaalmarqtwaarde)) / (sum(totaalbedrag) - sum(totaalbtw)) AS 'BM'
    FROM
        resultaat, artikel, product, winkel, commercielegroep com3, commercielegroep com2
    WHERE
        resultaat.artikelid = artikel.id
            AND product.id = artikel.productid
            AND winkel.id = resultaat.winkelid
            AND com3.id = product.commercieleGroepId
            AND com2.id = com3.commercieleGroepParentId
            AND resultaat.jaar = YEAR(CURDATE())
            AND resultaat.week = WEEK(CURDATE(), 3) - 1
            AND resultaat.label = 'OPSW'
    GROUP BY com2.id) jaar2013min1week
WHERE
    jaar2013min1week.omschrijving = jaar2013min4week.omschrijving
        AND jaar2013min1week.omschrijving = jaar2013min3week.omschrijving
        AND jaar2013min1week.omschrijving = jaar2013min2week.omschrijving