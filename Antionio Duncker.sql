select 
    winkel.nummer,
    inkooporder.orderkenmerk,
    inkooporder.orderstatus,
    goederenontvangstdatumtijd,
    inkooporder.statusdatumtijd,
    partner.naam as 'partnernaam',
    levergroep.naam as 'leverschema',
    levergroeppartner.naam as 'partner van leverschema',
	sum(MWbesteld) as 'MW besteld',
	sum(MWgeleverd) as 'MW geleverd'
from
    inkooporder,
	inkooporderregel,
    partner,
    winkel,
    levergroep,
    partner levergroeppartner
where
    inkooporder.partnerid = partner.id
        and inkooporder.winkelid = winkel.id
        and inkooporder.levergroepid = levergroep.id
        and levergroep.partnerid = levergroeppartner.id
		and inkooporder.id = inkooporderregel.orderid
        and statusdatumtijd between '2013-01-01' and '2013-12-31'
group by inkooporder.id