select 
    artikel.omschrijving1,
    date_format(historie.laatstewijziging,'%d-%m-%Y'),
    format(historie.waardeoud, 2, 'NL_nl'),
    format(historie.waardenieuw, 2, 'NL_nl')
from
    partner,
    winkelpartnerafdeling,
    winkelartikel,
    artikel,
    historie
where
    partner.id = winkelpartnerafdeling.partnerid
        and winkelpartnerafdeling.id = winkelartikel.winkelpartnerafdelingid
        and winkelartikel.artikelid = artikel.id
        and winkelartikel.id = historie.recordid
        and historie.tabel = 'winkelartikel'
        and historie.veld = 'prijsadviesincbtw'
        and historie.laatstewijziging > date_sub(curdate(), interval 14 month)
        and partner.nummer = 121
        and artikel.actief = 1
        and artikel.artikelnummer = 140331
group by historie.waardeoud