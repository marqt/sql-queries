select 
    artikel.artikelnummer as 'Article Code',
   promotieperiode.startdatum,
 promotieperiode.einddatum
from
    promotie,
    promotieperiode,
    promotiestatus,
    promotieartikel,
    artikel
where
    promotie.promotieperiodeid = promotieperiode.Id
        and promotie.promotieStatusId = promotiestatus.Id
        and promotieartikel.promotieId = promotie.Id
        and promotieartikel.artikelId = artikel.Id
        and promotieperiode.startdatum >= date_sub(NOW(), interval 14 day)
        and promotieperiode.einddatum <= date_sub(NOW(), interval -7 day)