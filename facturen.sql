select 
    orderkenmerk,
    partner.naam,
    date(statusdatumtijd) as besteltijd,
    date(inkooporder.goederenontvangstdatumtijd) as ontvangstdatum,
    date(inkoopfactuur.datum) as factuurdatum,
    artikel.ArtikelNummer,
    artikel.omschrijving1,
    inkooporderregel.CEbesteld,
    inkooporderregel.MWbesteld,
    inkooporderregel.CEgeleverd,
    inkooporderregel.MWgeleverd,
    inkoopfactuurregel.invoicedAmount,
    if(partner.BEVerplicht = 1,
        invoicedAmount * besteleenheid.BestelHoeveelheid,
        invoicedAmount) as CEgefactureerd,
    inkoopfactuurregel.netAmount as MWgefactureerd
from
    inkooporder,
    inkooporderregel,
    inkoopfactuur,
    inkoopfactuurregel,
    artikel,
    partner,
    winkelartikelexporthistorie,
    besteleenheid
where
    inkooporder.id = inkooporderregel.orderid
        and inkoopfactuur.id = inkoopfactuurregel.inkoopFactuurId
        and inkooporder.id = inkoopfactuur.inkooporderId
        and inkooporderregel.artikelId = inkoopfactuurregel.artikelId
        and artikel.id = inkooporderregel.artikelid
        and inkooporder.partnerId = partner.id
        and inkooporderregel.winkelartikelExportHistorieId = winkelartikelexporthistorie.id
        and winkelartikelexporthistorie.bestelEenheidId = besteleenheid.id
        and CEgeleverd != if(partner.BEVerplicht = 1,
        invoicedAmount * besteleenheid.BestelHoeveelheid,
        invoicedAmount)
        and statusdatumtijd between '2016-01-01' and '2016-01-10'