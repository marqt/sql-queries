select 
    vorigeweek.omzet as 'Omzet vorige week',
    (vorigeweek.omzet - gemiddelde3weken.omzet) / gemiddelde3weken.omzet as 't.o.v. trend',
    (vorigeweek.omzet - vorigjaar.omzet) / vorigjaar.omzet as 'vs VJ'
from
    (select 
        winkelid, sum(totaalbedrag) as 'Omzet'
    from
        resultaat
    where
        jaar = 2014
            and week = week(date_sub(curdate(), interval 1 week), 3)
            and label = 'OPSW'
    group by winkelid) vorigeweek,
    (select 
        winkelid, sum(totaalbedrag) / 3 as 'Omzet'
    from
        resultaat
    where
        jaar = 2014
            and week < week(curdate(), 3)
            and label = 'OPSW'
    group by winkelid) gemiddelde3weken,
    (select 
        winkelid, sum(totaalbedrag) as 'Omzet'
    from
        resultaat
    where
        jaar = 2013
            and week = week(date_sub(curdate(), interval 1 week), 3)
            and label = 'OPSW'
    group by winkelid) vorigjaar
where
    vorigeweek.winkelid = gemiddelde3weken.winkelid
        and vorigeweek.winkelid = vorigjaar.winkelid