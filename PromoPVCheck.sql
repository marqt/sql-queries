#
# Promoaantallen check. Controleert bestelling of de Promo PV (buffer) groot genoeg is
# Auteur Bob. iov Isabe/Theo
#
SELECT 
    artikel.artikelnummer,
    artikel.Omschrijving1,
    winkel.Naam,
    besteladviesregel.voorraad as 'Voorraad A',
    besteladviesregel.onderweg as 'Onderweg B',
    besteladviesregel.forecastCE as 'Forcecast C',
    besteladviesregel.adviesCE as 'Advies D',
    besteladviesregel.buffer as 'Buffer E',
    #Som van theo (zie JIRA: REP-32)
    (besteladviesregel.voorraad + besteladviesregel.onderweg - besteladviesregel.forecastCE + besteladviesregel.adviesCE) AS 'A+B-C+D<E'
    
FROM
    besteladviesregel,
    winkelartikel,
    artikel,
    winkel,
    product,
    operationelegroep og3,
    operationelegroep og2,
    operationelegroep og1,
    besteladvies,
    besteladviesfile
WHERE
    besteladviesfile.id = besteladvies.besteladviesfileid
        AND besteladviesregel.besteladviesid = besteladvies.id
        AND besteladvies.statusId < 90
        AND DATE(besteladviesfile.datum) = DATE(NOW())
        AND winkelartikel.id = besteladviesregel.winkelartikelid
        AND winkelartikel.ArtikelId = artikel.id
        AND winkel.id = winkelartikel.winkelid
        AND product.id = artikel.productid
        AND og3.id = product.OperationeleGroepId
        AND og2.id = og3.OperationeleGroepParentId
        AND og1.id = og2.OperationeleGroepParentId
        AND besteladviesregel.promotie = 1
        AND (besteladviesregel.voorraad + besteladviesregel.onderweg - besteladviesregel.forecastCE + besteladviesregel.adviesCE) < besteladviesregel.buffer
        AND og2.id IN (417 , 418, 419)