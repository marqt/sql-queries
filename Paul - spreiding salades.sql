select 
    count((verkoopregel.id)),
    IF(artikel.artikelnummer in (118402 , 154875, 158325, 470120, 470139),
        'Maass',
        'Sandays') as leverancier,
    IF(verkoop.starttijd > '15:00:00',
        'na 15:00',
        'tot 15:00') as scheiding
from
    verkoop,
    verkoopregel,
    artikel
where
    verkoop.id = verkoopregel.verkoopid
        and verkoopregel.artikelid = artikel.id
       and artikel.artikelnummer in (118402 , 154875,
        158325,
       470120,
       470139,
       159384,
       159409,
       159418,
      160708,
      160717)
        and verkoop.jaar = 2013
        and verkoop.week = 52
        and verkoopregel.rubriekid = 1
group by leverancier , scheiding
