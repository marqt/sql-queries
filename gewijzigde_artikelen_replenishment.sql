SELECT 
    Omschrijving1 AS Artikelomschrijving,
    ArtikelNummer,
    og1.Omschrijving AS Pilaar,
    historie.veld,
    historie.WaardeOud,
    historie.WaardeNieuw,
    historie.GebruikerOmschrijving,
    historie.LaatsteWijziging
    
    
    
FROM
    central.artikel
        INNER JOIN
    product ON product.id = artikel.ProductId
        INNER JOIN
    operationelegroep og3 ON og3.id = product.OperationeleGroepId
        INNER JOIN
    operationelegroep og2 ON og2.id = og3.OperationeleGroepParentId
        INNER JOIN
    operationelegroep og1 ON og1.id = og2.OperationeleGroepParentId
        JOIN
    winkelartikel ON artikel.id = winkelartikel.ArtikelId
		 JOIN
	historie on historie.recordid=artikel.id
    
    
WHERE
    
	historie.Tabel='Winkelartikel' and (historie.veld='schapinhoudaangepast' or historie.veld='schapinhoud')
    and DATE(historie.laatstewijziging) > DATE(DATE_SUB(CURDATE(), INTERVAL 7 DAY))
    and og1.Id=405

group by ArtikelNummer
     
Order by historie.LaatsteWijziging desc