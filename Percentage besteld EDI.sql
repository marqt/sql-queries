select 
    count(distinct(inkooporder.id)), count(inkooporderregel.id), sum(inkooporderregel.MWbesteld)
from
    inkooporder,
    inkooporderregel,
    partner
where
    inkooporder.statusdatumtijd >= DATE_SUB(DATE_ADD(MAKEDATE(YEAR(CURDATE()), 1),
            INTERVAL WEEK(CURDATE()) WEEK),
        INTERVAL WEEKDAY(DATE_ADD(MAKEDATE(YEAR(CURDATE()), 1),
                    INTERVAL WEEK(CURDATE()) WEEK)) DAY)
        AND inkooporder.statusdatumtijd < DATE_SUB(DATE_ADD(MAKEDATE(YEAR(CURDATE()), 1),
            INTERVAL WEEK(CURDATE()) + 1 WEEK),
        INTERVAL WEEKDAY(DATE_ADD(MAKEDATE(YEAR(CURDATE()), 1),
                    INTERVAL WEEK(CURDATE()) WEEK)) DAY)
        AND inkooporder.partnerid = partner.id
        AND inkooporder.id = inkooporderregel.orderid
        AND partner.OrderEDIFACT = 1
UNION
select 
    count(distinct(inkooporder.id)), count(inkooporderregel.id), sum(inkooporderregel.MWbesteld)
from
    inkooporder,
    inkooporderregel,
    partner
where
    inkooporder.statusdatumtijd >= DATE_SUB(DATE_ADD(MAKEDATE(YEAR(CURDATE()), 1),
            INTERVAL WEEK(CURDATE()) WEEK),
        INTERVAL WEEKDAY(DATE_ADD(MAKEDATE(YEAR(CURDATE()), 1),
                    INTERVAL WEEK(CURDATE()) WEEK)) DAY)
        AND inkooporder.statusdatumtijd < DATE_SUB(DATE_ADD(MAKEDATE(YEAR(CURDATE()), 1),
            INTERVAL WEEK(CURDATE()) + 1 WEEK),
        INTERVAL WEEKDAY(DATE_ADD(MAKEDATE(YEAR(CURDATE()), 1),
                    INTERVAL WEEK(CURDATE()) WEEK)) DAY)
        AND inkooporder.partnerid = partner.id
        AND inkooporder.id = inkooporderregel.orderid