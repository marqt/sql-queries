SELECT 
    verkopen.winkelnummer, verkopen.uur, avg(verkopen.aantallen)
FROM
    (select 
        winkel.nummer AS 'winkelnummer',
            verkoop.uur,
            count(verkoopregel.hoeveelheid) AS 'aantallen'
    from
        verkoop, verkoopregel, winkel
    where
        verkoop.id = verkoopregel.verkoopid
            and winkel.id = verkoop.winkelid
            and verkoop.jaar = 2013
            and verkoop.week between 41 and 44
            and verkoop.weekdag = 7
            and winkel.nummer = 159
            and (verkoopregel.commercielegroepid = 12731
            OR verkoopregel.artikelid in (7892 , 7895, 7899, 7900))
    group by verkoop.week , verkoop.uur) verkopen
group by verkopen.uur

