select 
    artikel.artikelnummer,
    artikel.omschrijving1 as artikelomschrijving,
    sum(totaalbedrag) as omzet_vw
from
    artikel,
    resultaat
where
    artikel.id = resultaat.artikelid
        and resultaat.label = 'OPSW'
        and resultaat.week = week(date_sub(curdate(), interval 1 week),
        3)
        and resultaat.jaar = year(date_sub(curdate(), interval 1 week))
        and artikel.verkoopvariant = 1
        and not exists( select 
            *
        from
            verhoudingartikelnaarartikel
        where
            artikel.id = verhoudingartikelnaarartikel.VerhoudingArtikelId)
group by artikel.id
order by omzet_vw desc