-------------------------------------------------------------------------------
-- Author       Bob Hentenaar
-- Created      08/07/2017
-- Purpose      Query om een check te op marqtwaarde (inkoopprijs) besteladviezen
--              
--              
-- Copyright © 2017, Marqt B.V., All Rights Reserved
-------------------------------------------------------------------------------
-- Modification History
--
-- 05/07/0000  developer full name  
--      A comprehensive description of the changes. The description may use as 
--      many lines as needed.
-------------------------------------------------------------------------------

SELECT 
    artikelid,
    artikelnummer,
    omschrijving1,
    winkel.afkorting,
    adviesce,
    ROUND(besteladviesregel.adviesCE * winkelartikel.PrijsInkoop,
            2) AS marqtwaardeadvies
FROM
    besteladviesregel,
    besteladvies,
    besteladviesfile,
    winkelartikel,
    artikel,
    winkel,
	product,
    operationelegroep og1,
    operationelegroep og2,
    operationelegroep og3
WHERE
			besteladviesfile.id = besteladvies.besteladviesfileid
        AND besteladviesregel.besteladviesid = besteladvies.id
        AND besteladvies.statusId < 90
        AND DATE(besteladviesfile.datum) = DATE(NOW())
        AND winkelartikel.id = winkelartikelid
        AND besteladviesfile.winkelid = winkelartikel.winkelid
        AND winkelartikel.artikelid = artikel.id
		AND product.id = artikel.productid
        AND og3.id = product.OperationeleGroepId
        AND og2.id = og3.OperationeleGroepParentId
        AND og1.id = og2.OperationeleGroepParentId
        AND og1.id = 405
        AND besteladviesregel.adviesCE * winkelartikel.PrijsInkoop > 200
        AND winkel.id = winkelartikel.winkelid
        AND winkel.id <> 4
        
        group by ArtikelId, winkel.id, adviesce