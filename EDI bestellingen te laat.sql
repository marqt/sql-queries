SELECT 
winkel.nummer AS 'Winkelnummer',
    partner.naam AS 'Partnernaam',
    inkooporder.orderkenmerk AS 'Ordernummer',
    inkooporder.levergroepid AS 'Levergroepid',
    inkooporder.statusdatumtijd AS 'Besteltijd',
    orderschemaitem.besteltijd 'Besteltijd volgens orderschema',
    TIMEDIFF(DATE_FORMAT(inkooporder.statusdatumtijd, '%H:%i:%s'),
            orderschemaitem.besteltijd) AS 'Verschil'
FROM
    inkooporder,
    winkel,
    partner,
    orderschema,
    orderschemaitem
WHERE
    inkooporder.winkelid = winkel.id
        AND inkooporder.partnerid = partner.id
        AND inkooporder.levergroepid = orderschema.levergroepid
        AND orderschema.id = orderschemaitem.orderschemaid
        AND inkooporder.statusdatumtijd > DATE_SUB(CURDATE(), interval 0 day)
        AND inkooporder.statusdatumtijd < DATE_ADD(CURDATE(), interval 1 day)
        AND partner.OrderEDIFACT = 1
        AND orderschema.actief = 1
        AND orderschema.ingangsdatum <= curdate()
        AND orderschemaitem.besteldag = weekday(curdate())+2
        AND TIMEDIFF(DATE_FORMAT(inkooporder.statusdatumtijd, '%H:%i:%s'),
            orderschemaitem.besteltijd) > 0
ORDER BY verschil DESC, inkooporder.statusdatumtijd ASC