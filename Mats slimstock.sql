SELECT 
    og1.omschrijving as pilaar,
    og2.omschrijving as productgroep,
    og3.omschrijving as productcluster,
    artikel.artikelnummer AS 'Artikelnummer',
    artikel.omschrijving1 AS 'Artikelomschrijving',
    status.omschrijving as status,
    if(artikel.bestelbaar = 1, 'Ja', 'Nee') as bestelbaar,
    artikel.verkoopvariant,
    artikel.inkoopvariant,
    verhoudingartikelnaarartikel.AantalNaar,
    artikelvan.ArtikelNummer as inkoopartikelnummer,
    artikelvan.Omschrijving1 as inkoopartikelomschrijving,
	product.Houdbaarheid
FROM
    artikel
        inner join
    product ON product.id = artikel.productid
        inner join
    operationelegroep as og3 ON og3.id = product.operationeleGroepId
        inner join
    operationelegroep as og2 ON og2.id = og3.operationeleGroepParentId
        inner join
    operationelegroep as og1 ON og1.id = og2.operationeleGroepParentId
        inner join
    status ON status.id = artikel.statusid
        left join
    verhoudingartikelnaarartikel ON verhoudingartikelnaarartikel.VerhoudingArtikelId = artikel.Id
        left join
    artikel artikelvan ON artikelvan.id = verhoudingartikelnaarartikel.ArtikelNaarId
where
    og1.id = 404