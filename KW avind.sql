select 
    cg3.omschrijving AS 'Commercielegroep',
    artikel.artikelnummer AS 'Artikelnummer',
    artikel.omschrijving1 AS 'Artikelomschrijving',
    artikel.merk AS 'Merk',
    winkelartikel.PrijsAdviesIncBtw AS 'Verkoopprijs',
    winkelartikel.PrijsInkoop AS 'Inkoopprijs',
    winkelartikel.marge AS 'Marge',
    sum(resultaat.totaalmarqtvolume)/4 AS 'Totaal'
from
    resultaat,
    commercielegroep cg2,
    commercielegroep cg3,
    product,
    artikel,
    winkelartikel
where
    resultaat.artikelid = artikel.id
        and product.id = artikel.productid
        and cg3.Omschrijving = 'Droge kruiden'
        and cg3.id = product.commercielegroepid
        and cg2.id = cg3.commercielegroepparentid
        and artikel.id = winkelartikel.artikelid
        and resultaat.winkelid = winkelartikel.winkelid
        and artikel.actief = 1
        and resultaat.jaar = year(curdate())
        and resultaat.week between week(curdate(),3)-4 and week(curdate(),3)-1
        and resultaat.label = 'OPSW'
group by resultaat.artikelid