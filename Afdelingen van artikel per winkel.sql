select 
    artikelnummer, afdeling.omschrijving
from
    artikel,
    winkelartikel,
    winkelpartnerafdeling,
    afdeling
where
    artikel.id = winkelartikel.artikelid
        and winkelartikel.winkelpartnerafdelingid = winkelpartnerafdeling.id
        AND winkelpartnerafdeling.afdelingid = afdeling.id
        and artikel.artikelnummer in (148465,148456,148438,148429,156680,156699,156705,156714)