SELECT 
    partner.Naam,
    partner.Nummer,
    CONCAT(winkel.NaamKort,
            ' (',
            winkel.Nummer,
            ')') winkelNaam,
    verkoop.Datum datum,
    inkoopOrderNummer.orderkenmerk AS `inkoopordernummer`,
    winkelartikel.BestelCodePartner,
    artikel.Omschrijving1,
    verkoopregel.Hoeveelheid,
    verkoopregel.Prijs,
    verkoopregel.BedragTotaal,
    TRUNCATE((verkoopregel.BedragInkoop * verkoopregel.Hoeveelheid),
        2) AS BedragInkoop,
    inkoopOrderNummer.*,
    verkoopregel.id,
    verkoop.id,
    grondslag.Omschrijving
FROM
    verkoop
        INNER JOIN
    verkoopregel ON verkoopregel.VerkoopId = verkoop.Id
        INNER JOIN
    partner ON partner.id = verkoopregel.PartnerId
        AND partner.Nummer = 3
        INNER JOIN
    winkelartikel ON winkelartikel.id = verkoopregel.WinkelArtikelId
        INNER JOIN
    winkel ON winkel.Id = verkoop.WinkelId
        INNER JOIN
    artikel ON artikel.id = verkoopregel.ArtikelId
        INNER JOIN
    product ON artikel.ProductId = product.Id
        INNER JOIN
    grondslag ON grondslag.id = product.GrondslagId
        INNER JOIN
    rubriek ON rubriek.id = verkoopregel.RubriekId
        AND rubriek.Nummer = 3400
        LEFT JOIN
    (SELECT 
        inkooporder.winkelId,
            inkooporder.partnerId,
            GROUP_CONCAT(DISTINCT inkooporder.orderkenmerk
                ORDER BY inkooporder.orderkenmerk DESC
                SEPARATOR ' / ') orderkenmerk,
            DATE(inkooporder.goederenontvangstdatumtijd) AS `goederenontvangstdatum`,
            inkooporderregel.winkelArtikelId
    FROM
        inkooporder
    INNER JOIN inkooporderregel ON inkooporderregel.orderId = inkooporder.id
    INNER JOIN partner ON partner.id = inkooporder.PartnerId
        AND partner.Nummer = 3
    WHERE
        inkooporder.goederenontvangstdatumtijd >= STR_TO_DATE(CONCAT(YEAR(DATE_ADD(NOW(), INTERVAL - 7 DAY)), WEEK(DATE_ADD(NOW(), INTERVAL - 7 DAY)), ' Monday'), '%X%V %W')
    GROUP BY inkooporder.winkelId , inkooporder.partnerId , DATE(inkooporder.goederenontvangstdatumtijd) , inkooporderregel.winkelArtikelId) inkoopOrderNummer ON inkoopOrderNummer.winkelArtikelId = verkoopregel.WinkelArtikelId
        AND inkoopOrderNummer.`goederenontvangstdatum` = verkoop.Datum
WHERE
    verkoop.jaar = YEAR(DATE_ADD(NOW(), INTERVAL - 7 DAY))
        AND week = WEEK(DATE_ADD(NOW(), INTERVAL - 7 DAY))
        AND inkoopOrderNummer.orderkenmerk IS NOT NULL
ORDER BY verkoop.Id DESC
