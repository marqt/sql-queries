select 
    inkooporder.id,
    inkooporder.orderkenmerk,
    count(distinct (inkooporderhistorie.id)) as herakkordeerd,
    count(distinct (inkooporderregel.artikelid)) as aantalorderregels
from
    inkooporder,
    inkooporderhistorie,
    inkooporderregel
where
    inkooporder.id = inkooporderhistorie.inkooporderId
        and inkooporder.id = inkooporderregel.orderId
        and inkooporder.statusdatumtijd >= date_sub(curdate(), interval 1 week)
        and inkooporderhistorie.statusid = 9
group by inkooporder.orderkenmerk
having herakkordeerd > 1
