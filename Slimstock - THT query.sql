select 
    afdeling.Omschrijving,
    ArtikelNummer,
    omschrijving1,
    status.Omschrijving,
    partner.naam,
    product.Houdbaarheid,
    (select 
            max(LaatsteWijziging)
        from
            historie
        where
            historie.RecordId = artikel.id
                and tabel = 'artikel'
                and veld = 'bestelbaar'
                and waardenieuw = 'false')
from
    artikel,
    winkelartikel,
    winkelpartnerafdeling,
    afdeling,
    partner,
    status,
    product
where
    artikel.productid = product.id
        and artikel.statusid = status.id
        and artikel.id = winkelartikel.artikelid
        and winkelartikel.WinkelPartnerAfdelingId = winkelpartnerafdeling.Id
        and winkelpartnerafdeling.afdelingid = afdeling.id
        and winkelpartnerafdeling.partnerid = partner.id
        and afdeling.id = 1
        and artikel.statusid in (6)
group by artikel.id