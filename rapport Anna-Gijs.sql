select 
    winkel.nummer,
    winkel.afkorting,
    format(bestelwaarde_vt.mw,2,'nl_NL')  as 'voorraad vorige telling',
    format(bestelwaarde.mw,2,'nl_NL') as 'voorraad afgelopen telling',
    format(bestelwaarde.mw / resultaten.omzet * resultaten.dagen,
        1) as 'gem #dagen voorraad',
    format(resultaten.voorraadmutatie / resultaten.omzet * 100,
        1) as '% verschil tov omzet'
from
    winkel
        left join
    (select 
        inventaris.winkelid, sum(bestelwaarde) as mw
    from
        inventaris, inventarisregel, (select 
        winkelartikelid,
            max(inventaris.statusdatumtijd) as statusdatumtijd
    from
        inventaris, inventarisregel
    where
        inventaris.id = inventarisregel.inventarisid
            and date(inventaris.statusdatumtijd) = '2016-05-24'
            and type != 'CORRECTIETELLING'
    group by winkelartikelid) as mx
    where
        inventaris.id = inventarisregel.inventarisid
            and inventaris.statusdatumtijd = mx.statusdatumtijd
            and inventarisregel.winkelArtikelId = mx.winkelartikelid
            and inventarisregel.leverancierId = 2
    group by inventaris.winkelid) bestelwaarde_vt ON bestelwaarde_vt.winkelid = winkel.id
        left join
    (select 
        inventaris.winkelid, sum(bestelwaarde) as mw
    from
        inventaris, inventarisregel, (select 
        winkelartikelid,
            max(inventaris.statusdatumtijd) as statusdatumtijd
    from
        inventaris, inventarisregel
    where
        inventaris.id = inventarisregel.inventarisid
            and date(inventaris.statusdatumtijd) = '2016-06-21'
            and type != 'CORRECTIETELLING'
    group by winkelartikelid) as mx
    where
        inventaris.id = inventarisregel.inventarisid
            and inventaris.statusdatumtijd = mx.statusdatumtijd
            and inventarisregel.winkelArtikelId = mx.winkelartikelid
            and inventarisregel.leverancierId = 2
    group by inventaris.winkelid) bestelwaarde ON bestelwaarde.winkelid = winkel.id
        left join
    (select 
        winkelid,
            sum(if(rubriekid = 1, TotaalMarqtWaarde, 0)) as omzet,
            sum(if(rubriekid = 21, TotaalMarqtWaarde, 0)) as voorraadmutatie,
            count(distinct (datum)) as dagen
    from
        resultaat
    where
        resultaat.label = 'BASE'
            and resultaat.datum > '2016-05-24'
            and resultaat.datum <= '2016-06-21'
            and resultaat.partnerid = 2
    group by winkelid) resultaten ON resultaten.winkelid = winkel.id
where
    winkel.type = 'winkel'