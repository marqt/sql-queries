select 
    winkelartikel.bestelcodepartner,
    artikel.omschrijving1,
    artikel.inkoopvariant,
    artikel.verkoopvariant,
    artikel.actief
from
    winkelartikel,
    artikel
where
    artikel.id = winkelartikel.artikelid
        #and winkelartikel.bestelcodepartner LIKE 'vv%'
		#and winkelartikel.bestelcodepartner NOT LIKE 'vv-%'
and artikel.inkoopvariant = 1
and artikel.verkoopvariant = 1
group by winkelartikel.bestelcodepartner