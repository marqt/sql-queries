SELECT

inkooporder.id,
LeverGroepId,
date(inkooporder.statusdatumtijd),
dayname(date(inkooporder.statusdatumtijd)),
    count(inkooporderregel.winkelartikelid)
FROM
    inkooporder,
    inkooporderregel
WHERE
    inkooporder.id = inkooporderregel.orderId
        AND inkooporder.partnerId = 107
        AND statusid NOT IN (1 , 99)


group by 

LeverGroepId, date(inkooporder.statusdatumtijd)

order by inkooporder.id desc