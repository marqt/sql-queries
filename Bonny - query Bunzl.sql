select 
	winkel.nummer,
    inkooporder.orderkenmerk,
    artikel.artikelnummer,
    artikel.omschrijving1,
    inkooporderregel.CEbesteld,
    inkooporderregel.CEgeleverd,
    inkooporderregel.MWbesteld,
    inkooporderregel.MWgeleverd
from
    inkooporder,
    inkooporderregel,
    artikel,
winkel,
    partner
where
    inkooporder.id = inkooporderregel.orderId
        and inkooporderregel.ArtikelId = artikel.id
        and inkooporder.partnerId = partner.Id
and inkooporder.winkelid = winkel.id
        and inkooporder.statusdatumtijd >= '2014-01-01'
        and partner.nummer = 85