 select 
 
 v_bestellevermomenttim.levergroepid,
 levergroep.naam,
 orderschemaid,
 levergroep.orderForecastAuto,
 orderschema.naam,
 group_concat(orderschemaitemid),
 group_concat(besteldatumtijd),
 volgendeleverdatum,
 count(*) 
 
 from v_bestellevermomenttim
 join central.levergroep on levergroep.id=levergroepid
 join central.orderschema on orderschema.id=orderschemaid
 
 where bestelDatumTijd >= curdate()
 
 group by 
 v_bestellevermomenttim.levergroepid,volgendeleverdatum
 
 having count(*)>1 order by volgendeleverdatum;