SELECT 
    winkel.Afkorting,
    artikel.ArtikelNummer,
    artikel.Omschrijving1,
    winkelartikelid,
    voorraadstand,
    winkelartikel.Bestelbaar
FROM
    voorraadstand,
    winkelartikel,
    winkel,
    artikel
WHERE
    voorraadstand.winkelartikelId = winkelartikel.id
        AND winkel.id = winkelartikel.WinkelId
        AND winkelartikel.ArtikelId = artikel.Id
        AND voorraadstandDatum > '2017-09-01 00:00:00'
        AND wijzigingssoort = 'DAGSTART_BEREKENING'
        And winkelartikel.Bestelbaar=1
        and artikel.StatusId in (3,4)
        AND WINKEL.Type='Winkel'
        AND artikel.artikelTypeId  in (1,3)
        AND winkelartikelid =174358


        
HAVING voorraadstand <= 0

ORDER BY winkel.id