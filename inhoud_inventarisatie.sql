select 
			verkoop.datum as datum,
			verkoop.starttijd as tijd,
			winkel.nummer as winkelnummer,
			winkel.afkorting as winkel,
			artikel.artikelnummer,
			artikel.omschrijving1 as artikelomschrijving,
			rubriek.nummer as rubrieknummer,
			rubriek.omschrijving as rubriek,
			verkoopregel.hoeveelheid as Volume,
			format((verkoopregel.hoeveelheid * verkoopregel.BedragInkoop),2,'nl_NL') as MW,
			inventarisnummer
		from
			verkoop,
			verkoopregel,
			winkel,
			artikel,
			rubriek,
			partner,
			inventaris
		where
			verkoop.winkelid = winkel.id
				and verkoop.id = verkoopregel.verkoopid
				and artikelid = artikel.id
				and rubriek.id = rubriekid
				and partnerid = partner.id
				and verkoop.id = inventaris.verkoopid
                and inventarisnummer='LOS 17a3a3f5-8db8-435d-9daf-20aa8d823573.XML'
                and rubriek.Nummer=3400

		order by winkel.id