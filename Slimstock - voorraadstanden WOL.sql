select 
    verkoop.datum, artikel.Omschrijving1, verkoopregel.hoeveelheid
from
    verkoop,
    verkoopregel,
    artikel
where
    verkoop.id = verkoopregel.verkoopid
        and verkoopregel.ArtikelId = artikel.id
		and verkoop.id in (7851258,7760696,7735580,7727004,7699971,7675402)
order by verkoop.datum desc