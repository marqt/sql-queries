select 
    artikel.omschrijving1,
    barcode.barcode,
    PrijsAdviesIncBtw,
    inhoud,
    @barcodeaangepast:=left(barcode.barcode, 7) as barcodeaangepast,
    @ean1:=substring(@barcodeaangepast, 1, 1) as ean1,
    @ean2:=substring(@barcodeaangepast, 2, 1) as ean2,
    @ean3:=substring(@barcodeaangepast, 3, 1) as ean3,
    @ean4:=substring(@barcodeaangepast, 4, 1) as ean4,
    @ean5:=substring(@barcodeaangepast, 5, 1) as ean5,
    @ean6:=substring(@barcodeaangepast, 6, 1) as ean6,
    @ean7:=substring(@barcodeaangepast, 7, 1) as ean7,
    @prijs:=lpad(round(PrijsAdviesIncBtw * 100 * inhoud / 1000),
            4,
            0) as prijs,
    @ean9:=substring(@prijs, 1, 1) as prijsdigit1,
    @ean10:=substring(@prijs, 2, 1) as prijsdigit2,
    @ean11:=substring(@prijs, 3, 1) as prijsdigit3,
    @ean12:=substring(@prijs, 4, 1) as prijsdigit4,
    @weightedproduct_digit1:=mod(@ean9 * 2 - floor(@ean9 * 2 / 10),
        10) as weightedproduct_digit1,
    @weightedproduct_digit2:=mod(@ean10 * 2 - floor(@ean10 * 2 / 10),
        10) as weightedproduct_digit2,
    @weightedproduct_digit3:=mod(@ean11 * 3, 10) as weightedproduct_digit3,
    @weightedproduct_digit4:=mod(@ean12 * 5 - floor(@ean12 * 5 / 10),
        10) as weightedproduct_digit4,
    @ean8:=mod(@weightedproduct_digit1 + @weightedproduct_digit2 + @weightedproduct_digit3 + @weightedproduct_digit4,
        10) as ean8,
    @temp:=(@ean1 * 1 + @ean2 * 3 + @ean3 * 1 + @ean4 * 3 + @ean5 * 1 + @ean6 * 3 + @ean7 * 1 + @ean8 * 3 + @ean9 * 1 + @ean10 * 3 + @ean11 * 1 + @ean12 * 3) as temp,
    @ean13:=ceil(@temp / 10) * 10 - @temp as ean13,
    concat(@ean1,
            @ean2,
            @ean3,
            @ean4,
            @ean5,
            @ean6,
            @ean7,
            @ean8,
            @ean9,
            @ean10,
            @ean11,
            @ean12,
            @ean13) as barcodenormgewicht
from
    artikel,
    product,
    barcode,
    winkelartikel,
    winkelpartnerafdeling
where
    product.id = artikel.ProductId
        and artikel.id = winkelartikel.artikelid
        and winkelartikel.WinkelPartnerAfdelingId = winkelpartnerafdeling.id
        and artikel.id = barcode.ArtikelId
        and afdelingid = 15
        and artikel.statusid in (3 , 4)
        and barcode.barcode like '23%'
        and grondslagid = 2
group by artikel.id