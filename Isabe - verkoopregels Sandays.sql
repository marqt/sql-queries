select 
    winkel.nummer,
    winkel.naamkort,
    artikel.ArtikelNummer,
    artikel.Omschrijving1,
    verkoopregel.Hoeveelheid,
    verkoop.datum,
    verkoop.StartTijd
from
    verkoop,
    verkoopregel,
    artikel,
    partner,
    winkel
where
    verkoop.id = verkoopregel.VerkoopId
        and verkoop.WinkelId = winkel.id
        and verkoopregel.partnerid = partner.Id
        and verkoopregel.ArtikelId = artikel.id
        and partner.nummer = 104
        and verkoop.jaar = 2014
        and verkoop.week between 21 and 25
        and verkoopregel.RubriekId = 1