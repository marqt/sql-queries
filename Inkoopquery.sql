select 
    *
from
    bestelbareartikelen
        left join
    forecast ON bestelbareartikelen.artikelid = forecast.artikelid
        and forecast.datum in ('2014-03-20','2014-03-21')
        left join
    inkoopforecastdelen ON bestelbareartikelen.artikelid = inkoopforecastdelen.artikelid_naar_delen
        and inkoopforecastdelen.datum_delen = forecast.datum