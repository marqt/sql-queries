select 
    afdeling.omschrijving,
    winkelartikel.bestelcodepartner,
    artikel.omschrijving1,
    artikel.plucode,
    winkelartikelexporthistorie.prijsadviesincbtw
from
    winkelartikelexporthistorie,
    winkelartikel,
    winkelpartnerafdeling,
    afdeling,
    artikel
where
    winkelartikelexporthistorie.bestelcodepartner = winkelartikel.bestelcodepartner
        and winkelartikel.artikelid = artikel.id
        and winkelartikel.winkelpartnerafdelingid = winkelpartnerafdeling.id
        and winkelpartnerafdeling.afdelingid = afdeling.id
        and datumgeldigvanaf >= '2014-05-06'
        and artikel.plucode > 0
group by bestelcodepartner
order by afdeling.omschrijving
