select 
    concat(cg1.nummer,
            '.',
            cg2.nummer,
            '.',
            cg3.nummer,
            ' ',
            cg1.omschrijving,
            '-',
            cg2.omschrijving,
            '-',
            cg3.omschrijving),
    cg3.marketinginformatie1,
    cg3.marketinginformatie2,
    cg3.marketinginformatie3,
    cg3.marketinginformatie4,
    cg3.marketinginformatie5,
    cg3.marketinginformatie6
from
    commercielegroep cg1,
    commercielegroep cg2,
    commercielegroep cg3
where
cg2.id = cg3.CommercieleGroepParentId
        and cg1.id = cg2.CommercieleGroepParentId