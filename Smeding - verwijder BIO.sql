select 
    partner.naam, artikel.Omschrijving1, replace(lower(artikel.Omschrijving1),' bio','')
from
    artikel,
    winkelartikel,
    winkelpartnerafdeling,
    partner
where
    artikel.id = winkelartikel.ArtikelId
        and winkelartikel.WinkelPartnerAfdelingId = winkelpartnerafdeling.Id
        and winkelpartnerafdeling.PartnerId = partner.Id
        and partner.nummer = 3
        and artikel.Omschrijving1 like '%BIO%'
group by artikel.id