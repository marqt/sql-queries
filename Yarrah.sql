SELECT 
    winkel.naam,
	winkelartikel.bestelcodepartner,
    resultaat.datum,
    resultaat.jaar,
    resultaat.week,
    date_format(resultaat.datum,'%W'),
    artikel.omschrijving1,
    resultaat.totaalbedrag,
    resultaat.totaalbtw,
    resultaat.totaalmarqtvolume
FROM
    resultaat,
    winkel,
    artikel,
    partner,
    winkelartikel
WHERE
    resultaat.winkelid = winkel.id
        AND resultaat.artikelid = artikel.id
        AND resultaat.partnerid = partner.id
        AND resultaat.winkelid = winkelartikel.winkelid
        AND resultaat.artikelid = winkelartikel.artikelid
        AND resultaat.jaar = 2016
        AND resultaat.label = 'OPSD'
		AND artikel.merk = 'Yarrah'
ORDER BY winkel.nummer ASC , resultaat.week ASC , artikel.id ASC