#Vul hier het winkelnummer in
SET @WNR=166;

select 

a.artikelnummer as artikelnummer, 
bc.BarCode as BarCode, 
a.omschrijving1 as omschrijving1,
og3.nummer as nummer,
og3.omschrijving as omschrijving,
og2.nummer as nummer2, 
og2.omschrijving as omschrijving3,
wa.bestelbaar actueel,
' ' as ' ',
CONCAT(a.artikelnummer,a.omschrijving1) as omschrijving2

from artikel a

inner join product p on p.id=a.productId
inner join barcode bc on bc.artikelid = a.id
inner join meeteenheid m on m.id = p.inhoudmeeteenheidid
inner join winkelartikel wa on wa.artikelid = a.id
inner join winkel w on w.id = wa.winkelId
inner join grondslag g on p.grondslagid = g.id
inner join operationelegroep og on og.id = p.operationelegroepId
inner join operationelegroep as og3 on og3.id = p.operationeleGroepId
inner join operationelegroep as og2 on og2.id = og3.operationeleGroepParentId
inner join operationelegroep as og1 on og1.id = og2.operationeleGroepParentId
inner join winkelpartnerafdeling as wpa on wpa.id = wa.winkelpartnerafdelingid
inner join afdeling as afd on afd.id = wpa.afdelingId
inner join partner on wpa.partnerid = partner.id

where w.nummer = @WNR
and (afd.nummer in (0,1,2,3,4,5,6,8,9,10,11,14,15) or a.artikelnummer in (950235 , 950244, 122779, 178987))
and partner.actief = 1

group by bc.Barcode
order by a.artikelnummer