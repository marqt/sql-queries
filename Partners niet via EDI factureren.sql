select 
    partner.naam, count(inkooporder.id)
from
    inkooporder,
    partner
where
    inkooporder.statusdatumtijd >= DATE_SUB(DATE_ADD(MAKEDATE(YEAR(CURDATE()), 1),
            INTERVAL WEEK(CURDATE())-1 WEEK),
        INTERVAL WEEKDAY(DATE_ADD(MAKEDATE(YEAR(CURDATE()), 1),
                    INTERVAL WEEK(CURDATE()) WEEK)) - WEEKDAY(CURDATE()) + 1 DAY)
        AND inkooporder.statusdatumtijd <= DATE_SUB(DATE_ADD(MAKEDATE(YEAR(CURDATE()), 1),
            INTERVAL WEEK(CURDATE()) WEEK),
        INTERVAL WEEKDAY(DATE_ADD(MAKEDATE(YEAR(CURDATE()), 1),
                    INTERVAL WEEK(CURDATE()) WEEK)) - WEEKDAY(CURDATE()) + 2 DAY)
        AND inkooporder.partnerid = partner.id
        AND partner.nummer not in (201,104,131,3,16,78,48,36,56,136,215,19,175,159)
group by partner.naam
order by count(inkooporder.id) DESC