SELECT DISTINCT
    (artikel.artikelNummer),
    winkelartikel.bestelCodePartner,
    status.omschrijving,
    IF(winkelArtikel.bestelbaar,
        'bestelbaar',
        'onbestelbaar') AS Bestelbaar,
    IF(artikel.inkoopvariant, 'ink', '') AS Inkoopvariant,
    IF(artikel.verkoopvariant, 'vrk', '') AS Verkoopvariant,
    IF(artikel.bewerkt, 'bew', '') AS Bewerkt,
    artikel.omschrijving1 AS Artikelomschrijving,
    artikel.merk,
    partner.Nummer,
    partner.Naam,
    levergroep.naam AS levergroep,
    Grondslag.omschrijving,
    winkelartikel.PrijsInkoop,
    winkelartikel.PrijsAdviesIncBtw,
    product.inhoud,
    meeteenheid.code AS meeteenheid,
    (SELECT 
            bestelHoeveelheid
        FROM
            besteleenheid
        WHERE
            besteleenheid.artikelId = artikel.Id
                AND datumvanaf <= NOW()
        ORDER BY datumvanaf DESC , besteleenheid.id ASC
        LIMIT 1) AS bestelHoeveelheid,
    product.houdbaarheid,
    (SELECT 
            barcode
        FROM
            barcode
        WHERE
            barcode.artikelid = artikel.id
                AND besteleenheidid IS NULL
                AND datumvanaf <= NOW()
        ORDER BY datumvanaf DESC , barcode.id ASC
        LIMIT 1) AS barcode,
    (SELECT 
            GROUP_CONCAT(barcode.barcode)
        FROM
            barcode
        WHERE
            barcode.ArtikelId = artikel.Id
        ORDER BY barcode.Volgnummer ASC) AS Alle_barcodes,
    og1.omschrijving,
    og2.omschrijving,
    og3.omschrijving,
    CONCAT(og1.nummer,
            '.',
            og2.nummer,
            '.',
            og3.nummer,
            ' ',
            og1.omschrijving,
            '-',
            og2.omschrijving,
            '-',
            og3.omschrijving) AS 'Operationele Groep',
    artikel.breedte,
    artikel.lengte,
    artikel.hoogte,
    product.Ingredienten,
	btw.percentage,
    artikel.verkoopinformatie1,
    artikel.verkoopinformatie2,
    artikel.verkoopinformatie3,
    artikel.verkoopinformatie4,
    artikel.verkoopinformatie5,
    artikel.verkoopinformatie6,
	artikel.verkoopinformatie7,
    artikel.verkoopinformatie8,
    partnercommercieel.nummer AS 'Commercieel Partnernummer',
    partnercommercieel.naam AS 'Commerciele Partnernaam',
    levergroep.id AS 'Levergroepnummer'
FROM
    artikel,
    product,
    winkelartikel,
    winkelpartnerafdeling,
    partner,
    meeteenheid,
    grondslag,
    status,
    winkel,
    operationelegroep og3,
    operationelegroep og2,
    operationelegroep og1,
    levergroep,
	btw,
    partner as partnercommercieel
WHERE
    artikel.productid = product.id
        AND winkelartikel.artikelid = artikel.id
        AND winkelpartnerafdeling.id = winkelartikel.winkelpartnerafdelingid
        AND winkelpartnerafdeling.partnerid = partner.id
        AND winkelpartnerafdeling.winkelid = winkel.id
        AND meeteenheid.id = product.inhoudmeeteenheidid
        AND winkelartikel.statusid = status.id
        AND grondslag.id = product.grondslagId
		AND btw.id = product.btwid
        AND og3.id = product.OperationeleGroepId
        AND og2.id = og3.OperationeleGroepParentId
        AND og1.id = og2.OperationeleGroepParentId
        AND levergroep.id = IFNULL(winkelartikel.levergroepId,
            winkelpartnerafdeling.DefaultLeverGroepId)
		AND partnercommercieel.id=artikel.commercielePartnerId