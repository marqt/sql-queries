select 
    winkelartikel.bestelcodepartner,
	REPLACE (winkelartikel.bestelcodepartner, 'LV- ', ''),
	REPLACE (winkelartikel.bestelcodepartner, 'lv- ', ''),
	REPLACE (winkelartikel.bestelcodepartner, 'LV-', ''),
	REPLACE (winkelartikel.bestelcodepartner, 'lv-', ''),
	REPLACE (winkelartikel.bestelcodepartner, 'LV', ''),
	REPLACE (winkelartikel.bestelcodepartner, 'lv', ''),
    artikel.omschrijving1,
    artikel.inkoopvariant,
    artikel.verkoopvariant,
    artikel.actief
from
    winkelartikel,
    artikel
where
    artikel.id = winkelartikel.artikelid
        and winkelartikel.bestelcodepartner LIKE 'LV%'
group by winkelartikel.bestelcodepartner
order by winkelartikel.bestelcodepartner ASC
