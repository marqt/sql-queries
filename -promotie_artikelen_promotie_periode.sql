select 
  distinct(artikelId)
from
    promotie,
    promotieperiode,
    promotiestatus,
    promotieartikel
where
    promotie.promotieperiodeid = promotieperiode.Id
        and promotie.promotieStatusId = promotiestatus.Id
        and promotieartikel.promotieId = promotie.Id
        and promotieperiode.startdatum >= date_sub(NOW(), interval 14 day)
        and promotieperiode.einddatum <= date_sub(NOW(), interval -7 day)
        order by artikelId
    