SELECT 
    count(*),
    format(sum(mwbesteld), 2, 'nl_nl'),
    format(sum(MWgeleverd), 2, 'nl_nl'),
    format(sum(inkoopfactuurregel.netAmount),
        2,
        'nl_NL'),
   if(inkoopfactuurregel.netAmount is null,'geen factuur','wel factuur') as factuur
FROM
    (inkooporder, inkooporderregel, winkelartikelexporthistorie, artikel, winkel, partner, product, grondslag, operationelegroep og1, operationelegroep og2, operationelegroep og3, besteleenheid)
        LEFT JOIN
    inkooporderhistorie ON inkooporderhistorie.inkooporderId = inkooporder.id
        AND inkooporderhistorie.statusid IN (5 , 7)
        LEFT JOIN
    inkooporderregelhistorie ON inkooporderregelhistorie.inkooporderregelId = inkooporderregel.id
        AND inkooporderregelhistorie.inkooporderhistorieId = inkooporderhistorie.id
        LEFT JOIN
    inkoopfactuur ON inkoopfactuur.inkooporderid = inkooporder.id
        LEFT JOIN
    inkoopfactuurregel ON inkoopfactuurregel.inkoopFactuurId = inkoopfactuur.id
        AND inkoopfactuurregel.artikelid = inkooporderregel.artikelid
WHERE
    inkooporder.winkelid = winkel.id
        AND inkooporder.partnerid = partner.id
        AND inkooporder.id = inkooporderregel.orderid
        AND inkooporderregel.winkelartikelExportHistorieId = winkelartikelexporthistorie.id
        AND winkelartikelexporthistorie.bestelEenheidId = besteleenheid.id
        AND inkooporderregel.artikelid = artikel.id
        AND artikel.ProductId = product.id
        AND product.GrondslagId = grondslag.Id
        AND product.OperationeleGroepId = og3.Id
        AND og3.OperationeleGroepParentId = og2.Id
        AND og2.OperationeleGroepParentId = og1.Id
        AND inkooporder.statusid = 9
        AND partner.nummer = 19
        AND artikel.artikelTypeId != 2
        AND yearweek(inkooporder.statusdatumtijd, 3) BETWEEN 201609 AND 201624
group by factuur
order by factuur