select 
    artikel.ArtikelNummer,
    winkelartikel.BestelCodePartner,
    artikel.Omschrijving1,
    partner.gtin,
    (select 
            barcode
        from
            barcode
        where
            barcode.artikelid = artikel.id
                and datumvanaf <= curdate()
                and besteleenheidid is not null
        order by datumvanaf desc
        limit 1) as bebarcode,
    artikel.merk,
    product.inhoud,
    grondslag.code,
    (select 
            barcode
        from
            barcode
        where
            barcode.artikelid = artikel.id
                and datumvanaf <= curdate()
                and besteleenheidid is null
        order by datumvanaf desc
        limit 1) as cebarcode,
    (select 
            format(BestelHoeveelheid, 3, 'nl_NL')
        from
            besteleenheid
        where
            besteleenheid.artikelid = artikel.id
                and datumvanaf <= curdate()
        order by datumvanaf desc
        limit 1) as bestelhoeveelheid,
    (select 
            meeteenheid.Code
        from
            besteleenheid,
            meeteenheid
        where
            meeteenheid.id = besteleenheid.BestelMeeteenheidId
                and besteleenheid.artikelid = artikel.id
                and datumvanaf <= curdate()
        order by datumvanaf desc
        limit 1) as bestelhoeveelheidmeeteenheid,
    levergroep.naam,
    levergroep.id,
    artikel.statusId
from
    partner,
    winkelpartnerafdeling,
    winkelartikel,
    artikel,
    product,
    grondslag,
    levergroep
where
    partner.id = winkelpartnerafdeling.PartnerId
        and winkelpartnerafdeling.id = winkelartikel.WinkelPartnerAfdelingId
        and winkelartikel.ArtikelId = artikel.Id
        and artikel.ProductId = product.Id
        and product.GrondslagId = grondslag.Id
        and ifnull(winkelartikel.LeverGroepId,
            winkelpartnerafdeling.DefaultLeverGroepId) = levergroep.Id
        and winkelartikel.bestelbaar = 1
        and partner.Nummer = 44
group by artikel.id
order by BestelCodePartner
