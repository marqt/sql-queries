select 
    partner.naam,
    levergroep.naam,
    orderschema.naam,
    orderschemaitem.BestelDag,
    orderschemaitem.BestelTijd
from
    partner,
    levergroep,
    orderschema,
    orderschemaitem
where
    partner.id = levergroep.partnerid
        and levergroep.id = orderschema.levergroepid
        and orderschema.id = orderschemaitem.orderschemaid
        and orderschema.actief = 1
        and orderschema.ingangsdatum <= curdate()
