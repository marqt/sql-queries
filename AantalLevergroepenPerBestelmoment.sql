/* -------------------------------------------------------------------------------
-- Author       Bob Hentenaar
-- Created      15/08/2017
-- Purpose      Query om aantal levergroepen per bestelmoment inzichtelijk te maken
--              
-- Copyright © 2017, Marqt B.V., All Rights Reserved
-------------------------------------------------------------------------------
-- Modification History
--
-- 05/07/0000  developer full name  
--      A comprehensive description of the changes. The description may use as 
--      many lines as needed.
---------------------------------------------------------------------------- */
SELECT
	bestelDatumTijd,
	count(*),
    group_concat(levergroepid)
    
FROM
    central.v_bestellevermomenttim,
    levergroep,
    partner
WHERE
    v_bestellevermomenttim.levergroepid = levergroep.id
    and levergroep.PartnerId=partner.id
		#Vanaf NU
        #AND v_bestellevermomenttim.bestelDatumTijd >= NOW()
        #Vandaag 
        AND v_bestellevermomenttim.bestelDatumTijd >= CURDATE()
        AND v_bestellevermomenttim.bestelDatumTijd <= DATE_SUB(DATE(CURDATE()), INTERVAL -31 DAY)
        #and orderForecast=1
        
GROUP BY bestelDatumTijd
ORDER BY bestelDatumTijd ASC