select 
    week, count(distinct (verkoop.id))
from
    verkoop,
    verkoopregel
where
    verkoop.id = verkoopregel.verkoopid
        and verkoop.jaar = 2013
        and verkoop.week between 20 and 35
        and verkoopregel.commercielegroepid = 12731
        and not exists( select distinct
            (verkoop.id)
        from
            verkoop ver,
            verkoopregel verreg
        where
            ver.id = verreg.verkoopid
                and verreg.commercielegroepid = 12658
                and ver.jaar = verkoop.jaar
                and ver.week = verkoop.week
                and ver.id = verkoop.id)
group by verkoop.week

