select 
    winkel.nummer,
    artikel.omschrijving1,
    artikel.artikelnummer,
    @besteldag:=ifnull((select 
                    orderschemaitem.besteldag
                from
                    winkelpartnerafdeling,
                    levergroep,
                    orderschema,
                    orderschemaitem
                where
                    artikel.id = winkelartikel.artikelid
                        and winkelartikel.WinkelPartnerAfdelingId = winkelpartnerafdeling.id
                        and levergroep.id = ifnull(winkelartikel.LeverGroepId,
                            winkelpartnerafdeling.DefaultLeverGroepId)
                        and orderschema.LeverGroepId = levergroep.Id
                        and orderschemaitem.OrderSchemaId = orderschema.Id
                        and orderschema.Actief = 1
                        and besteldag > weekday(curdate()) + 2
                order by BestelDag asc
                limit 0 , 1),
            (select 
                    orderschemaitem.besteldag
                from
                    winkelpartnerafdeling,
                    levergroep,
                    orderschema,
                    orderschemaitem
                where
                    winkelartikel.WinkelPartnerAfdelingId = winkelpartnerafdeling.id
                        and levergroep.id = ifnull(winkelartikel.LeverGroepId,
                            winkelpartnerafdeling.DefaultLeverGroepId)
                        and orderschema.LeverGroepId = levergroep.Id
                        and orderschemaitem.OrderSchemaId = orderschema.Id
                        and orderschema.Actief = 1
                order by BestelDag asc
                limit 0 , 1)) as 'Eerst_volgende_besteldag_na_vandaag',
    @leverdagwinkel:=ifnull((select 
                    orderschemaitem.leverdagwinkel
                from
                    winkelpartnerafdeling,
                    levergroep,
                    orderschema,
                    orderschemaitem
                where
                    artikel.id = winkelartikel.artikelid
                        and winkelartikel.WinkelPartnerAfdelingId = winkelpartnerafdeling.id
                        and levergroep.id = ifnull(winkelartikel.LeverGroepId,
                            winkelpartnerafdeling.DefaultLeverGroepId)
                        and orderschema.LeverGroepId = levergroep.Id
                        and orderschemaitem.OrderSchemaId = orderschema.Id
                        and orderschema.Actief = 1
                        and besteldag > weekday(curdate()) + 2
                order by BestelDag asc
                limit 1 , 1),
            (select 
                    orderschemaitem.leverdagwinkel
                from
                    winkelpartnerafdeling,
                    levergroep,
                    orderschema,
                    orderschemaitem
                where
                    winkelartikel.WinkelPartnerAfdelingId = winkelpartnerafdeling.id
                        and levergroep.id = ifnull(winkelartikel.LeverGroepId,
                            winkelpartnerafdeling.DefaultLeverGroepId)
                        and orderschema.LeverGroepId = levergroep.Id
                        and orderschemaitem.OrderSchemaId = orderschema.Id
                        and orderschema.Actief = 1
                order by BestelDag asc
                limit 0 , 1)) as leverdag,
    (@besteldag + @leverdagwinkel) as 'Leverdag_winkel_uit_schema_behorende_bij_de_eerstvolgende_bestelling'
from
    winkelartikel,
    artikel,
    winkel
where
    artikel.id = winkelartikel.artikelid
        and winkel.id = winkelartikel.winkelid
        and artikel.statusid in (3 , 4)