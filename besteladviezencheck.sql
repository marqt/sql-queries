SELECT 
    winkelid, filename, COUNT(*)
FROM
    besteladvies,
    besteladviesfile
WHERE
    besteladviesfile.id = besteladviesfileid
        AND DATE(datum) = curdate()
        AND backup = 0
        AND statusId = 1
GROUP BY besteladviesfile.id DESC
LIMIT 550