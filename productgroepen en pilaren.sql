select 
    og1.omschrijving, og2.omschrijving, og2.id
from
    operationelegroep og1,
    operationelegroep og2
where
    og2.OperationeleGroepParentId = og1.id
        and og1.id > 400
        and og1.nivo = 0