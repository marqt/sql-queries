select 
    artikel.omschrijving1
from
    artikel,
    winkelartikel,
    winkelpartnerafdeling,
    levergroep
where
    artikel.id = winkelartikel.ArtikelId
        and winkelartikel.winkelpartnerafdelingid = winkelpartnerafdeling.Id
        and ifnull(winkelartikel.LeverGroepId,
            winkelpartnerafdeling.DefaultLeverGroepId) = levergroep.Id
        and levergroep.id = 194
group by artikel.id