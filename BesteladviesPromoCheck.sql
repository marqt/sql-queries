-------------------------------------------------------------------------------
-- Author       Bob Hentenaar
-- Created      05/07/2017
-- Purpose      Query om een check te doen of besteladviezen voldoende bestellen
--              om de coverperiode van een promo te dekken.
--              
-- Copyright © 2017, Marqt B.V., All Rights Reserved
-------------------------------------------------------------------------------
-- Modification History
--
-- 05/07/0000  developer full name  
--      A comprehensive description of the changes. The description may use as 
--      many lines as needed.
-------------------------------------------------------------------------------

# Winkelartikelen met advies en vandaag is bestelmoment

SELECT
			artikelid,
            artikelnummer,
            omschrijving1,
            og1.Omschrijving AS pilaar,
            winkel.afkorting,
            adviesce
            #round(besteladviesregel.adviesCE*winkelartikel.PrijsInkoop,2) as marqtwaardeadvies
    FROM
        besteladviesregel, besteladvies, besteladviesfile, winkelartikel, artikel, winkel, product,operationelegroep og3, operationelegroep og2, operationelegroep og1,
        v_bestellevermoment, winkelpartnerafdeling
    WHERE
				besteladviesfile.id = besteladvies.besteladviesfileid
            AND besteladviesregel.besteladviesid = besteladvies.id
            AND besteladvies.statusId < 90
            AND DATE(besteladviesfile.datum) = DATE(NOW())
            AND winkelartikel.id = winkelartikelid
            AND besteladviesfile.winkelid = winkelartikel.winkelid
            AND winkelartikel.artikelid = artikel.id
            and winkel.id=winkelartikel.winkelid
            AND product.id = artikel.productid
			AND og3.id = product.OperationeleGroepId
            AND og2.id = og3.OperationeleGroepParentId
            AND og1.id = og2.OperationeleGroepParentId
            AND v_bestellevermoment.levergroepid = IFNULL(winkelartikel.levergroepid, winkelpartnerafdeling.defaultlevergroepid)
            and winkel.id<>4;
            