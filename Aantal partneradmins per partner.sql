select 
    partner.naam, count(*) as aantalactievegebruikers
from
    gebruikerfunctiepartner,
    gebruikerfunctie,
    gebruiker,
    functie,
    partner
where
    gebruikerfunctiepartner.gebruikerfunctieid = gebruikerfunctie.id
        and gebruikerfunctie.gebruikerid = gebruiker.id
        and gebruikerfunctie.functieid = functie.id
        and gebruikerfunctiepartner.partnerid = partner.id
        #and partner.id = 268
        and functie.id = 3
		and gebruiker.actief = 1
group by partner.id
having aantalactievegebruikers = 1 
