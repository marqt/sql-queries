CREATE TEMPORARY TABLE IF NOT EXISTS BB AS (SELECT 

    Omschrijving1 AS Artikelomschrijving,
    ArtikelNummer,
    og1.Omschrijving AS Pilaar,
    sum(besteladviesregel.adviesCE) AS advies

    
    
FROM
    central.artikel
        INNER JOIN
    product ON product.id = artikel.ProductId
        INNER JOIN
    operationelegroep og3 ON og3.id = product.OperationeleGroepId
        INNER JOIN
    operationelegroep og2 ON og2.id = og3.OperationeleGroepParentId
        INNER JOIN
    operationelegroep og1 ON og1.id = og2.OperationeleGroepParentId
        JOIN
    winkelartikel ON artikel.id = winkelartikel.ArtikelId
		JOIN
	besteladviesregel on besteladviesregel.winkelartikelId=winkelartikel.id
		join
	besteladvies on besteladviesregel.besteladviesId = besteladvies.id
		join
	besteladviesfile on besteladvies.besteladviesfileId=besteladviesfile.id

WHERE

og1.id=405 AND date(besteladviesfile.datum) = curdate() 

group by ArtikelNummer
     
Order by ArtikelNummer);

CREATE TEMPORARY TABLE IF NOT EXISTS VV AS (SELECT 

ArtikelNummer,
sum(voorraadstand) AS voorraad

FROM central.voorraadstand

join winkelartikel on winkelartikel.id=voorraadstand.winkelartikelId
join winkel on winkel.id=winkelartikel.WinkelId
join artikel on artikel.id = winkelartikel.ArtikelId
INNER JOIN
    product ON product.id = artikel.ProductId
        INNER JOIN
    operationelegroep og3 ON og3.id = product.OperationeleGroepId
        INNER JOIN
    operationelegroep og2 ON og2.id = og3.OperationeleGroepParentId
        INNER JOIN
    operationelegroep og1 ON og1.id = og2.OperationeleGroepParentId

where Winkel.id=4 and DATE(voorraadstandDatum)=CURDATE() and wijzigingssoort='DAGSTART_BEREKENING' and og1.id=405

group by artikel.artikelnummer);

select

*
from BB

JOIN VV on BB.artikelnummer = VV.artikelnummer

where 'BB.sum(besteladviesregel.adviesCE)' < 'VV.sum(voorraadstand)'