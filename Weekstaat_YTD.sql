select 
    winkel.*,
    resultaathuidigjaar . *,
    klantenhuidigjaar . *,
    dervinghuidigjaar . *,
    resultaatvorigjaar . *,
    klantenvorigjaar . *,
    dervingvorigjaar . *
from
    (select id, nummer, if(winkelsoortfinancieel = 'NONID','NIETID',winkelsoortfinancieel) as Winkeltype
    	from winkel where type = 'winkel' and nummer <= 166) winkel
        left join
    (select 
        winkelid,
        week,
            sum(totaalbedrag) as 'omzet_huidigjaar',
            sum(Totaalbedrag) - sum(Totaalbtw) - sum(totaalmarqtwaarde) as 'brutomarge_huidigjaar',
            sum(Totaalmarqtvolume) as 'scans_huidigjaar',
            sum(Totaalbedrag) - sum(Totaalbtw) as 'nettoomzet_huidigjaar'
    from
        resultaat
    where
        resultaat.label = 'OPSW'
            and jaar = year(curdate())
    group by winkelid) resultaathuidigjaar ON winkel.id = resultaathuidigjaar.winkelid
        left join
    (select 
        winkelid, count(verkoop.id) as 'klanten_huidigjaar'
    from
        verkoop
    where
        TransactieSoort = 'KAS' and jaar = year(curdate())
            and verkoop.BedragOmzet > 0
    group by winkelid) klantenhuidigjaar ON winkel.id = klantenhuidigjaar.winkelid
        left join
    (select 
        winkelid, sum(totaalmarqtwaarde) as 'derving_huidigjaar'
    from
        resultaat
    where
        resultaat.label = 'DVSW'
            and jaar = year(curdate())
    group by winkelid) dervinghuidigjaar ON winkel.id = dervinghuidigjaar.winkelid
        left join
    (select 
        winkelid,
            sum(totaalbedrag) as 'omzet_vorigjaar',
            sum(Totaalbedrag) - sum(Totaalbtw) - sum(totaalmarqtwaarde) as 'brutomarge_vorigjaar',
            sum(Totaalmarqtvolume) as 'scans_vorigjaar',
            sum(Totaalbedrag) - sum(Totaalbtw) as 'nettoomzet_vorigjaar'
    from
        resultaat
    where
        resultaat.label = 'OPSW'
            and jaar = year(date_sub(curdate(), interval 1 year))
            and week < week(curdate(),3)
    group by winkelid) resultaatvorigjaar ON winkel.id = resultaatvorigjaar.winkelid
        left join
    (select 
        winkelid, count(verkoop.id) as 'klanten_vorigjaar'
    from
        verkoop
    where
        TransactieSoort = 'KAS'
            and jaar = year(date_sub(curdate(), interval 1 year))
            and week < week(curdate(),3)
            and verkoop.BedragOmzet > 0
    group by winkelid) klantenvorigjaar ON winkel.id = klantenvorigjaar.winkelid
        left join
    (select 
        winkelid, sum(totaalmarqtwaarde) as 'derving_vorigjaar'
    from
        resultaat
    where
        resultaat.label = 'DVSW'
            and jaar = year(date_sub(curdate(), interval 1 year))
            and week < week(curdate(),3)
    group by winkelid) dervingvorigjaar ON winkel.id = dervingvorigjaar.winkelid