(select 
    'resultaat',
    sum(totaalbedrag) as omzet,
    sum(totaalbtw) as btw,
    sum(TotaalMarqtwaarde) as MW,
    sum(totaalfee) as Fee,
    sum(totaalbedrag - totaalbtw - TotaalMarqtwaarde) as POSmargeberekend
from
    resultaat
where
    jaar = 2015 and week = 1
        and label = 'BASE'
        and rubriekid = 1
and winkelid = 9
) union all (select 
    'verkoop-verkoopregel berekend',
    sum(artikelomzet),
    sum(artikelbtw),
    sum(artikelmw),
    sum(artikelfee),
    sum(artikelposmargeberekend)
from
    (select 
        sum(verkoopregel.bedragtotaal) as artikelomzet,
            round(sum(verkoopregel.bedragbtw), 2) as artikelbtw,
            round(sum(hoeveelheid * bedraginkoop), 4) artikelmw,
            round(sum(if(verkoopregel.prijs <> 0,verkoopregel.bedragfee,0)), 2) as artikelfee,
            sum(verkoopregel.bedragtotaal) - round(sum(verkoopregel.bedragbtw), 2) - round(sum(hoeveelheid * bedraginkoop), 4) as artikelposmargeberekend
    from
        verkoop, verkoopregel
    where
        verkoop.id = verkoopid and week = 1
            and jaar = 2015
            and rubriekid = 1
and winkelid = 9
    group by verkoop.winkelId , verkoop.Datum , verkoopregel.ArtikelId , verkoopRegel.PartnerId , verkoopRegel.RubriekId , verkoopRegel.CommercieleGroepId , verkoopRegel.AfdelingId) berekend) union all (select 
    'verkoop-verkoopregel',
    sum(verkoopregel.bedragtotaal),
    sum(verkoopregel.bedragbtw),
    sum(hoeveelheid * bedraginkoop) artikelmw,
    sum(verkoopregel.bedragfee) as artikelfee,
    sum(verkoopregel.bedragtotaal - verkoopregel.bedragbtw - hoeveelheid * bedraginkoop)
from
    verkoop,
    verkoopregel
where
    verkoop.id = verkoopid and week = 1
        and jaar = 2015
        and rubriekid = 1
and winkelid = 9
)
