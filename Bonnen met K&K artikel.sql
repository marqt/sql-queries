SELECT 
    *#sum(bedragtotaal), sum(bedragbtw), sum(bedragfee), count(id)
FROM
    verkoop
WHERE
    EXISTS (SELECT DISTINCT
            (ver.id)
        FROM
            verkoop ver,
            verkoopregel,
            afdeling
        WHERE
            ver.id = verkoopregel.verkoopid
                and verkoopregel.afdelingid = afdeling.id
                and afdeling.nummer in (6 , 11)
                and ver.week = verkoop.week
                and ver.jaar = verkoop.jaar
                and verkoopregel.rubriekid = 1
				and ver.id = verkoop.id)
AND verkoop.week = 33
AND verkoop.jaar = 2013
