SELECT 
    jaar2013min4week.omschrijving,
    jaar2013min4week.omzet / jaar2012min4week.omzet - 1,
    jaar2013min3week.omzet / jaar2012min3week.omzet - 1,
    jaar2013min2week.omzet / jaar2012min2week.omzet - 1,
    jaar2013min1week.omzet / jaar2012min1week.omzet - 1,
	(jaar2013min1week.omzet - jaar2012min1week.omzet)/1000
FROM
    (SELECT 
        com2.omschrijving, sum(totaalbedrag) AS 'omzet'
    FROM
        resultaat, artikel, product, winkel, commercielegroep com3, commercielegroep com2
    WHERE
        resultaat.artikelid = artikel.id
            AND product.id = artikel.productid
            AND winkel.id = resultaat.winkelid
            AND com3.id = product.commercieleGroepId
            AND com2.id = com3.commercieleGroepParentId
            AND resultaat.jaar = YEAR(CURDATE())
            AND resultaat.week = WEEK(CURDATE(), 3) - 4
            AND resultaat.label = 'OPSW'
            AND winkel.nummer in (151 , 152, 153)
    GROUP BY com2.id) jaar2013min4week,
    (SELECT 
        com2.omschrijving, sum(totaalbedrag) AS 'omzet'
    FROM
        resultaat, artikel, product, winkel, commercielegroep com3, commercielegroep com2
    WHERE
        resultaat.artikelid = artikel.id
            AND product.id = artikel.productid
            AND winkel.id = resultaat.winkelid
            AND com3.id = product.commercieleGroepId
            AND com2.id = com3.commercieleGroepParentId
            AND resultaat.jaar = YEAR(CURDATE()) - 1
            AND resultaat.week = WEEK(CURDATE(), 3) - 4
            AND resultaat.label = 'OPSW'
            AND winkel.nummer in (151 , 152, 153)
    GROUP BY com2.id) jaar2012min4week,
    (SELECT 
        com2.omschrijving, sum(totaalbedrag) AS 'omzet'
    FROM
        resultaat, artikel, product, winkel, commercielegroep com3, commercielegroep com2
    WHERE
        resultaat.artikelid = artikel.id
            AND product.id = artikel.productid
            AND winkel.id = resultaat.winkelid
            AND com3.id = product.commercieleGroepId
            AND com2.id = com3.commercieleGroepParentId
            AND resultaat.jaar = YEAR(CURDATE())
            AND resultaat.week = WEEK(CURDATE(), 3) - 3
            AND resultaat.label = 'OPSW'
            AND winkel.nummer in (151 , 152, 153)
    GROUP BY com2.id) jaar2013min3week,
    (SELECT 
        com2.omschrijving, sum(totaalbedrag) AS 'omzet'
    FROM
        resultaat, artikel, product, winkel, commercielegroep com3, commercielegroep com2
    WHERE
        resultaat.artikelid = artikel.id
            AND product.id = artikel.productid
            AND winkel.id = resultaat.winkelid
            AND com3.id = product.commercieleGroepId
            AND com2.id = com3.commercieleGroepParentId
            AND resultaat.jaar = YEAR(CURDATE()) - 1
            AND resultaat.week = WEEK(CURDATE(), 3) - 3
            AND resultaat.label = 'OPSW'
            AND winkel.nummer in (151 , 152, 153)
    GROUP BY com2.id) jaar2012min3week,
    (SELECT 
        com2.omschrijving, sum(totaalbedrag) AS 'omzet'
    FROM
        resultaat, artikel, product, winkel, commercielegroep com3, commercielegroep com2
    WHERE
        resultaat.artikelid = artikel.id
            AND product.id = artikel.productid
            AND winkel.id = resultaat.winkelid
            AND com3.id = product.commercieleGroepId
            AND com2.id = com3.commercieleGroepParentId
            AND resultaat.jaar = YEAR(CURDATE())
            AND resultaat.week = WEEK(CURDATE(), 3) - 2
            AND resultaat.label = 'OPSW'
            AND winkel.nummer in (151 , 152, 153)
    GROUP BY com2.id) jaar2013min2week,
    (SELECT 
        com2.omschrijving, sum(totaalbedrag) AS 'omzet'
    FROM
        resultaat, artikel, product, winkel, commercielegroep com3, commercielegroep com2
    WHERE
        resultaat.artikelid = artikel.id
            AND product.id = artikel.productid
            AND winkel.id = resultaat.winkelid
            AND com3.id = product.commercieleGroepId
            AND com2.id = com3.commercieleGroepParentId
            AND resultaat.jaar = YEAR(CURDATE()) - 1
            AND resultaat.week = WEEK(CURDATE(), 3) - 2
            AND resultaat.label = 'OPSW'
            AND winkel.nummer in (151 , 152, 153)
    GROUP BY com2.id) jaar2012min2week,
    (SELECT 
        com2.omschrijving, sum(totaalbedrag) AS 'omzet'
    FROM
        resultaat, artikel, product, winkel, commercielegroep com3, commercielegroep com2
    WHERE
        resultaat.artikelid = artikel.id
            AND product.id = artikel.productid
            AND winkel.id = resultaat.winkelid
            AND com3.id = product.commercieleGroepId
            AND com2.id = com3.commercieleGroepParentId
            AND resultaat.jaar = YEAR(CURDATE())
            AND resultaat.week = WEEK(CURDATE(), 3) - 1
            AND resultaat.label = 'OPSW'
            AND winkel.nummer in (151 , 152, 153)
    GROUP BY com2.id) jaar2013min1week,
    (SELECT 
        com2.omschrijving, sum(totaalbedrag) AS 'omzet'
    FROM
        resultaat, artikel, product, winkel, commercielegroep com3, commercielegroep com2
    WHERE
        resultaat.artikelid = artikel.id
            AND product.id = artikel.productid
            AND winkel.id = resultaat.winkelid
            AND com3.id = product.commercieleGroepId
            AND com2.id = com3.commercieleGroepParentId
            AND resultaat.jaar = YEAR(CURDATE()) - 1
            AND resultaat.week = WEEK(CURDATE(), 3) - 1
            AND resultaat.label = 'OPSW'
            AND winkel.nummer in (151 , 152, 153)
    GROUP BY com2.id) jaar2012min1week
WHERE
    jaar2013min1week.omschrijving = jaar2012min4week.omschrijving
        AND jaar2013min1week.omschrijving = jaar2013min4week.omschrijving
        AND jaar2013min1week.omschrijving = jaar2012min3week.omschrijving
        AND jaar2013min1week.omschrijving = jaar2013min3week.omschrijving
        AND jaar2013min1week.omschrijving = jaar2012min2week.omschrijving
        AND jaar2013min1week.omschrijving = jaar2013min2week.omschrijving
        AND jaar2013min1week.omschrijving = jaar2012min1week.omschrijving
order by jaar2013min4week.omschrijving