/* -------------------------------------------------------------------------------
-- Author       Bob Hentenaar
-- Created      12/09/2017
-- Purpose      Query om orderschemaitem/orderschem/levergroep info op te roepen
--  
--              
-- Copyright © 2017, Marqt B.V., All Rights Reserved
-------------------------------------------------------------------------------
-- Modification History
--
-- 05/07/0000  developer full name  
--      A comprehensive description of the changes. The description may use as 
--      many lines as needed.
---------------------------------------------------------------------------- */

select 

partner.naam, 
levergroep.Naam,
levergroep.Stroom,
orderschema.id,
orderschema.LaatsteWijziging,
levergroep.id,
orderschema.Naam,
orderschema.IngangsDatum,
orderschema.EindDatum,
orderschema.actief,
orderschemaitem.id,
orderschema.id,
orderschemaitem.LaatsteWijziging,
orderschemaitem.BestelDag,
orderschemaitem.BestelTijd,
orderschemaitem.OphaalDagPartner,
orderschemaitem.OphaalTijdPartner,
orderschemaitem.LeverDagDC,
orderschemaitem.LeverTijdDC,
orderschemaitem.LeverDagWinkel,
dayname(day(orderschemaitem.BestelDag)),
orderschemaitem.BestelTijd,
'' as week

from orderschema, levergroep, partner, orderschemaitem

where orderschema.LeverGroepId=levergroep.id
and partner.id=levergroep.PartnerId
and orderschemaitem.OrderSchemaId=orderschema.id

order by partner.id asc


