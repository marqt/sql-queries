select 
    verkoop.week,
    verkoop.datum,
    winkel.nummer,
    verkoop.starttijd
from
    verkoop,
    verkoopregel,
    winkel
where
    verkoop.id = verkoopregel.verkoopid
        and winkel.id = verkoop.winkelid
        and verkoop.jaar = 2013
        and verkoop.week = 46
        and verkoop.weekdag = 6
        and winkel.nummer in (151 , 153, 158)
        and verkoopregel.commercielegroepid in (12731 , 12658)
group by verkoop.id
order by verkoop.week , verkoop.datum , winkel.nummer , verkoop.starttijd