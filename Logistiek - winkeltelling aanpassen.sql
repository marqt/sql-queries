select 
    artikel.artikelnummer,
    artikel.omschrijving1,
    inventarisregel.totaalgeteld,
    resultaat.TotaalVolume as 'afzet 26-08',
    derving.TotaalVolume as 'derving 26-08',
    ontvangen.cegeleverd as 'geleverd 26-08'
from
    inventaris
        inner join
    inventarisregel ON inventarisregel.inventarisid = inventaris.id
        inner join
    winkelartikel ON winkelartikel.id = inventarisregel.winkelartikelid
        and winkelartikel.winkelid = inventaris.winkelid
        inner join
    artikel ON winkelartikel.artikelid = artikel.id
        inner join
    resultaat ON winkelartikel.artikelid = resultaat.artikelid
        and winkelartikel.winkelid = resultaat.winkelid
        and label = 'OPSD'
        and datum = '2014-08-26'
        left join
    resultaat derving ON winkelartikel.artikelid = derving.artikelid
        and winkelartikel.winkelid = derving.winkelid
        and derving.label = 'DVSD'
        and derving.datum = '2014-08-26'
        left join
    (select 
        artikelid, winkelid, CEgeleverd
    from
        inkooporder, inkooporderregel
    where
        inkooporder.id = inkooporderregel.orderId
            and goederenontvangstdatumtijd between '2014-08-26' and '2014-08-27') ontvangen ON ontvangen.artikelid = winkelartikel.artikelid
        and ontvangen.winkelid = winkelartikel.winkelid
where
    inventaris.id = 6357
