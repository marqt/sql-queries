# Hier de datum van de inventarisatie invullen
set @InventarisatieInWinkelDatum='2018-01-01 00:00:00';

SELECT 
	a.artikelnummer,
    a.Omschrijving1,
    waeh112018.PrijsAdviesIncBtw AS '" price="',
    g.omschrijving
    
FROM
    winkelartikel wa
        INNER JOIN
    (SELECT 
        winkelartikelid, PrijsAdviesIncBtw
    FROM
        winkelartikelexporthistorie
    WHERE
        (@InventarisatieInWinkelDatum BETWEEN winkelartikelexporthistorie.datumGeldigVanaf AND winkelartikelexporthistorie.datumGeldigTot)) AS waeh112018 ON waeh112018.winkelartikelid = wa.id

inner join artikel a on a.id = wa.artikelid
inner join product p on p.id=a.productId
inner join barcode bc on bc.artikelid = a.id
inner join meeteenheid m on m.id = p.inhoudmeeteenheidid
inner join winkel w on w.id = wa.winkelId
inner join winkelpartnerafdeling as wpa on wpa.id = wa.winkelpartnerafdelingid
inner join afdeling as afd on afd.id = wpa.afdelingId
inner join partner on wpa.partnerid = partner.id
inner join grondslag g on p.grondslagid = g.id

where w.nummer in (158,153,165)
and (afd.nummer in (0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15) or a.artikelnummer in (950235, 950244, 127303, 122779, 172400)) 
#and partner.actief = 1

    

group by a.artikelnummer
order by a.artikelnummer

