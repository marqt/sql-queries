select artikel.omschrijving1, product.inhoud, meeteenheid.Omschrijving,
concat(com1.Nummer , '.',com2.nummer, '.', com3.nummer, ' ',  com1.Omschrijving , '-' , com2.Omschrijving, '-' ,com3.Omschrijving) as commercielegroep
from artikel
inner join product on product.id=artikel.productid
inner join meeteenheid on product.inhoudmeeteenheidid = meeteenheid.id
inner join commercielegroep as com3 on com3.id = product.commercieleGroepId
inner join commercielegroep as com2 on com2.id = com3.commercieleGroepParentId
inner join commercielegroep as com1 on com1.id = com2.commercieleGroepParentId
where com3.nummer = 101